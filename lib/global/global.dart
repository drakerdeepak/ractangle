import 'package:flutter/cupertino.dart';

class Global {
  static final List<String> lang = [
    "English",
    "हिंदी",
  ];

  static final List<String> lang1 = [
    "English",
    "हिंदी",
    "मराठी",
    "ಕನ್ನಡ",
  ];

  static final List<String> article = [
    "Clap",
    "Share",
    "Bookmark",
  ];

  static height(context) => MediaQuery.of(context).size.height;
  static width(context) => MediaQuery.of(context).size.width;
  static String apikey = ""/*"c2085a87c49f4884a2715f7512291114"*//*"paste your key here"*/;
}
