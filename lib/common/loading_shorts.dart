import 'package:flutter/material.dart';
import 'package:Two/global/global.dart';
import 'package:Two/style/text_style.dart';

import '../aplication_localization.dart';

class LoadingShorts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: SafeArea(
        child: Container(
          constraints: BoxConstraints(
            minHeight: Global.height(context),
            minWidth: double.maxFinite,
          ),
          decoration: BoxDecoration(


            color: Theme.of(context).scaffoldBackgroundColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/images/img_splash.png",
                  fit: BoxFit.contain,
                  width: double.maxFinite,
                  height: Global.height(context) * 0.5,
                ),
                SizedBox(
                  height: 8,
                ),
                // Text(
                //   AppLocalizations.of(context).translate('loading_message'),
                //   style: AppTextStyle.loading,
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
