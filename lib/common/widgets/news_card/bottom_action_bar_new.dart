import 'package:Two/controller/settings.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:Two/bloc/feed/news_feed_bloc.dart';
import 'package:Two/bloc/feed/news_feed_event.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/model/news_model.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/services/news/offline_service.dart';
import 'package:Two/services/news/share_service.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/style/text_style.dart';

import 'package:provider/provider.dart';

import '../../../aplication_localization.dart';

class BottomActionBarNew extends StatelessWidget {
  final containerKey;
  final Articles articles;

  const BottomActionBarNew({
    Key key,
    this.containerKey,
    this.articles,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider2 = Provider.of<SettingsProvider>(context, listen: false);
    return Container(
      width: double.maxFinite,
     // color: AppColor.upcomming,
      color: Theme.of(context).bottomAppBarColor,
     // padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          actionButton(
            title: AppLocalizations.of(context).translate("home"),
            icon: FeatherIcons.home,
            provider: provider2,
            onTap: () {

              final provider = Provider.of<FeedProvider>(context, listen: false);
              if (provider.gethasDataLoaded) {
                 provider.getCurentArticalIndex == 0;
                 Icon(FeatherIcons.rotateCw);
                      reloade(context);
                      bringToTop(provider.getfeedPageController);
              } else {
                return IconButton(icon: Icon(FeatherIcons.loader), onPressed: null);
              }


             /* provider.getCurentArticalIndex == 0;

                    reloade(context);
                    bringToTop(provider.getfeedPageController);
                  }*/
                 // bringToTop(provider.getfeedPageController));

              /*Rouut.navigator.pushNamed(
                  Rouut.feedScreen);*/

              /*Provider.of<FeedProvider>(context, listen: false)
                  .setWatermarkVisible(true);

              Future.delayed(Duration(seconds: 2),
                      () => convertWidgetToImageAndShare(context, containerKey));*/
            },
          ),
          actionButton(
            title: AppLocalizations.of(context).translate("video"),
            icon: FeatherIcons.video,
            provider: provider2,
            onTap: () {
              Rouut.navigator.pushNamed(
                  Rouut.videoScreen);
            },

          ),

          actionButton(
            title: AppLocalizations.of(context).translate("article"),
            icon: FeatherIcons.bookmark,
            provider: provider2,
            onTap: () {
              Rouut.navigator.pushNamed(
                  Rouut.articleScreen);
            },
          ),

          actionButton(
            title: AppLocalizations.of(context).translate("profile"),
            icon: FeatherIcons.user,
            provider: provider2,
            onTap: () {
              Rouut.navigator.pushNamed(
                  Rouut.profileScreen);
            },
          ),
         /* WatchBoxBuilder(
            box: Hive.box<Articles>('bookmarks'),
            builder: (context, snap) => actionButton(
              title: AppLocalizations.of(context).translate("bookmark"),
              icon: snap.containsKey(articles.url)
                  ? Icons.bookmark
                  : FeatherIcons.bookmark,
              onTap: () {
                handleBookmarks(articles);
              },
            ),
          )*/







          /*actionButton(
            title: AppLocalizations.of(context).translate("share"),
            icon: FeatherIcons.share2,
            onTap: () {
              Provider.of<FeedProvider>(context, listen: false)
                  .setWatermarkVisible(true);

              Future.delayed(Duration(seconds: 2),
                  () => convertWidgetToImageAndShare(context, containerKey));
            },
          ),
          WatchBoxBuilder(
            box: Hive.box<Articles>('bookmarks'),
            builder: (context, snap) => actionButton(
              title: AppLocalizations.of(context).translate("bookmark"),
              icon: snap.containsKey(articles.url)
                  ? Icons.bookmark
                  : FeatherIcons.bookmark,
              onTap: () {
                handleBookmarks(articles);
              },
            ),
          )*/
        ],
      ),
    );
  }

  Widget actionButton({
    @required String title,
    @required IconData icon,
    @required Function onTap, SettingsProvider provider,
  }) {
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            icon,
           // color: AppColor.grey2,
            color: provider.isDarkThemeOn
                ? AppColor.surface
                : AppColor.grey2,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            title,
            style: AppTextStyle.bottomActionbar,
          ),
        ],
      ),
    );
  }
}

void bringToTop(PageController pageController) {
  pageController.animateToPage(0,
      duration: Duration(milliseconds: 700), curve: Curves.ease);
}

List<BottomNavigationBarItem> _getNavBarItems() {
  //  if (mediaType == MediaType.movie) {
  return [
    BottomNavigationBarItem(
      /* icon:  new Image.asset(im,width: 26,height: 26,),
          activeIcon:new Image.asset(home_icon,width: 26,height: 26,color: color_app,),*/
        icon: Icon(Icons.home_filled),
        title: Text('Home',style: TextStyle(fontWeight: FontWeight.w500),)),
    BottomNavigationBarItem(
      /*icon:  new Image.asset(diseases_icon,width: 26,height: 26,),
          activeIcon:new Image.asset(diseases_icon,width: 26,height: 26,color: color_app,),*/
        icon: Icon(Icons.video_collection_outlined),
        title: Text('Video',style: TextStyle(fontWeight: FontWeight.w500))),
    BottomNavigationBarItem(
      /*icon:  new Image.asset(reports_icon,width: 26,height: 26,),
          activeIcon:new Image.asset(reports_icon,width: 26,height: 26,color: color_app,),*/
        icon: Icon(Icons.content_paste),
        title: Text('Artical',style: TextStyle(fontWeight: FontWeight.w500))),
    BottomNavigationBarItem(
      /*icon:  new Image.asset(about_icon,width: 26,height: 26,),
          activeIcon:new Image.asset(about_icon,width: 26,height: 26,color: color_app,),*/
        icon: Icon(Icons.person),
        title: Text('Profile',style: TextStyle(fontWeight: FontWeight.w500))),
    /*BottomNavigationBarItem(
          icon:  new Image.asset(profile_icon,width: 26,height: 26,),
          activeIcon:new Image.asset(profile_icon,width: 26,height: 26,color: color_app,),
          title: Text('Profile',style: TextStyle(fontWeight: FontWeight.w500))),
*/
  ];
}

void reloade(context) {

  print("hi reloade");
  final provider = Provider.of<FeedProvider>(context, listen: false);
  print("lastGetReq - "+provider.getLastGetRequest.elementAt(0));

  switch (provider.getLastGetRequest.elementAt(0)) {
    case "getNewsByTopic":
      BlocProvider.of<NewsFeedBloc>(context)
        ..add(
          FetchNewsByTopicEvent(
              topic: provider.getLastGetRequest.elementAt(1)),
        );
      break;
    case "getNewsByCategory":
      BlocProvider.of<NewsFeedBloc>(context)
        ..add(
          FetchNewsByCategoryEvent(
              category: provider.getLastGetRequest.elementAt(1)),
        );
      break;
    case "getNewsFromLocalStorage":
      BlocProvider.of<NewsFeedBloc>(context)
        ..add(
          FetchNewsFromLocalStorageEvent(
              box: provider.getLastGetRequest.elementAt(1)),
        );
      break;
    default:
      return;
  }
}


