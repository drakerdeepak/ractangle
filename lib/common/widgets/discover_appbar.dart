
import 'package:Two/controller/settings.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Two/aplication_localization.dart';
import 'package:Two/bloc/feed/news_feed_bloc.dart';
import 'package:Two/bloc/feed/news_feed_event.dart';
import 'package:Two/controller/feed_controller.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/global/global.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/style/text_style.dart';
import 'package:provider/provider.dart';

class CustomAppBar extends StatelessWidget {
  final int index;
  const CustomAppBar({Key key, this.index = 1}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider2 = Provider.of<SettingsProvider>(context, listen: false);
    return Consumer<FeedProvider>(
      builder: (context, value, child) => SafeArea(
        child: Material(
          // color: Colors.white,
          child: Container(
            height: 52,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: index != 1
                              ? Consumer<SettingsProvider>(
                              builder: (context, theme, child) =>
                                  IconButton(
                                    icon: Icon(FeatherIcons.settings,
                                      //size: 18,
                                      color: theme.isDarkThemeOn
                                          ? Colors.white
                                          : Colors.black,
                                    ),

                                    onPressed: () {
                                      Rouut.navigator.pushNamed(
                                          Rouut.settingsScreen);
                                    },
                                  ))


                          /*IconButton(
                              icon: Icon(
                                FeatherIcons.settings, color: ColorConst.BLACK_COLOR
                              ),
                              onPressed: () {
// Setting Work
                                Rouut.navigator.pushNamed(
                                  Rouut.settingsScreen);

                              },
                            )*/
                              : Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Consumer<SettingsProvider>(
                                  builder: (context, theme, child) =>
                                      IconButton(
                                        icon: Icon(FeatherIcons.chevronLeft,
                                          //size: 18,
                                          color: theme.isDarkThemeOn
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                        // alignment: Alignment.topCenter,
                                        onPressed: () {
                                          FeedController.addCurrentPage(0);

                                        },
                                      )),
                              /*IconButton(
                                  icon: Icon(FeatherIcons.chevronLeft,
                                      color: ColorConst.BLACK_COLOR),
                                  onPressed: () {
                                    FeedController.addCurrentPage(0);
                                  },
                                ),*/
                              Text(
                                AppLocalizations.of(context)
                                    .translate("discover"),
                                style: AppTextStyle.appBarTitle,
                              )
                            ],
                          ),
                        )),
                    Expanded(
                      child: Text(
                        index == 1
                            ? value.getAppBarTitle != null
                            ? value.getAppBarTitle
                            : AppLocalizations.of(context)
                            .translate("my_feed")
                            : AppLocalizations.of(context)
                            .translate("discover"),
                        style: AppTextStyle.appBarTitle.copyWith(
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            index != 1
                                ? Text(
                              value.getAppBarTitle != null
                                  ? value.getAppBarTitle
                                  : AppLocalizations.of(context)
                                  .translate("my_feed"),
                              style: AppTextStyle.appBarTitle1,
                              overflow: TextOverflow.ellipsis,
                              textDirection: TextDirection.rtl,
                            )
                                : Container(),
                            getIcon(context)
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  width: Global.width(context) * 0.1,
                  height: 3,
                  color: provider2.isDarkThemeOn
                      ? Colors.white
                      : Colors.black,//ColorConst.BLACK_COLOR,
                  //color: AppColor.accent,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void bringToTop(PageController pageController) {
    pageController.animateToPage(0,
        duration: Duration(milliseconds: 700), curve: Curves.ease);
  }

  IconButton getIcon(context) {
    final provider = Provider.of<FeedProvider>(context, listen: false);
    final provider1 = Provider.of<SettingsProvider>(context, listen: false);


    if (index != 1) {
      return IconButton(
          icon: Icon(FeatherIcons.chevronRight,
              color: provider1.isDarkThemeOn
                  ? Colors.white
                  : Colors.black
            /*color: ColorConst.BLACK_COLOR*/),
          onPressed: () {
            FeedController.addCurrentPage(1);
          });
    } else {
      if (provider.gethasDataLoaded) {
        return provider.getCurentArticalIndex == 0
            ? IconButton(
          icon: Icon(FeatherIcons.rotateCw,
            //size: 18,
            color: provider1.isDarkThemeOn
                ? Colors.white
                : Colors.black,
          ),
          // alignment: Alignment.topCenter,
          onPressed: () {
            reloade(context);

          },
        ): IconButton(
            icon: Icon(FeatherIcons.arrowUp,/*color: ColorConst.BLACK_COLOR*/color: provider1.isDarkThemeOn
                ? Colors.white
                : Colors.black),
            onPressed: () => bringToTop(provider.getfeedPageController));

        /*IconButton(
                icon: Icon(FeatherIcons.rotateCw,color: ColorConst.BLACK_COLOR),
                onPressed: () {
                  reloade(context);
                })
            : IconButton(
                icon: Icon(FeatherIcons.arrowUp,color: ColorConst.BLACK_COLOR),
                onPressed: () => bringToTop(provider.getfeedPageController));*/
      } else {
        return IconButton(icon: Icon(FeatherIcons.loader,color: provider1.isDarkThemeOn
            ? Colors.white
            : Colors.black), onPressed: null);
      }
    }
  }

  void reloade(context) {

    print("hi reloade");
    final provider = Provider.of<FeedProvider>(context, listen: false);
    print("lastGetReq - "+provider.getLastGetRequest.elementAt(0));

    switch (provider.getLastGetRequest.elementAt(0)) {
      case "getNewsByTopic":
        BlocProvider.of<NewsFeedBloc>(context)
          ..add(
            FetchNewsByTopicEvent(
                topic: provider.getLastGetRequest.elementAt(1)),
          );
        break;
      case "getNewsByCategory":
        BlocProvider.of<NewsFeedBloc>(context)
          ..add(
            FetchNewsByCategoryEvent(
                category: provider.getLastGetRequest.elementAt(1)),
          );
        break;
      case "getNewsFromLocalStorage":
        BlocProvider.of<NewsFeedBloc>(context)
          ..add(
            FetchNewsFromLocalStorageEvent(
                box: provider.getLastGetRequest.elementAt(1)),
          );
        break;
      default:
        return;
    }
  }
}
