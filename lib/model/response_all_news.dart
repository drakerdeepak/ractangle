import 'package:hive/hive.dart';

class AllNewsResponse {
  bool success;
  String message;
  int status;
  List<NewsInfo> data;

  AllNewsResponse({this.success, this.message, this.status, this.data});

  AllNewsResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <NewsInfo>[];
      json['data'].forEach((v) {
        data.add(new NewsInfo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
@HiveType(typeId: 101)
class NewsInfo {
  @HiveField(0)
  String newsId;
  @HiveField(1)
  String newsTitle;
  @HiveField(2)
  String newsContent;
  @HiveField(3)
  String newsDate;
  @HiveField(4)
  List<dynamic> newsImage;
  @HiveField(5)
  String youTubeUrl;
  @HiveField(6)
  String newsSource;
  @HiveField(7)
  String sourceURL;

  NewsInfo(
      {this.newsId,
        this.newsTitle,
        this.newsContent,
        this.newsDate,
        this.newsImage,
        this.youTubeUrl,
        this.newsSource,
        this.sourceURL});

  NewsInfo.fromJson(Map<String, dynamic> json) {
    newsId = json['news_id'];
    newsTitle = json['newsTitle'];
    newsContent = json['newsContent'];
    newsDate = json['newsDate'];
    newsImage = json['newsImage'] ?? <String>[];
    youTubeUrl = json['youTubeUrl'];
    newsSource = json['newsSource'];
    sourceURL = json['Source_URL'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['news_id'] = this.newsId;
    data['newsTitle'] = this.newsTitle;
    data['newsContent'] = this.newsContent;
    data['newsDate'] = this.newsDate;
    data['newsImage'] = this.newsImage;
    data['youTubeUrl'] = this.youTubeUrl;
    data['newsSource'] = this.newsSource;
    data['source_URL'] = this.sourceURL;
    return data;
  }
}
