class FilterNewsResponse {
  bool success;
  String message;
  int status;
  List<FilterNewsInfo> data;

  FilterNewsResponse({this.success, this.message, this.status, this.data});

  FilterNewsResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <FilterNewsInfo>[];
      json['data'].forEach((v) {
        data.add(new FilterNewsInfo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FilterNewsInfo {
  String newsId;
  String newsTitle;
  String newsContent;
  String newsDate;
  List<String> newsImage;
  Null youTubeUrl;
  String sourceURL;

  FilterNewsInfo(
      {this.newsId,
        this.newsTitle,
        this.newsContent,
        this.newsDate,
        this.newsImage,
        this.youTubeUrl,
        this.sourceURL});

  FilterNewsInfo.fromJson(Map<String, dynamic> json) {
    newsId = json['news_id'];
    newsTitle = json['newsTitle'];
    newsContent = json['newsContent'];
    newsDate = json['newsDate'];
    newsImage = json['newsImage'].cast<String>();
    youTubeUrl = json['youTubeUrl'];
    sourceURL = json['Source_URL'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['news_id'] = this.newsId;
    data['newsTitle'] = this.newsTitle;
    data['newsContent'] = this.newsContent;
    data['newsDate'] = this.newsDate;
    data['newsImage'] = this.newsImage;
    data['youTubeUrl'] = this.youTubeUrl;
    data['Source_URL'] = this.sourceURL;
    return data;
  }
}
