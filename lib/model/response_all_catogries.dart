class AllCategoriesResponse {
  bool success;
  String message;
  int status;
  List<CategoryInfo> data;

  AllCategoriesResponse({this.success, this.message, this.status, this.data});

  AllCategoriesResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <CategoryInfo>[];
      json['data'].forEach((v) {
        data.add(new CategoryInfo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CategoryInfo {
  String categoryId;
  String categoryName;
  String image;

  CategoryInfo({this.categoryId, this.categoryName, this.image});

  CategoryInfo.fromJson(Map<String, dynamic> json) {
    categoryId = json['category_id'];
    categoryName = json['category_name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_id'] = this.categoryId;
    data['category_name'] = this.categoryName;
    data['image'] = this.image;
    return data;
  }
}
