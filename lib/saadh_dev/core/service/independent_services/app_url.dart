import 'package:Two/saadh_dev/core/service/independent_services/constant.dart';

class AppUrl {

  //http://13.232.88.56/api/login

  static final baseHost = "13.232.88.56";
  static final baseHttp = "http://";
  static final baseUrl = "$baseHttp$baseHost";
  static final baseImageHost= "/admin/img/news/";
  static final imageUrl = "$baseHttp$baseHost$baseImageHost";

  static final pathLogin = "/api/login";//login: implemeted
  static final pathRegister = "/api/register";   //register account : implemented
  static final pathListofNews = "/api/news";//list of news
  static final pathListAllCategories = "/api/categories";//list of categories
  static final pathListofFilterCategory = "/api/filter_category";//list of news





}
