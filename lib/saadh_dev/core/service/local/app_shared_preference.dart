import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_log_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AppSharedPreference {
  final log = getLogger('App SP');

  // singleton boilerplate
  AppSharedPreference._internal();

  static final AppSharedPreference _singleInstance =
      AppSharedPreference._internal();

  factory AppSharedPreference() => _singleInstance;

  //save: token of user after successful login
  Future<bool> saveUserToken(String token) async {
    log.i('called: saveUserToken');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(AppConstants.KEY_TOKEN_USER, token);
  }

  //get: token of user
  Future<String> getUserToken() async {
    log.i('called: getUserToken');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(AppConstants.KEY_TOKEN_USER);
  }

  Future saveBooleanValue(String keyName, bool value) async {
    log.i('called { ' + keyName + " " + value.toString() + "}");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(keyName, value);
  }

  Future<bool> getBoolValue(String keyName) async {
    log.i('getIntValue' + keyName);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(keyName);
  }
}
