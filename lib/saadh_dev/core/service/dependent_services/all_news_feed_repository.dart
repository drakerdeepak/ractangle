import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Two/model/filter_news_request.dart';
import 'package:Two/model/response_all_news.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/base_repository/base_repository.dart';
import 'package:Two/saadh_dev/core/service/independent_services/app_url.dart';
import 'package:Two/saadh_dev/core/service/independent_services/constant.dart';
import 'package:Two/saadh_dev/core/service/independent_services/method.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_network_check.dart';

class AllNewsFeedRepository extends BaseRepository {
  //api: get all categories
  Future<AllNewsResponse> apiGetAllNews(String locale,FilterNewsRequest requestParams,String categoryid) async {

    bool isNetworkAvail = await NetworkCheck().check();
    print("QUERY ${requestParams.toJson()}");
    print("QUERY ${AppUrl.pathListofNews}");
    print("QUERY ${buildDefaultHeaderWithLocale(locale)}");
    print("QUERY ${(locale)}");
    if (isNetworkAvail) {
      final response = await networkProvider.call(
          method: Method.GET,
          pathUrl: AppUrl.pathListofNews+"?category_id=${Constant.selectedcategory}",
          // queryParam: requestParams.toJson(),
          headers: buildDefaultHeaderWithLocale(locale));

      print("QUERY ${response}");
      if (response.statusCode == HttpStatus.ok) {
        try {
          AllNewsResponse allNewsResponse =
          AllNewsResponse.fromJson(json.decode(response.body));
          allNewsResponse.success = true;
          return allNewsResponse;
        }
        catch(e){
          return e;
        }

      } else if (response.statusCode == HttpStatus.notFound) {
        return AllNewsResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.unprocessableEntity) {
        return AllNewsResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.badRequest) {
        return AllNewsResponse.fromJson(json.decode(response.body));
      } else {
        //need to handel network connection error
        return null;
      }
    } else {
      return AllNewsResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);
    }
  }
}
