import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Two/model/response_all_catogries.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/base_repository/base_repository.dart';
import 'package:Two/saadh_dev/core/service/independent_services/app_url.dart';
import 'package:Two/saadh_dev/core/service/independent_services/method.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_network_check.dart';

class NewsFeedRepository extends BaseRepository {
  //api: get all categories
  Future<AllCategoriesResponse> apiGetAllCategories(String locale) async {
    bool isNetworkAvail = await NetworkCheck().check();

    if (isNetworkAvail) {
      final response = await networkProvider.call(
          method: Method.GET,
          pathUrl: AppUrl.pathListAllCategories,
          headers: buildDefaultHeaderWithLocale(locale));
      if (response.statusCode == HttpStatus.ok) {
        AllCategoriesResponse allCategoriesResponse =
        AllCategoriesResponse.fromJson(json.decode(response.body));
        allCategoriesResponse.success = true;
        return allCategoriesResponse;
      } else if (response.statusCode == HttpStatus.notFound) {
        return AllCategoriesResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.unprocessableEntity) {
        return AllCategoriesResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.badRequest) {
        return AllCategoriesResponse.fromJson(json.decode(response.body));
      } else {
        //need to handel network connection error
        return null;
      }
    } else {
      return AllCategoriesResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);
    }
  }
}
