import 'package:Two/saadh_dev/core/notifiers/common_notifier.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/base_repository/base_repository.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/news_list/news_list_response.dart';
import 'package:Two/saadh_dev/core/service/independent_services/app_url.dart';
import 'package:Two/saadh_dev/core/service/independent_services/method.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_network_check.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'dart:io';
import 'dart:convert';


class NewsRepository extends BaseRepository {
  NewsRepository._internal();

  static final NewsRepository _singleInstance = NewsRepository._internal();

  factory NewsRepository() => _singleInstance;

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Kindly check your Internet connection'),
      ),
    );
  }

  Future<NewsListResponse> getNewsList(BuildContext context, String userToken) async {
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      final response = await networkProvider.call(
          pathUrl: AppUrl.pathListofNews,
          method: Method.GET,
         headers: buildDefaultHeaderWithToken(CommonNotifier().userToken));
      if (response.statusCode == HttpStatus.  ok) {
        NewsListResponse updateDiscoverResponse =
        NewsListResponse.fromJson(json.decode(response.body));
        updateDiscoverResponse.success = true;
        return updateDiscoverResponse;
      } else if (response.statusCode == HttpStatus.notFound) {
        return NewsListResponse.fromJson(json.decode(response.body));
      }else if (response.statusCode == HttpStatus.unauthorized) {
        return NewsListResponse.fromJson(json.decode(response.body));
      } else {
        //need to handel network connection error
        return null;
      }
    } else {
      //return WishListResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);
      _showToast(context);
    }
  }


}