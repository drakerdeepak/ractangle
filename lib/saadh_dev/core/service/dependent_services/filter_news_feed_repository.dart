import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Two/model/filter_news_request.dart';
import 'package:Two/model/response_all_news.dart';
import 'package:Two/model/response_filter_news.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/base_repository/base_repository.dart';
import 'package:Two/saadh_dev/core/service/independent_services/app_url.dart';
import 'package:Two/saadh_dev/core/service/independent_services/method.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_network_check.dart';

class FilterNewsFeedRepository extends BaseRepository {
  //api: get all categories
  Future<AllNewsResponse> apiGetFilterNewsList(String locale,FilterNewsRequest requestParams) async {
    bool isNetworkAvail = await NetworkCheck().check();

    if (isNetworkAvail) {
      final response = await networkProvider.call(
          method: Method.POST,
          pathUrl: AppUrl.pathListofFilterCategory,
          body: requestParams.toJson(),
          headers: buildDefaultHeaderWithLocale(locale));

      if (response.statusCode == HttpStatus.ok) {
        try {
          AllNewsResponse allNewsResponse =
          AllNewsResponse.fromJson(json.decode(response.body));
          allNewsResponse.success = true;
          return allNewsResponse;
        }
        catch(e){
          return e;
        }

      } else if (response.statusCode == HttpStatus.notFound) {
        return AllNewsResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.unprocessableEntity) {
        return AllNewsResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.badRequest) {
        return AllNewsResponse.fromJson(json.decode(response.body));
      } else {
        //need to handel network connection error
        return null;
      }
    } else {
      return AllNewsResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);
    }
  }
}
