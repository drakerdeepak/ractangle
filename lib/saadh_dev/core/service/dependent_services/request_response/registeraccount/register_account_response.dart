class RegisterAccountResponse {
  bool success = false;
  String message;
  int status;
  List errors;

  Data data;

  RegisterAccountResponse({this.success, this.message, this.status, this.data});

  RegisterAccountResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    errors = json['errors'] != null? json['errors']: null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['status'] = this.status;
    data['errors'] = this.errors;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String accessToken;
  UserDetails userDetails;

  Data({this.accessToken, this.userDetails});

  Data.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    userDetails = json['User Details'] != null
        ? new UserDetails.fromJson(json['User Details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accessToken'] = this.accessToken;
    if (this.userDetails != null) {
      data['User Details'] = this.userDetails.toJson();
    }
    return data;
  }
}

class UserDetails {
  String name;
  String email;
  String updatedAt;
  String createdAt;
  String sId;

  UserDetails(
      {this.name, this.email, this.updatedAt, this.createdAt, this.sId});

  UserDetails.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['_id'] = this.sId;
    return data;
  }
}
