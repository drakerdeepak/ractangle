class NewsListResponse {
  bool success;
  String message;
  int status;
  List<Data> data;

  NewsListResponse({this.success, this.message, this.status, this.data});

  NewsListResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String sId;
  String title;
  String date;
  String newsContent;
  String image;
  String sourceUrl;

  Data(
      {this.sId,
        this.title,
        this.date,
        this.newsContent,
        this.image,
        this.sourceUrl});

  Data.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    date = json['date'];
    newsContent = json['news_content'];
    image = json['image'];
    sourceUrl = json['source_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['date'] = this.date;
    data['news_content'] = this.newsContent;
    data['image'] = this.image;
    data['source_url'] = this.sourceUrl;
    return data;
  }
}
