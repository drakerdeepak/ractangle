import 'dart:io';

import 'package:Two/saadh_dev/core/service/independent_services/network_provider.dart';




class BaseRepository {
  final networkProvider = NetworkProvider();

  final headerContentTypeAndAccept = {
    HttpHeaders.contentTypeHeader: "application/json",
    HttpHeaders.acceptHeader: "application/json"
  };
  final Map<String, String> mapAuthHeader = {
    HttpHeaders.authorizationHeader: 'Bearer nhl8gwc8vtee0bookm2vbkw31enngxhj'
  };

  Map<String, String> buildDefaultHeaderWithToken(String token) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }
  Map<String, String> buildDefaultHeaderWithLocale(String locale) {
    Map<String, String> header = headerContentTypeAndAccept;
    header.remove(HttpHeaders.authorizationHeader);
    header.remove("X-localization");
    header.putIfAbsent("X-localization", () { return locale;});
    return header;
  }


  Map<String, String> buildOnlyHeaderWithToken(String token) {
    Map<String, String> header = {};
    header.remove(HttpHeaders.authorizationHeader);
    header.putIfAbsent(HttpHeaders.authorizationHeader, () => getFormattedToken(token));
    return header;
  }

  String getFormattedToken(String token) {
    return 'Bearer $token';
  }
}
