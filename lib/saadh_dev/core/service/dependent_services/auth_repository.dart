import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:Two/saadh_dev/core/service/dependent_services/base_repository/base_repository.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/login/login_request.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/login/login_response.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/registeraccount/register_account_request.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/registeraccount/register_account_response.dart';
import 'package:Two/saadh_dev/core/service/independent_services/app_url.dart';
import 'package:Two/saadh_dev/core/service/independent_services/method.dart';
import 'package:Two/saadh_dev/core/service/independent_services/network_provider.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_network_check.dart';



class AuthRepository extends BaseRepository {
  NetworkProvider _networkProvider;

  //api: Registration
  Future<RegisterAccountResponse> userRegistration(
      RegisterAccountRequest requestParams) async {
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      final response = await networkProvider.call(
          method: Method.POST,
          pathUrl: AppUrl.pathRegister,
          body: requestParams.toJson(),
          headers: headerContentTypeAndAccept);

      if (response.statusCode == HttpStatus.ok) {
        return RegisterAccountResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.notFound) {
        return RegisterAccountResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.unprocessableEntity) {
        return RegisterAccountResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.badRequest) {
        return RegisterAccountResponse.fromJson(json.decode(response.body));
      } else {
        //need to handel network connection error
        return null;
      }
    } else {
      return RegisterAccountResponse(
          message: AppConstants.ERROR_INTERNET_CONNECTION);
    }
  }

  //api: Login

  Future<LoginResponse> apiUserLogin(LoginRequest requestParams) async {
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      final response = await networkProvider.call(
          method: Method.POST,
          pathUrl: AppUrl.pathLogin,
          body: requestParams.toJson(),
          headers: headerContentTypeAndAccept);

      if (response.statusCode == HttpStatus.ok) {
        LoginResponse loginResponse =
        LoginResponse.fromJson(json.decode(response.body));
        loginResponse.success = true;
        return loginResponse;
      } else if (response.statusCode == HttpStatus.notFound) {
        return LoginResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.unprocessableEntity) {
        return LoginResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.badRequest) {
        return LoginResponse.fromJson(json.decode(response.body));
      } else {
        //need to handel network connection error
        return null;
      }
    } else {
      return LoginResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);
    }
  }


  //duplicate
  //api: Login
/*

  Future<LoginResponse> apiUserLogin(LoginRequest requestParams) async {
    bool isNetworkAvail = await NetworkCheck().check();
    if (isNetworkAvail) {
      final response = await networkProvider.call(
          method: Method.POST,
          pathUrl: AppUrl.pathLogin,
          body: requestParams.toJson(),
          headers: headerContentTypeAndAccept);

      if (response.statusCode == HttpStatus.ok) {
        LoginResponse loginResponse =
        LoginResponse.fromJson(json.decode(response.body));
        loginResponse.isSuccess = true;
        return loginResponse;
      } else if (response.statusCode == HttpStatus.notFound) {
        return LoginResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.unprocessableEntity) {
        return LoginResponse.fromJson(json.decode(response.body));
      } else if (response.statusCode == HttpStatus.badRequest) {
        return LoginResponse.fromJson(json.decode(response.body));
      } else {
        //need to handel network connection error
        return null;
      }
    } else {
      return LoginResponse(message: AppConstants.ERROR_INTERNET_CONNECTION);
    }
  }
*/


}
