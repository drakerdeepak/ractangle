import 'dart:collection';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/login/login_response.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_log_helper.dart';

import 'base/base_notifier.dart';

class CommonNotifier extends BaseNotifier {
  CommonNotifier._internal();

  static final CommonNotifier _singleInstance = CommonNotifier._internal();

  factory CommonNotifier() => _singleInstance;

  final _log = getLogger('CommonNotifier');

  //common needed values for app
  //final _commonRepository = CommonRepository();
  String _userToken = '';
  Data _userDetail;
  int _unReadCount;
  double _userCurrentLat;
  double _userCurrentLong;

  String get userToken => _userToken;

  set userToken(String token) {
    this._userToken = token.isNotEmpty ? token : '';
    log.i('updated : user token = ${this._userToken}');
  }

  Data get userDetail => _userDetail;

  set userDetail(Data value) {
    _userDetail = value;
    log.i('updated : userProfileInfo');
  }
  int get unReadCount => _unReadCount;

  set unReadCount(int unReadCount) {
    this._unReadCount = unReadCount;
    log.i('updated : unread Count = ${this._unReadCount}');
  }

  double get currentLocLat => _userCurrentLat;

  set currentLocLat(double userCurrentLat) {
    this._userCurrentLat = userCurrentLat;

  }
  double get currentLocLong => _userCurrentLong;

  set currentLocLong(double userCurrentLong) {
    this._userCurrentLong = userCurrentLong;

  }

}
