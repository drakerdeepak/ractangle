import'dart:io' show Platform;

import 'package:Two/saadh_dev/core/notifiers/base/base_notifier.dart';
import 'package:Two/saadh_dev/core/notifiers/common_notifier.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/auth_repository.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/login/login_request.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/login/login_response.dart';
import 'package:Two/saadh_dev/core/service/local/app_shared_preference.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_log_helper.dart';


class LoginNotifier extends BaseNotifier {

  final log = getLogger('LoginNotifier');

  final _repository = AuthRepository();

  Future<LoginResponse> apiLogin(
       String email,  String password ) async {
    log.i('api ::: apiLogin called');

    super.isLoading = true;
    LoginResponse response = LoginResponse();
    try {
      response = await _repository.apiUserLogin(LoginRequest(
           email: email, password: password));

      log.i('response : ${response.toString()}');
      super.isLoading = false;
    } catch (e) {
      log.e(e.toString());
      isLoading = false;
    }
    return response;
  }
/*
  Future<ResendOtpResponse> apiResendOtp(String resendEmail) async {
    log.i('api ::: apiResendOtp called');
    isLoading = true;
    ResendOtpResponse response = ResendOtpResponse();
    try {
      response = await _repository
          .resendOtp(ResendOtpRequest(resend_email: resendEmail));

      log.i('response : ${response.toString()}');
      isLoading = false;
    } catch (e) {
      log.e(e.toString());
      isLoading = false;
    }
    return response;
  }

  */
/*

  Future<LoginResponse> apiSocialLogin(
      String type, String email, String token, String password, String deviceType,String deviceToken) async {
    log.i('api ::: apiLogin called');
    super.isLoading = true;
    LoginResponse response = LoginResponse();
    try {
      response = await _repository.apiSocLogin(LoginRequest(
          type: type, email: email, token: token, password: password, deviceToken:deviceToken,deviceType:deviceType));

      log.i('response : ${response.toString()}');
      super.isLoading = false;
    } catch (e) {
      log.e(e.toString());
      isLoading = false;
    }
    return response;
  }
*/

  void saveUserToken(String token) async {
    CommonNotifier().userToken = token;
    bool isTokenSaved = await AppSharedPreference().saveUserToken(token);
    log.i('user token saved status: $isTokenSaved');
    String spUserToken = await AppSharedPreference().getUserToken();
    //update to common model for app use
    //CommonNotifier().userToken = spUserToken;
  //  log.i('test saved value from SP: $spUserToken');
  }

}
