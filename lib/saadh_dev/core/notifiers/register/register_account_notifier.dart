import 'package:Two/saadh_dev/core/service/dependent_services/auth_repository.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/registeraccount/register_account_request.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/registeraccount/register_account_response.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_log_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';



class CreateAccountNotifier extends ChangeNotifier {
  final log = getLogger('RegisterNotifier');

  final _repository = AuthRepository();
  bool _isLoading = false;

  get isLoading => _isLoading;

  set isLoading(value) {
    log.i('loading value is : $value)');
    _isLoading = value;
    notifyListeners();
  }

  //api registration
  Future<RegisterAccountResponse> apiRegisterUser(String email,  String password, String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var usertoken = await prefs.getString('token');
    log.i('api ::: apiRegisterUser called');
    isLoading = true;
    RegisterAccountResponse response = RegisterAccountResponse();
    try{
    response =
        await _repository.userRegistration(RegisterAccountRequest(email:email,  password: password , name:name ));

    log.i('respose : ${response.toString()}');
    isLoading = false;
    } catch (e) {
      log.e(e.toString());
      isLoading = false;
    }
    return response;
  }

}
