import 'package:Two/saadh_dev/core/notifiers/base/base_notifier.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/repository/news_repository.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/news_list/news_list_response.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_log_helper.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';



class NewsNotifier extends BaseNotifier {
  final log = getLogger('MY news notifier');

  final _repository = NewsRepository();
  bool _isLoading = false;
  String token= "";

  NewsListResponse _newsListGetResponse = NewsListResponse();

  TextEditingController _searchGroupEditText = TextEditingController();
  get isLoading => _isLoading;


  set isLoading(value) {
    log.i('loading value is : $value)');
    _isLoading = value;
    notifyListeners();
  }
  NewsListResponse get getMyNewsResponse => _newsListGetResponse;

  set getMyNewsResponse(NewsListResponse value) {
    _newsListGetResponse = value;
    notifyListeners();
  }

  TextEditingController get searchGroupEditText => _searchGroupEditText;

  set searchGroupEditText(TextEditingController value) {
    _searchGroupEditText = value;
  }
  NewsNotifier(BuildContext context) {
    super.context = context;
    apiMyGroupList();
 //   _searchGroupEditText.addListener(_searchGroupEditText);
  }



  void onMyNewsListResponse(NewsListResponse response) {
    if (response != null) {
      getMyNewsResponse = response;
    }
  }
  void onSearchGroupResponse(NewsListResponse searchResponse) {
    if (searchResponse != null) {
      getMyNewsResponse = searchResponse;
    }
  }

  //api news list api
  Future<NewsListResponse> apiMyGroupList() async {

    log.i('api ::: apiMyGroupList called');
    isLoading = true;
    NewsListResponse response = NewsListResponse();
    try{
      response =
      (await _repository.getNewsList(context,token));
      onMyNewsListResponse(response);
      log.i('respose : ${response.toString()}');
      isLoading = false;
    } catch (e) {
      log.e(e.toString());
      isLoading = false;
    }
    return response;
  }







}
