import 'dart:ui';

import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'app_font.dart';

//default style

TextStyle getStyleButton(context) => Theme.of(context).textTheme.button;

TextStyle getStyleOverLine(context) => Theme.of(context).textTheme.overline;

TextStyle getStyleCaption(context) => Theme.of(context).textTheme.caption;

TextStyle getStyleBody1(context) => Theme.of(context).textTheme.body1;

TextStyle getStyleBody2(context) => Theme.of(context).textTheme.body2;

TextStyle getStyleSubHeading(context) => Theme.of(context).textTheme.subhead;

TextStyle getStyleSubTitle(context) => Theme.of(context).textTheme.subtitle;

TextStyle getStyleTitle(context) => Theme.of(context).textTheme.title;

TextStyle getStyleHeading(context) => Theme.of(context).textTheme.headline;

TextStyle getStyleDisplay1(context) => Theme.of(context).textTheme.display1;

TextStyle getStyleDisplay2(context) => Theme.of(context).textTheme.display2;

TextStyle getStyleDisplay3(context) => Theme.of(context).textTheme.display3;


//login title
TextStyle getFormTitleStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightSemiBold,fontSize: 20);

//sub title
TextStyle getFormsubtitleStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightMedium,fontSize: 15);
//text
TextStyle getFormsettingTitleStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightSemiBold,color: Colors.white,fontSize: 22);
//sub title
TextStyle getFormasettingrticletitleStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightMedium,fontSize: 18,color: Colors.white);

// sub title with red color
TextStyle getFormsubtitleredStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightMedium,fontSize: 15,color: Colors.redAccent);
// sub title weih green
TextStyle getFormsubtitlegreenStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightMedium,fontSize: 15,color: Colors.green);

//button text
TextStyle getFormbuttonStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightSemiBold,fontSize: 17,color: Colors.white);
// follwing button
TextStyle getFormfollowStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightMedium,fontSize: 13,color: Colors.white);

//artical title
TextStyle getFormarticletitleStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightMedium,fontSize: 18,color: Colors.black);
//artical sub title
TextStyle getFormarticlesubtitleStyle(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightMedium,fontSize: 14,color: lightgraycolor,);



//style : app form style
TextStyle getAppFormTextStyle(context) => getStyleSubTitle(context).copyWith(
      color: colorBlack,
      fontWeight: AppFont.fontWeightSemiBold,
    );

//style : form label text style
TextStyle getFormLabelStyleText(context) =>
    getStyleSubTitle(context).copyWith(color: colorDarkGrey, fontWeight: AppFont.fontWeightSemiBold);

//style : form text content style
TextStyle getFormStyleText(context) =>
    getStyleSubTitle(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightSemiBold,);

//style : form card title style
TextStyle getFormTitleStylecard(context) => getStyleSubHeading(context).copyWith(fontWeight: AppFont.fontWeightSemiBold);

//style : for card normal text
TextStyle getFormNormalTextStyle(context) => getStyleCaption(context).copyWith(color: colorDarkGrey);

//style : for card text navigation
TextStyle getSmallTextNavigationStyle(context) =>
    getStyleBody2(context).copyWith(color: colorAccent, fontWeight: AppFont.fontWeightBold);

//style : app bar title text
TextStyle getAppBarTitleTextStyle(context) =>
    getStyleTitle(context).copyWith(color: colorWhite, fontWeight: AppFont.fontWeightSemiBold);

//style : app search text style
TextStyle getAppSearchTextStyle(context) => getStyleCaption(context).copyWith(
      fontStyle: FontStyle.italic,
    );

//style : app button text style
TextStyle getStyleButtonText(context) =>
    getStyleSubHeading(context).copyWith(color: colorWhite, fontWeight: AppFont.fontWeightSemiBold);

//**********product Item text style***********//

//style : product item name
TextStyle getProductNameTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightMedium);

//style : product view all text style
TextStyle getProductViewAllTextStyle(context) =>
    getStyleBody1(context).copyWith(color: colorPrimary, fontWeight: AppFont.fontWeightSemiBold);

//style : product amount text style
TextStyle getProductAmtTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorPrimary, fontWeight: AppFont.fontWeightSemiBold);

//style : product Quantity text style
TextStyle getProductQtyTextStyle(context) =>
    getStyleSubHeading(context).copyWith(color: colorPrimary, fontWeight: AppFont.fontWeightSemiBold);

//**********wishList text style***********//

//style: wish list product name text style
TextStyle getWLProductNameTextStyle(context) => getStyleBody2(context).copyWith(fontWeight: AppFont.fontWeightSemiBold);

//style: wish list product Quantity text style
TextStyle getWLProductAmtTextStyle(context) =>
    getStyleBody2(context).copyWith(color: colorPrimary, fontWeight: AppFont.fontWeightBold);

//style: wish list product strike amount text style
TextStyle getWLProductStrikeAmtTextStyle(context) =>
    getStyleBody1(context).copyWith(decoration: TextDecoration.lineThrough);

//style: wish list product strike amount text style
TextStyle getWLProductOfferAmtTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorFlashGreen, fontWeight: AppFont.fontWeightSemiBold);

//style: wish list product items QTY label text style
TextStyle getWLQtyLabelTextStyle(context) => getStyleCaption(context).copyWith(color: colorBlack);

//style: wish list product items QTY label text style
TextStyle getWLQtyTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorPrimary, fontWeight: AppFont.fontWeightSemiBold);

TextStyle getheadesubTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: Colors.redAccent, fontWeight: AppFont.fontWeightMedium);

TextStyle getFormLabelTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightSemiBold,fontSize: 15);

TextStyle getFormTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: Colors.red, fontWeight: AppFont.fontWeightSemiBold,fontSize: 13);


TextStyle getHeaderTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: Colors.red, fontWeight: AppFont.fontWeightBold,fontSize: 20.0);

TextStyle underLineTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: colorOrange, fontWeight: AppFont.fontWeightRegular,fontSize: 14,decoration: TextDecoration.underline,);

TextStyle fabTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: Colors.red, fontWeight: AppFont.fontWeightBold,fontSize:9,);

TextStyle getBoldTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: Colors.red, fontWeight: AppFont.fontWeightBold);

TextStyle getEventTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: colorOrange, fontWeight: AppFont.fontWeightRegular);

TextStyle getBoldTextWithColorRedStyle(context) =>
    getStyleSubTitle(context).copyWith(color: Colors.red, fontWeight: AppFont.fontWeightBold);

TextStyle fabTextRedStyle(context) =>
    getStyleSubTitle(context).copyWith(color: Colors.red, fontWeight: AppFont.fontWeightBold,fontSize:9,);


TextStyle getNameTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightSemiBold);
TextStyle getAppbarTitleTextStyle(context) =>
    getStyleSubTitle(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightMedium,fontSize: 18,);

TextStyle getDoneTextStyle(context) =>
    getStyleSubHeading(context).copyWith(color: colorOrange, fontWeight: AppFont.fontWeightBold);

TextStyle getRegGreyTextStyle(context) =>
    getStyleCaption(context).copyWith(color: Colors.grey, fontWeight: AppFont.fontWeightRegular);

TextStyle getFormLabelTextStylenotification(context) =>
    getStyleSubTitle(context).copyWith(color: colorOrange, fontWeight: AppFont.fontWeightMedium,fontSize: 15);

TextStyle getFormLabelTextStyleSplashColor(context) =>
    getStyleSubTitle(context).copyWith(color: colorPrimary, fontWeight: AppFont.fontWeightMedium,fontSize: 15);

TextStyle getCaptionBoldTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightBold);

TextStyle underLineRedTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorOrange, fontWeight: AppFont.fontWeightRegular,decoration: TextDecoration.underline,);

TextStyle getSmallCaptionRegTextStyle(context) =>
    getStyleCaption(context).copyWith(color: Colors.grey, fontWeight: AppFont.fontWeightMedium);

TextStyle getSmallRegBoldTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightBold,fontSize: 10,);

TextStyle getRegBlackTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightRegular);

TextStyle getMediumBlackTextStyle(context) =>
    getStyleCaption(context).copyWith(color: colorBlack, fontWeight: AppFont.fontWeightMedium);
