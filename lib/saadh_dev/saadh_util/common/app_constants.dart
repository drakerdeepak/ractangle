class AppConstants {
  //shared Preference
  static const String KEY_TOKEN_USER = 'user_token';
  static const String KEY_TOKEN_ADMIN = 'admin_token';
  static const String KEY_QUOTE_ID = 'quote_id';
  static const String KEY_CUSTOMER_ID = 'customer_id';
  static const String KEY_CURRENCY_CODE = "currency_code";

  static const String KEY_USER_EMAIL_ID = 'user_email_id';
  static const String KEY_USER_PASSWORD = 'user_password';
  static const String KEY_SHOULD_REMEMBER = 'user_should_remember';
  static const String KEY_FACEBOOK_Remembers = 'facebook_login';

  static const String KEY_CUSTOMER_NAME = 'customer_name';
  static const String KEY_CUSTOMER_MAILID = 'customer_mail_id';
  static const String KEY_MOBILE_NUMBER = 'customer_mobilenumber';
  static const String Week = 'Week';

  static const String CAMERA = 'Capture Camera';
  static const String GALLERY = 'Pick from Gallery';

  //SnackBar ShowUp Time In seconds
  static const int TIME_SHOW_SNACK_BAR = 5;
  static const String IMAGE_PROFILE_PROTOCOL =
      'Image size should be 256x256'; // min 150x110


  //error
  static const String ERROR_INTERNET_CONNECTION = 'Network Connection Error !!';
  static const String ERROR = 'Something Went Wrong !!';

  //response status
  static const int RESPONSE_STATUS_SUCCESS = 1;
  static const int RESPONSE_STATUS_FAILED = 0;

  static const String LOGIN = "LOGIN";
  static const String SUBMIT = "SUBMIT";
  static const String COMPARE = "Compare";
  static const String FILTER = "Filter";
  static const String ADDED = "Added";
  static const String NEXT = "Next";
  static const String EMAIL = "Email";
  static const String PASSWORD = "Password";
  static const String FORGOT_PASSWORD = "Forgot Password";
  static const String RESET_PASSWORD = 'Reset Password';
  static const String NEW_PASSWORD = 'New Password';
  static const String CURRENT_PASSWORD = 'Current Password';
  static const String CONFIRM_PASSWORD = 'Confirm Password';
  static const String CANCEL_ORDER = "Cancel Order";
  static const String PAYMENT = 'Review & Payment';
  static const String PAY_NOW = 'Pay Now';
  static const String SHIPPING = 'Shipping';
  static const String SHIPPING_ADDRESS = 'Shipping Address';
  static const String E_MAIL_ADDRESS = 'Email Address';
  static const String FIRST_NAME = 'First name';
  static const String LAST_NAME = 'Last name';
  static const String COMPANY = 'Company';
  static const String STREET_ADDRESS = 'Street Address';
  static const String STREET_ADDRESS_ONE = 'Street Address 1';
  static const String STREET_ADDRESS_TWO = 'Street Address 2';
  static const String CITY = 'City';
  static const String STATE_PROVINCE = 'State / Province';
  static const String ZIP_POSTAL_CODE = 'Zip / Postal Code';
  static const String FAX = 'Fax';
  static const String COUNTRY = 'Country';
  static const String PHONE = 'Phone';
  static const String PHONE_NUMBER = 'Mobile Number';
  static const String TRN = 'TRN #';
  static const String CHANGE_PASSWORD = 'Change Password';
  static const String EDIT_PROFILE = 'Edit Profile';
  static const String INVOICE = 'Invoice';
  static const String ORDER_SUMMARY = 'Order Summary';
  static const String PRODUCTS = 'Product';
  static const String QUANTITY = 'Quantity';
  static const String PRICE = 'Price';
  static const String REVIEW = 'Review';
  static const String CONTACT_DETAILS = 'Contact Details';
  static const String PRODUCT_DETAILS = 'Product Details';
  static const String DELIVERY_TYPE = 'Delivery Type';
  static const String FLAT_RATE = 'Flat Rate';
  static const String ADD_NEW_CARD = 'Add New Card';
  static const String ADD_TO_CART = 'Add to Cart';
  static const String ADD_NEW_ADDRESS = 'Add New Address';
  static const String ADD_ADDRESS = 'Add Address';
  static const String EDIT_ADDRESS = 'Edit Address';
  static const String OK = 'Ok';
  static const String YES = 'Yes';
  static const String NO = 'No';
  static const String ARE_YOU_SURE_CANCEL_ORDER =
      'Are you sure to cancel this order?';
  static const String PASSWORD_FIELD_INPUT_PROTOCOL =
      'Should contain minimum 8 characters which includes atleast 1 uppercase, 1 lowercase, 1 digit, 1 special character.';
  static const int PASSWORD_FIELD_MIN_LENGTH = 8;


// login page
  static const String loginwelcome = 'Welcome Back!';
  static const String loginwelcomesubtitle = 'Please enter your account here';
  static const String loginuseremail = 'Email or phone number';
  static const String loginuserpassword = 'Password';
  static const String loginuserForgotpassword = 'Forgot password?';
  static const String login = 'LOG IN';
  static const String loginNothaveacount = 'Don’t have any account?';
  static const String signup = 'Sign Up';

//signup
  static const String signupfirstname = 'First Name';
  static const String signuplastname = 'Last Name';

  static const String signuploginname = 'User Name';
  static const String signupuseremail = 'Email address';
  static const String signupuserphone = 'Phone number';
  static const String signupsignup = 'SIGN UP';
  static const String signalready = 'Have an Account Already?';
  static const String loginin = 'Log in';

  //otp screen
  static const String Otpverifcation = 'Verification';
  static const String otpverficationsubtitle = 'You will get a  OTP via a SMS';
  static const String otpcodeexprie = 'code expires in:';
  static const String otpcodeexprietime = '03:12';
  static const String otpverfiy = 'Verify';
  static const String otpnotget = 'Didn’t get the OTP? ';
  static const String otpresend = 'Resend OTP';

  //forgot password
  static const String forgottitle = 'Forgot Password';
  static const String forgotsubtitle = 'Enter your Phone Number to recover your password';
  static const String send = 'Send';

  //reset password
  static const String resetYourpassword = 'Reset Your Password';
  static const String resetpasswordsubtitle = 'Please enter your new password';
  static const String resetpassword = 'Reset Password';
  static const String resetYourconfrompassword = 'Confirm Password';

  //home view
  static const String news = 'News';
  static const String Article = 'Article';
  static const String community = 'Community';
  static const String profile = 'Profile';



}
