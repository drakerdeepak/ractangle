import 'package:Two/aplication_localization.dart';
import 'package:Two/common/widgets/test.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/slide_right_roure.dart';
import 'package:Two/view/app_base/app_base.dart';
import 'package:Two/view/settings_screen/settings.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

Widget buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: colorWhite.withOpacity(0.3),
    elevation: 0.0,
    leading: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Icon(
          Icons.arrow_back_ios,
          size: 20.0,
        )),
  );
}

Widget buildAppBarWithTitle(BuildContext context, String text) {
  return AppBar(
    backgroundColor: colorWhite.withOpacity(0.3),
    elevation: 0.0,
    title: Text(
      text,
      style: getAppbarTitleTextStyle(context),
    ),
    titleSpacing: 0.0,
    leading: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Icon(
          Icons.arrow_back_ios,
          size: 20.0,
        )),
  );
}

Widget buildAppBarWithAll(
  BuildContext context,
) {
  return AppBar(
      backgroundColor: appbarcolor,
      automaticallyImplyLeading: false,
      centerTitle: true,
      elevation: 0.0,
      title: Image.asset('assets/images/img_logo.png',scale: 4.0,),
      titleSpacing: 0.0,
      leading:IconButton(
        icon: Icon(
          Icons.settings,
          color: appcolor,
          size: 25.0,
        ),
        onPressed: () {
          Navigator.pushReplacement(
            context,
            SlideRightRoute(page: SettingsScreen()), ).whenComplete(() {
            final provider = Provider.of<FeedProvider>(
                context, listen: false);

            final settingprovider = Provider.of<SettingsProvider>(context, listen: false);
            String languagecode = AppLocalizations.of(context).locale.languageCode;
            print(" languagecode "+languagecode);
            provider.getAllCategoriesList(languagecode);
            provider.getAllNewsList(languagecode);
            SystemChrome.setSystemUIOverlayStyle(
              SystemUiOverlayStyle(
                systemNavigationBarColor:settingprovider.isDarkThemeOn ? Colors.white
                    : Colors.black ,
                statusBarColor: settingprovider.isDarkThemeOn ? Colors.white
                    : Colors.black ,
                statusBarIconBrightness: settingprovider.isDarkThemeOn ? Brightness.dark
                    : Brightness.light,
                statusBarBrightness: settingprovider.isDarkThemeOn ? Brightness.dark
                    : Brightness.light,
                systemNavigationBarIconBrightness: settingprovider.isDarkThemeOn ? Brightness.dark
                    : Brightness.light,
              ),
            );
          }
          );
          // Navigator.of(context).pop();
        },
      ),
      actions: [

        IconButton(
          icon:  Image.asset(
            'assets/new_icons/search.png',
            //CustomIcon.comments_like,
            color: Colors.black,
            height: 25,width: 20,
            // fit: BoxFit.fill,
          ),
          onPressed: () {},
        ),
        IconButton(
          icon:  Image.asset(
            'assets/new_icons/refresh.png',
            //CustomIcon.comments_like,
            color: Colors.black,
            height: 22,width: 22,
            fit: BoxFit.fill,
          ),
          onPressed: () {
            Navigator.push(
              context,
              SlideRightRoute(page: AppBase()), ).whenComplete(() {
              final provider = Provider.of<FeedProvider>(
                  context, listen: false);
              String languagecode = AppLocalizations.of(context).locale.languageCode;
              print(" languagecode "+languagecode);
              provider.getAllCategoriesList(languagecode);
              provider.getAllNewsList(languagecode);
            }

            );
            // Navigator.of(context).pop();
          },
        ),
        IconButton(
          icon:  Image.asset(
            'assets/new_icons/bell.png',
            //CustomIcon.comments_like,
            color: Colors.black,
            height: 25,width: 20,
          ),
          onPressed: () {
           /* Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => TestClass()),);*/
          },
        ),
      ]);
}

Widget buildAppBarWithTitleHome(
    BuildContext context,
    String text,
    int unReadCount,
    IconButton search,
    IconButton notification,
    IconButton message) {
  print("unread Count :" + unReadCount.toString());
  return AppBar(
    automaticallyImplyLeading: false,
    backgroundColor: Colors.white,
    elevation: 0.0,
    title: Padding(
      padding: const EdgeInsets.all(20.0),
      child: Text(
        text,
        style: getAppbarTitleTextStyle(context),
      ),
    ),
    titleSpacing: 0.0,
    actions: [
      IconButton(
        icon: search,
        onPressed: () {},
      ),
      Stack(alignment: Alignment.centerLeft, children: [
        IconButton(
          icon: notification,
          onPressed: () {},
        ),
        Positioned(
          right: 10,
          top: 12,
          child: Container(
            padding: EdgeInsets.all(1),
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(6),
            ),
            constraints: BoxConstraints(
              minWidth: 14,
              minHeight: 14,
            ),
            child: new Text(
              unReadCount != null ? unReadCount.toString() : "0",
              style: new TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        )
      ]),
      IconButton(
        icon: message,
        onPressed: () {},
      ),
    ],
  );
}

Widget buildAppBarWithoutArrow(BuildContext context, String text) {
  return AppBar(
    backgroundColor: colorWhite.withOpacity(0.3),
    elevation: 0.0,
    title: Text(
      text,
      style: getAppBarTitleTextStyle(context),
    ),
  );
}
