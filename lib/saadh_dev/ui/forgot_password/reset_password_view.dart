import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:flutter/material.dart';


class ResetPasswordView extends StatefulWidget {

  @override
  _State createState() => _State();
}

class _State extends State<ResetPasswordView> {
  TextEditingController nameController = TextEditingController();
  TextEditingController email_phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  bool _isPasswordVisible = false;

  void _resetpasswordbutton(BuildContext context) {
    Navigator.pushNamed(context, RoutePaths.LoginviewRoute);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.only(top: 10,right: 20.0,left: 20.0,bottom: 10.0),
          child: _resetpassword()
      ),
    );
  }


  Widget _resetpassword(){
    return ListView(
      children: <Widget>[
        SizedBox(height: 50.0,),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top:50),
          child: Text(
            AppConstants.resetYourpassword,
            style: getFormTitleStyle(context),
          ),
        ),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top:8.0),
          child: Text(
            AppConstants.resetpasswordsubtitle,
            textAlign: TextAlign.center,
            style: getFormsubtitleStyle(context),
          ),
        ),
        SizedBox(height: 50.0,),
        Container(
          padding: EdgeInsets.only(top:20),
          child: TextField(
            controller: passwordController,
            obscureText: !_isPasswordVisible,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: AppConstants.loginuserpassword,
              prefixIcon: Icon(
                CustomIcon.lock_login,
                color: appcolor,
              ),
              suffixIcon: IconButton(
                icon: Icon(_isPasswordVisible
                    ? CustomIcon.show_password
                    : Icons.visibility_off),
                onPressed: () {
                  setState(() {
                    _isPasswordVisible =
                    !_isPasswordVisible;
                  });
                },
              ),
            ),

          ),
        ),
        Container(
          padding: EdgeInsets.only(top:20),
          child: TextField(
            controller: passwordController,
            obscureText: !_isPasswordVisible,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: AppConstants.resetYourconfrompassword,
              prefixIcon: Icon(
                CustomIcon.lock_login,
                color: appcolor,
              ),
              suffixIcon: IconButton(
                icon: Icon(_isPasswordVisible
                    ? CustomIcon.show_password
                    : Icons.visibility_off),
                onPressed: () {
                  setState(() {
                    _isPasswordVisible =
                    !_isPasswordVisible;
                  });
                },
              ),
            ),

          ),
        ),

        SizedBox(height: 40.0,),
        Container(
            height: 50,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top:0),
            child: RaisedButton(
              textColor: Colors.white,
              color: appcolor,
              child: Text(AppConstants.resetpassword,style: getFormbuttonStyle(context),),
              onPressed: () {
                _resetpasswordbutton(context);
                print(email_phoneController.text);
                print(passwordController.text);
              },
            )),
        SizedBox(height: 20.0,),

      ],
    );

  }
}