import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:Two/saadh_dev/ui/forgot_password/forgot_password_otp.dart';
import 'package:Two/saadh_dev/ui/reg/singup_view.dart';
import 'package:flutter/material.dart';


class ForgotPasswordView extends StatefulWidget {

  @override
  _State createState() => _State();
}

class _State extends State<ForgotPasswordView> {
  TextEditingController nameController = TextEditingController();
  TextEditingController email_phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  bool _isPasswordVisible = false;


  void _onClickTextsignup(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                SingupView()));;
  }

  void _onClickTextsend(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ForgotPasswordOtpView()));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.only(top: 10,right: 20.0,left: 20.0,bottom: 10.0),
          child: _forgotpassword()
      ),
    );
  }


  Widget _forgotpassword(){
    return ListView(
      children: <Widget>[
        SizedBox(height: 50.0,),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top:50),
          child: Text(
            AppConstants.forgottitle,
            style: getFormTitleStyle(context),
          ),
        ),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top:8.0),
          child: Text(
            AppConstants.forgotsubtitle,
            textAlign: TextAlign.center,
            style: getFormsubtitleStyle(context),
          ),
        ),
       SizedBox(height: 50.0,),
        Container(
          padding: EdgeInsets.only(top: 20),
          child: TextField(
            controller: phoneController,
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: AppConstants.signupuserphone,
              prefixIcon:  Icon(
                CustomIcon.call_reg,
                color: appcolor,
              ),
            ),
          ),
        ),

        SizedBox(height: 40.0,),
        Container(
            height: 50,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top:0),
            child: RaisedButton(
              textColor: Colors.white,
              color: appcolor,
              child: Text(AppConstants.send,style: getFormbuttonStyle(context),),
              onPressed: () {
                // Navigator.of(context).pushNamed(RoutePaths.OtpVerification);

                _onClickTextsend(context);
                print(email_phoneController.text);
                print(passwordController.text);
              },
            )),
        SizedBox(height: 20.0,),
        Container(
            child: Row(
              children: <Widget>[
                Text(AppConstants.loginNothaveacount,style:getFormsubtitleStyle(context)),
                FlatButton(
                  textColor: appcolorlight,
                  child: Text(
                    AppConstants.signup,
                    style: getFormTitleStyle(context),
                  ),
                  onPressed: () {
                    _onClickTextsignup(context);
                    //signup screen
                  },
                )
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ))
      ],
    );

  }
}