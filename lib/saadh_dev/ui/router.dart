import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/ScaleRoute.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/slide_right_roure.dart';
import 'package:Two/saadh_dev/ui/forgot_password/forgot_password_otp.dart';
import 'package:Two/saadh_dev/ui/forgot_password/forgot_password_view.dart';
import 'package:Two/saadh_dev/ui/forgot_password/reset_password_view.dart';
import 'package:Two/saadh_dev/ui/home/home_view.dart';
import 'package:Two/saadh_dev/ui/home/post_article/hash_popup_view.dart';
import 'package:Two/saadh_dev/ui/home/post_article/post_article_view.dart';
import 'package:Two/saadh_dev/ui/home/profile/profile_edit_view.dart';
import 'package:Two/saadh_dev/ui/login/login_view.dart';
import 'package:Two/saadh_dev/ui/otp/otp_view.dart';
import 'package:Two/saadh_dev/ui/reg/singup_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';



class MyRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutePaths.HomeviewRoute:
        return ScaleRoute(page: HomeView());
      case RoutePaths.LoginviewRoute:
        return SlideRightRoute(page: LoginView());
      case RoutePaths.RegisterviewRoute:
        return SlideRightRoute(page: SingupView());
      case RoutePaths.ForgotpasswordviewRoute:
        return SlideRightRoute(page: ForgotPasswordView());
      case RoutePaths.ForgotOtpviewRoute:
        return SlideRightRoute(page: ForgotPasswordOtpView());
      case RoutePaths.ForgotOtpRestPassword:
        return SlideRightRoute(page: ResetPasswordView());
      case RoutePaths.SingupOtp:
        return SlideRightRoute(page: SignupOTPView());
      case RoutePaths.profileedit:
        return SlideRightRoute(page: EditPorfileView());
      case RoutePaths.fabPostArticle:
        return ScaleRoute(page: PostArticleView());
      case RoutePaths.Hashpopup:
        return ScaleRoute(page: HashPopUpView());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
