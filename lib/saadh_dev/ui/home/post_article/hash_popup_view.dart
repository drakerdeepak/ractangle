import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_images.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:flutter/material.dart';


class HashPopUpView extends StatefulWidget {
  @override
  _HashPopUpViewState createState() => _HashPopUpViewState();
}

class _HashPopUpViewState extends State<HashPopUpView> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      elevation: 16,
      child: Container(
        padding: new EdgeInsets.only(top: 15.0, right: 10.0, left: 20.0),
        //height: MediaQuery.of(context).size.height,
        height: 500,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildWidgetTitle(context),
            _buildWidgetSearch(context),
            _buildWidgetSelected(context),
            _buildWidgetuser(context),
          ],
        ),
      ),
    );
  }

  Widget _buildWidgetTitle(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
              left: 10.0, top: 10.0, right: 10.0, bottom: 5.0),
          child: Text(
            "Tag People",
            style: getFormarticletitleStyle(context),
          ),
        ),
        Divider()
      ],
    );
  }

  Widget _buildWidgetSearch(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: const EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
            child: Container(
              height: 45.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: createpostcolor,
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              child: new TextField(
                keyboardType: TextInputType.multiline,
                maxLines: 1,
                textAlign: TextAlign.start,
                decoration: new InputDecoration(
                  contentPadding: EdgeInsets.all(12.0),
                  hintText: 'Search User',
                  border: InputBorder.none,
                  prefixIcon: Icon(Icons.search),
                ),
              ),
            )),
      ],
    );
  }

  _buildWidgetSelected(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 80,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: 22,
            itemBuilder: (context, position) {
              return _selecteduseritem();
            },
          ),
        ),
      ],
    );
  }

  _selecteduseritem() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
            onPressed: () {},
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10.0),
            elevation: 2,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
            child: Row(
              children: [
                Text(
                  "Jacob_jones",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 15.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  //list of user
  _buildWidgetuser(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 280,
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: 22,
            itemBuilder: (context, position) {
              return _userlistitem();
            },
          ),
        ),
      ],
    );
  }

  _userlistitem() {
    return Column(
      children: [
        Row(children: [
          Container(
            width: 65.0,
            height: 65.0,
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                fit: BoxFit.cover,
                image: new AssetImage(AppImages.proimage),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "jacob_jones",
                  style: getFormarticletitleStyle(context),
                ),
                Text(
                  "jacob jones",
                  style: getFormarticlesubtitleStyle(context),
                ),
              ],
            ),
          ),
        ]),
      ],
    );
  }
}
