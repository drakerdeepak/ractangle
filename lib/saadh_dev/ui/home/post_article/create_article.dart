import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_images.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/ui/home/post_article/hash_popup_view.dart';
import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class CreateArticleTabScreen extends StatefulWidget {
  @override
  _CreatePostTabState createState() => _CreatePostTabState();
}

void _onClickHash(BuildContext context) {
  Navigator.pushNamed(context, RoutePaths.Hashpopup);
}

class _CreatePostTabState extends State<CreateArticleTabScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _articleviewlist(),
              ],
            ),
          ),
        ));
  }

  _articleviewlist() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          Container(
            width: 65.0,
            height: 65.0,
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                fit: BoxFit.cover,
                image: new AssetImage(AppImages.proimage),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left:15.0),
            child: Text(
              "What’s on your mind, Jane Cooper",
              style: getFormarticletitleStyle(context),
            ),
          ),
        ]),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            SizedBox(
              height: 10.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: const EdgeInsets.only(left: 0.0, top: 5.0),
                    child: Container(
                      height: 60.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: createpostcolor,
                        borderRadius: BorderRadius.all(Radius.circular(6)),

                      ),
                      child: new TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        textAlign: TextAlign.start,
                        decoration: new InputDecoration(
                          contentPadding: EdgeInsets.all(12.0),
                          hintText: 'Post Heading',
                          border: InputBorder.none,
                        ),
                      ),
                    )),
              ],
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            SizedBox(
              height: 10.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: const EdgeInsets.only(left: 0.0, top: 5.0),
                    child: Container(
                      height: 200.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: createpostcolor,
                        borderRadius: BorderRadius.all(Radius.circular(6)),

                      ),
                      child: new TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        textAlign: TextAlign.start,
                        decoration: new InputDecoration(
                          contentPadding: EdgeInsets.all(12.0),
                          hintText: 'What’s on your Mind..',
                          border: InputBorder.none,
                        ),
                      ),
                    )),
              ],
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            SizedBox(
              height: 10.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: const EdgeInsets.only(left: 0.0, top: 5.0),
                    child: Container(
                      height: 60.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                        border: new Border.all(
                          color: Colors.grey,
                          width: 1.0,
                        ),

                      ),
                      child: Row(
                        children: [
                          new Flexible(
                            child: new TextField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              readOnly: true,
                              textAlign: TextAlign.start,
                              decoration: new InputDecoration(
                                contentPadding: EdgeInsets.all(12.0),
                                hintText: 'Select your Category',
                                border: InputBorder.none,
                              ),
                            ),
                          ),

                          IconButton(
                            onPressed: () {
                              // You enter here what you want the button to do once the user interacts with it
                            },
                            icon: Icon(
                              Icons.keyboard_arrow_down_rounded,
                              color: lightgraycolor,
                            ),
                            iconSize: 25.0,
                          ),

                        ],
                      ),
                    )),
              ],
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            SizedBox(
              height: 10.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: const EdgeInsets.only(left: 0.0, top: 5.0),
                    child: Container(
                      height: 60.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                        border: new Border.all(
                          color: Colors.grey,
                          width: 1.0,
                        ),

                      ),
                      child:  Row(
                        children: [
                          new Flexible(
                            child: new TextField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              readOnly: true,
                              textAlign: TextAlign.start,
                              decoration: new InputDecoration(
                                contentPadding: EdgeInsets.all(12.0),
                                hintText: 'Add to your post',
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              // You enter here what you want the button to do once the user interacts with it
                            },
                            icon: FaIcon(FontAwesomeIcons.image,color: lightgraycolor,),
                            iconSize: 20.0,
                          ),
                          IconButton(
                            onPressed: () {
                              
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return HashPopUpView();
                                },
                              );
                              // You enter here what you want the button to do once the user interacts with it
                            },
                            icon: FaIcon(FontAwesomeIcons.hashtag,color: lightgraycolor,),
                            iconSize: 20.0,
                          ),

                          IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return HashPopUpView();
                                },
                              );
                              // You enter here what you want the button to do once the user interacts with it
                            },
                            icon: Icon(
                              Icons.alternate_email_sharp,
                              color: lightgraycolor,
                            ),
                            iconSize: 20.0,
                          ),

                        ],
                      ),
                    )),
              ],
            ),
          ],
        ),

        SizedBox(
          height: 20.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            RaisedButton(
              onPressed: () {},
              padding: EdgeInsets.symmetric(horizontal: 50,vertical: 15.0),
              elevation: 2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6)),
              child: Text(
                "POST",
                style: TextStyle(
                    fontSize: 14,
                    letterSpacing: 2.2,
                    color: Colors.white),
              ),
            ),
            RaisedButton(
              onPressed: () {},
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 50,vertical: 15.0),
              elevation: 2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6)),
              child: Text(
                "DRAFT",
                style: TextStyle(
                    fontSize: 14,
                    letterSpacing: 2.2,
                    color: appcolor),
              ),
            )
          ],
        ),



      ],
    );
  }

}
