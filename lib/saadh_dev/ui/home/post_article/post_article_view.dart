import 'package:Two/saadh_dev/saadh_util/helper_widget/app_bar.dart';
import 'package:Two/saadh_dev/ui/home/post_article/create_article.dart';
import 'package:Two/saadh_dev/ui/home/post_article/create_post.dart';
import 'package:flutter/material.dart';

class PostArticleView extends StatefulWidget {
  @override
  _PostArticleViewState createState() => _PostArticleViewState();
}

class _PostArticleViewState extends State<PostArticleView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBarWithAll(context),

    body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: _Timelineview()
    )
    );
  }

  _Timelineview(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                SizedBox(height: 20.0),

                DefaultTabController(
                    length: 2, // length of tabs
                    initialIndex: 0,
                    child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                      Container(
                        child: TabBar(
                          labelColor: Colors.black,
                          unselectedLabelColor: Colors.grey,
                          tabs: [
                            Tab(text: 'POST'),
                            Tab(text: 'ARTICLE'),
                          ],
                        ),
                      ),
                      Container(
                          height: MediaQuery.of(context).size.width * 1.65,
                          decoration: BoxDecoration(
                              border: Border(top: BorderSide(color: Colors.grey, width: 0.5))
                          ),
                          child: TabBarView(children: <Widget>[
                            Container(
                                child:CreatePostTabScreen()
                            ),
                            Container(
                                child: CreateArticleTabScreen()
                            ),

                          ])
                      )
                    ])
                ),
                //create tabs here
              ]),
            ),
            ],
        ),

      ],
    );

  }

}
