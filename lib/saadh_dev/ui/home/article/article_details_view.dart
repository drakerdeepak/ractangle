import 'dart:math';

import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_images.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/app_bar.dart';
import 'package:flutter/material.dart';



class ArticleDetailsView extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<ArticleDetailsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBarWithAll(context),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(left:10.0,right:10.0,top:5.0,bottom:5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget> [
                articledetailsview(),
                SizedBox(height: 20.0,),
                _Otheraritclefollowview(),
                articleviewlistview(),

                SizedBox(height: 20.0,),
                _treadndingonview(),
                articleviewlistview(),



              ],
            ),
          ),
        )


    );
  }
  articledetailsview(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
        children: [
      Row(children: [
        Container(
          width: 65.0,
          height: 65.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            image: new DecorationImage(
              fit: BoxFit.cover,
              image: new AssetImage(AppImages.proimage),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            "Jane Cooper",
            style: getFormarticletitleStyle(context),
          ),
        ),


      ]),
          Text(
            "Lorem Ipsum is simply dummy printing industry.",
            maxLines: 2,
            style: getFormarticletitleStyle(context),
          ),
          SizedBox(height: 10.0,),
          Container(
            height: 180.0,
              width: MediaQuery.of(context).size.width ,
              child: Image.asset(AppImages.Defultimage1,)),
          SizedBox(height: 10.0,),
          Text(
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",

            textAlign: TextAlign.justify,
            style: getFormarticlesubtitleStyle(context),
          ),
          SizedBox(height: 10.0,),
          Text(
            "#Bitcoin   #Social   #Nature   #Hashtaglist",
          ),
          SizedBox(height: 10.0,),
          Text(
            "@naval   @riyo    ",
          ),
          SizedBox(height: 10.0,),

          Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                  child: Container(
                    padding: EdgeInsets.only(left:5.0,right: 5.0),
                    width: MediaQuery.of(context).size.width ,
                    decoration: BoxDecoration(
                      color: appbarcolor,
                      borderRadius: BorderRadius.all(Radius.circular(25.0),),),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(left:10.0,right: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(children: [
                              IconButton(
                                onPressed: () {
                                  // You enter here what you want the button to do once the user interacts with it
                                },
                                icon: Icon(
                                  Icons.favorite_border_outlined,
                                  //CustomIcon.like_icon,
                                  color: Colors.black,
                                ),
                                iconSize: 25.0,
                              ),
                              Text("2.4k"),
                            ]),
                            Row(children: [
                              IconButton(
                                onPressed: () {
                                  // You enter here what you want the button to do once the user interacts with it
                                },
                                icon: Icon(
                                  Icons.chat_bubble_outline,
                                  //CustomIcon.comments_like,
                                  color: Colors.black,
                                ),
                                iconSize: 25.0,
                              ),
                              Text("175"),
                            ]),
                            Transform.rotate(
                              angle: 180 * pi / 120,
                              child: IconButton(
                                onPressed: () {
                                  // You enter here what you want the button to do once the user interacts with it
                                },
                                icon: Icon(
                                  Icons.repeat_outlined,
                                  //CustomIcon.group_icon,
                                  color: Colors.black,
                                ),
                                iconSize: 25.0,
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                // You enter here what you want the button to do once the user interacts with it
                              },
                              icon: Icon(
                                Icons.share_outlined,
                                // CustomIcon.share_icon,
                                color: Colors.black,
                              ),
                              iconSize: 25.0,
                            ),
                            IconButton(
                              onPressed: () {
                                // You enter here what you want the button to do once the user interacts with it
                              },
                              icon: Icon(
                                Icons.bookmark_border,
                                //CustomIcon.flag_icon,
                                color: Colors.black,
                              ),
                              iconSize: 25.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ]),
          Divider(color: lightgraycolor,)
        ],
    );

  }


  articleviewlistview(){
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: 1,
      itemBuilder: (context, position) {
        return _articleviewlist();
      },
    );

  }

  _articleviewlist() {
    return Column(children: [
      Row(children: [
        Container(
          width: 65.0,
          height: 65.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            image: new DecorationImage(
              fit: BoxFit.cover,
              image: new AssetImage(AppImages.proimage),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            "Jane Cooper",
            style: getFormarticletitleStyle(context),
          ),
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text('Follow +'),
          style: ElevatedButton.styleFrom(
            shape: StadiumBorder(),
            primary: appcolor,
            onPrimary: Colors.white,
            textStyle: TextStyle(
              fontSize: 15,
              color: Colors.redAccent,
            ),
          ),
        ),

      ]),
      Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: 108.0,
              height: 102.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    image: new AssetImage(AppImages.Defultimage)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left:10.0,right: 5.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        width: 220.0,
                        child: Text(
                          "Lorem Ipsum is simply dummy printing industry.",
                          maxLines: 2,
                          style: getFormarticletitleStyle(context),
                        )),
                    SizedBox(height: 8.0,),
                    Container(
                      width: 220.0,
                      child: Text(
                        "It is a long established fact readable content of a page when that a reader will be distracted by the readable content of a page when looking at its reader will be  layout..",
                        maxLines: 4,
                        textAlign: TextAlign.justify,
                        style: getFormarticlesubtitleStyle(context),
                      ),
                    ),
                  ]),
            ),
          ]),
      SizedBox(
        height: 20.0,
      ),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          height: 50.0,
          width: 350.0,
          color: Colors.transparent,
          child: Container(
            decoration: BoxDecoration(
                color: appbarcolor,
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(children: [
                    IconButton(
                      onPressed: () {
                        // You enter here what you want the button to do once the user interacts with it
                      },
                      icon: Icon(
                        CustomIcon.like_icon,
                        color: Colors.black,
                      ),
                      iconSize: 20.0,
                    ),
                    Text("2.4k"),
                  ]),
                  Row(children: [
                    IconButton(
                      onPressed: () {
                        // You enter here what you want the button to do once the user interacts with it
                      },
                      icon: Icon(
                        CustomIcon.comments_like,
                        color: Colors.black,
                      ),
                      iconSize: 20.0,
                    ),
                    Text("175"),
                  ]),
                  IconButton(
                    onPressed: () {
                      // You enter here what you want the button to do once the user interacts with it
                    },
                    icon: Icon(
                      CustomIcon.group_icon,
                      color: Colors.black,
                    ),
                    iconSize: 20.0,
                  ),
                  IconButton(
                    onPressed: () {
                      // You enter here what you want the button to do once the user interacts with it
                    },
                    icon: Icon(
                      CustomIcon.share_icon,
                      color: Colors.black,
                    ),
                    iconSize: 20.0,
                  ),
                  IconButton(
                    onPressed: () {
                      // You enter here what you want the button to do once the user interacts with it
                    },
                    icon: Icon(
                      CustomIcon.flag_icon,
                      color: Colors.black,
                    ),
                    iconSize: 20.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
      SizedBox(height: 10.0,),
      Row(
        children: [
          Icon(CustomIcon.date_icon),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("05-02-2021"),
          ),
          Container(
            color: Colors.black45,
            height: 20,
            width: 2,
          ),
          SizedBox(
            width: 8.0,
          ),
          Icon(CustomIcon.time_icon),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("5 Minutes to read"),
          )
        ],
      ),
      Divider(color: lightgraycolor,)
    ],

    );
  }



  _Otheraritclefollowview(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Other Articles Of Same Author",style: getFormarticletitleStyle(context),),
        Expanded(
            child: Divider(height: 5,thickness: 2,color: dividercolor,)
        ),

        SizedBox(width: 10.0,),

      ],
    );

  }



  //people follwo
  _peoplefollowview(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("People to Follow",style: getFormarticletitleStyle(context),),
        Expanded(
            child: Divider(height: 5,thickness: 2,color: dividercolor,)
        ),
        Container(
          padding: const EdgeInsets.all(4),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: lightgraycolor,
          ),
          child: Icon(
            Icons.chevron_left_outlined,
            size: 20.0,
            color: appcolor,
          ),
        ),
        SizedBox(width: 6.0,),
        Container(
          padding: const EdgeInsets.all(4),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: lightgraycolor,
          ),
          child: Icon(
            Icons.chevron_right_outlined,
            size: 20.0,
            color: appcolor,
          ),
        ),
        SizedBox(width: 10.0,),

      ],
    );

  }

  peoplefollwlist() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 100,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: 22,
            itemBuilder: (context, position) {
              return _peoplefollwolistitem();
            },
          ),
        ),

      ],
    );


  }
  _peoplefollwolistitem(){
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(
            width: 65.0,
            height: 65.0,
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                fit: BoxFit.cover,
                image: new AssetImage(AppImages.proimage),
              ),
            ),
          ),
          Text("Albert")
        ],
      ),
    );


  }

  //treanding on two
  _treadndingonview(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Trending on Two",style: getFormarticletitleStyle(context),),
        Expanded(
            child: Divider(height: 5,thickness: 2,color: dividercolor,)
        ),

        SizedBox(width: 20.0,),

      ],
    );

  }

}




