
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:Two/saadh_dev/ui/home/article/article_view.dart';
import 'package:Two/saadh_dev/ui/home/community/community_view.dart';
import 'package:Two/saadh_dev/ui/home/news/news_view.dart';
import 'package:Two/saadh_dev/ui/home/profile/profile_view.dart';
import 'package:flutter/material.dart';



class HomeView extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeView> {
  // Properties & Variables needed

  int currentTab = 0; // to keep track of active tab index
  final List<Widget> screens = [
    NewsView(),
    ArticleView(),
    CommunityView(),
    ProfileView(),
  ]; // to store nested tabs
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = NewsView(); // Our first view in viewport

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add,size: 40.0,),
        onPressed: () {
          Navigator.pushNamed(context, RoutePaths.fabPostArticle);
    },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,

                children: <Widget>[
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            NewsView(); // if user taps on this dashboard tab will be active
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          CustomIcon.home_news_icon,size: 17.0,
                          color: currentTab == 0 ? Colors.black : Colors.grey,
                        ),
                        Text(
                          AppConstants.news,
                          style: TextStyle(
                            color: currentTab == 0 ? Colors.black : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            ArticleView(); // if user taps on this dashboard tab will be active
                        currentTab = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          CustomIcon.home_artical_icon,size: 17.0,
                          color: currentTab == 1 ? Colors.black : Colors.grey,
                        ),
                        Text(
                          AppConstants.Article,
                          style: TextStyle(
                            color: currentTab == 1 ? Colors.black : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),

              // Right Tab bar icons

              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            CommunityView(); // if user taps on this dashboard tab will be active
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          CustomIcon.home_community_icon,size: 17.0,
                          color: currentTab == 2 ? Colors.black : Colors.grey,
                        ),
                        Text(
                          AppConstants.community,
                          style: TextStyle(
                            color: currentTab == 2 ? Colors.black : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            ProfileView(); // if user taps on this dashboard tab will be active
                        currentTab = 3;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          CustomIcon.home_profile_icon,size: 17.0,
                          color: currentTab == 3 ? Colors.black : Colors.grey,
                        ),
                        Text(
                          AppConstants.profile,
                          style: TextStyle(
                            color: currentTab == 3 ? Colors.black : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )

            ],
          ),
        ),
      ),
    );
  }
}