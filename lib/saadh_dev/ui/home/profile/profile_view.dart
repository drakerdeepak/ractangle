import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_images.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/app_bar.dart';
import 'package:Two/saadh_dev/ui/home/profile/article_tab_view.dart';
import 'package:Two/saadh_dev/ui/home/profile/post_tab_view.dart';
import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';



class ProfileView extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

void _EditProfile(BuildContext context) {
  Navigator.pushNamed(context, RoutePaths.profileedit);
}

class _ProfileState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBarWithAll(context),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(left:10.0,right:10.0,top:5.0,bottom:5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget> [
                profileview(),
                SizedBox(height: 10.0,),
                _accountview(),
                SizedBox(height: 10.0,),
                _topictofollowview(),
                topictofollwlist(),
                SizedBox(height: 10.0,),
                _Aboutview(),
                SizedBox(height: 10.0,),
                _Timelineview(),

              ],
            ),
          ),
        )


    );
  }

  profileview() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      color: Colors.white,
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 108.0,
                  height: 116.0,
                  decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: new DecorationImage(
                        fit: BoxFit.cover,
                        image: new AssetImage(AppImages.Defultimage)),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      Row(

                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top:0.0,left: 10.0),
                            child: Text(
                              "Jane Cooper",
                              style: getFormarticletitleStyle(context),
                            ),
                          ),
                          Spacer(),
                          IconButton(
                            onPressed: () {
                              _EditProfile(context);
                            },
                            icon:Image.asset(
                              'assets/new_icons/written.png',
                              //CustomIcon.comments_like,
                              color: Colors.black,
                            ),
                            iconSize: 20.0,
                            //
                            // written.png
                            // FaIcon(FontAwesomeIcons.edit,color: lightgraycolor,),
                            // iconSize: 20.0,
                          ),
                        ],
                      ),

                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Container(
                          width:MediaQuery.of(context).size.width * 0.55,
                          color: Colors.transparent,
                          child: Container(
                            decoration: BoxDecoration(
                                color: procardcolor,
                                borderRadius: BorderRadius.all(Radius.circular(7.0))),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                      Text("Post"),
                                      SizedBox(height: 10.0,),
                                      Text("34",style: getFormarticletitleStyle(context),),
                                    ]),
                                    Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                      Text("Followers"),
                                      SizedBox(height: 10.0,),
                                      Text("1K",style: getFormarticletitleStyle(context),),
                                    ]),
                                    Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                      Text("Following"),
                                      SizedBox(height: 10.0,),
                                      Text("34",style: getFormarticletitleStyle(context),),
                                    ]),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ]),
                    ],
                  ),
                ),

              ],

            ),

          ],
        ),
      ),
    );


  }
//account
  _accountview(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text("Account",style: getFormarticletitleStyle(context),),
            SizedBox(width: 10.0,),
            Expanded(
                child: Divider(thickness: 2,color: dividercolor,)
            ),

            SizedBox(width: 10.0,),

          ],
        ),
        SizedBox(height: 10.0,),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          color: procardcolor,
          elevation: 10,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left:10.0,top: 15.0),
                child: Text("Email"),
              ),
              Padding(
                padding: const EdgeInsets.only(left:10.0,top: 5.0),
                child: Text("janecooper@gmail.com",style: getFormarticletitleStyle(context),),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Divider(thickness: 2,color: dividercolor,),
              ),
              Padding(
                padding: const EdgeInsets.only(left:10.0),
                child: Text("Phone"),
              ),
              Padding(
                padding: const EdgeInsets.only(left:10.0,top: 5.0,bottom: 15.0),
                child: Text("+91 1234 567890",style: getFormarticletitleStyle(context),),
              ),
            ],
          ),

        ),


      ],
    );

  }

  //follow topic
  _topictofollowview(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Topics to follow",style: getFormarticletitleStyle(context),),
        SizedBox(width: 5.0,),
        Expanded(
            child: Divider(height: 5,thickness: 2,color: dividercolor,)
        ),

        Container(
          padding: const EdgeInsets.all(4),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: lightgraycolor,
          ),
          child: Icon(
            Icons.chevron_left_outlined,
            size: 20.0,
            color: appcolor,
          ),
        ),
        SizedBox(width: 6.0,),
        Container(
          padding: const EdgeInsets.all(4),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: lightgraycolor,
          ),
          child: Icon(
            Icons.chevron_right_outlined,
            size: 20.0,
            color: appcolor,
          ),
        ),
        SizedBox(width: 10.0,),

      ],
    );

  }

  topictofollwlist() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 80,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: 22,
            itemBuilder: (context, position) {
              return _toptofollwolistitem();
            },
          ),
        ),

      ],
    );


  }

  _toptofollwolistitem(){
    return  Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: 50.0,
            width: 120.0,
            color: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all( width: 2, color: appbarcolor),
                  borderRadius: BorderRadius.all(Radius.circular(25.0))),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("EDUCATION"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );


  }

  //about us
  _Aboutview(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text("About Us",style: getFormarticletitleStyle(context),),
            SizedBox(width: 10.0,),
            Expanded(
                child: Divider(thickness: 2,color: dividercolor,)
            ),

            SizedBox(width: 10.0,),

          ],
        ),
        SizedBox(height: 10.0,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left:10.0,top: 15.0),
              child: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."),
            ),

          ],
        ),


      ],
    );

  }
//time line view
  _Timelineview(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text("Timeline",style: getFormarticletitleStyle(context),),
            SizedBox(width: 10.0,),
            Expanded(
                child: Divider(thickness: 2,color: dividercolor,)
            ),

            SizedBox(width: 10.0,),

          ],
        ),



        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                SizedBox(height: 20.0),

                DefaultTabController(
                    length: 2, // length of tabs
                    initialIndex: 0,
                    child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
                      Container(
                        child: TabBar(
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.grey,
                          tabs: [
                            Tab(text: 'POST'),
                            Tab(text: 'ARTICLE'),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom:18.0),
                        child: Container(
                            height: MediaQuery.of(context).size.height ,
                            width: MediaQuery.of(context).size.width,//height of TabBarView
                            decoration: BoxDecoration(
                                border: Border(top: BorderSide(color: Colors.grey, width: 0.5))
                            ),
                            child: TabBarView(children: <Widget>[
                              Container(
                                  child:PostTabScreen()
                              ),
                              Container(
                                child: ArticleTabScreen()
                              ),


                            ])
                        ),
                      )
                    ])
                ),
                //create tabs here
              ]),
            ),



    ],
        ),


      ],
    );

  }
}




