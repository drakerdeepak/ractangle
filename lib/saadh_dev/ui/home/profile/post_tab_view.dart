import 'dart:math';

import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_images.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:flutter/material.dart';



class PostTabScreen extends StatefulWidget {
  @override
  _PostTabState createState() => _PostTabState();
}

class _PostTabState extends State<PostTabScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(left:10.0,right:10.0,top:5.0,bottom:5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget> [
                articleviewlistview(),

              ],
            ),
          ),
        )


    );
  }

  articleviewlistview(){
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: 2,
      itemBuilder: (context, position) {
        return _articleviewlist();
      },
    );

  }

  _articleviewlist() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          Container(
            width: 65.0,
            height: 65.0,
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                fit: BoxFit.cover,
                image: new AssetImage(AppImages.proimage),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              "Jane Cooper",
              style: getFormarticletitleStyle(context),
            ),
          ),
          ElevatedButton(
            onPressed: () {},
            child: Text('Follow +'),
            style: ElevatedButton.styleFrom(
              shape: StadiumBorder(),
              primary: appcolor,
              onPrimary: Colors.white,
              textStyle: TextStyle(
                fontSize: 15,
                color: Colors.redAccent,
              ),
            ),
          ),

        ]),
        Text(
          "Lorem Ipsum is simply dummy printing industry.",
          maxLines: 2,
          style: getFormarticletitleStyle(context),
        ),
        SizedBox(height: 10.0,),Text(
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",

          textAlign: TextAlign.justify,
          style: getFormarticlesubtitleStyle(context),
        ),
        SizedBox(height: 10.0,),
        Text(
          "#Bitcoin   #Social   #Nature   #Hashtaglist",
        ),
        SizedBox(height: 10.0,),
        Text(
          "@naval   @riyo    ",
        ),
        SizedBox(height: 10.0,),
        Container(
            height: 180.0,
            width: MediaQuery.of(context).size.width ,
            child: Image.asset(AppImages.Defultimage1,)),
        SizedBox(
          height: 20.0,
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                child: Container(
                  padding: EdgeInsets.only(left:5.0,right: 5.0),
                  width: MediaQuery.of(context).size.width ,
                  decoration: BoxDecoration(
                    color: appbarcolor,
                    borderRadius: BorderRadius.all(Radius.circular(25.0),),),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left:10.0,right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(children: [
                            IconButton(
                              onPressed: () {
                                // You enter here what you want the button to do once the user interacts with it
                              },
                              icon: Icon(
                                Icons.favorite_border_outlined,
                                //CustomIcon.like_icon,
                                color: Colors.black,
                              ),
                              iconSize: 22.0,
                            ),
                            Text("2.4k"),
                          ]),
                          Row(children: [
                            IconButton(
                              onPressed: () {
                                // You enter here what you want the button to do once the user interacts with it
                              },
                              icon: Icon(
                                Icons.chat_bubble_outline,
                                //CustomIcon.comments_like,
                                color: Colors.black,
                              ),
                              iconSize: 22.0,
                            ),
                            Text("175"),
                          ]),
                          // Transform.rotate(
                          //   angle: 180 * pi / 120,
                          //   child:
                            IconButton(
                              onPressed: () {
                                // You enter here what you want the button to do once the user interacts with it
                              },
                              icon: Image.asset(
                                'assets/new_icons/swap.png',
                                //CustomIcon.comments_like,
                                color: Colors.black,
                              ),

                              iconSize: 25.0,
                            ),
                          // ),
                          IconButton(
                            onPressed: () {
                              // You enter here what you want the button to do once the user interacts with it
                            },
                            icon: Image.asset(
                              'assets/new_icons/share.png',
                              //CustomIcon.comments_like,
                              color: Colors.black,
                            ),

                            iconSize: 25.0,
                          ),
                          IconButton(
                            onPressed: () {
                              // You enter here what you want the button to do once the user interacts with it
                            },
                            icon:Image.asset(
                              'assets/new_icons/bookmark.png',
                              //CustomIcon.comments_like,
                              color: Colors.black,
                            ),
                            iconSize: 25.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ]),
        SizedBox(height: 10.0,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(CustomIcon.date_icon),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("05-02-2021"),
                ),
              ],
            ),

            Row(
              children: [
                Icon(CustomIcon.like_icon),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Abhishek liked riyo post"),
                )
              ],
            ),

          ],
        ),
        Divider(color: lightgraycolor,)
      ],

    );
  }


}




