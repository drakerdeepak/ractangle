import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/ui/login/login_view.dart';
import 'package:flutter/material.dart';
import 'package:otp_count_down/otp_count_down.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';


class SignupOTPView extends StatefulWidget {
  @override
  _OTPViewState createState() => _OTPViewState();
}

class _OTPViewState extends State<SignupOTPView> {
  String enetered_otp;
  String user_email;
  String password;
  String type;
  int userID;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _countDown;
  OTPCountDown _otpCountDown;
  final int _otpTimeInMS = 1000 * 5 * 60;
  TextEditingController controller = TextEditingController(text: "");
  String thisText = "";
  int pinLength = 4;
  bool hasError = false;
  bool hassucess = false;
  String errorMessage;

  void _signupotpverify(BuildContext context) {

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                LoginView()));
  }
  @override
  void initState() {
    _startCountDown();
    super.initState();
  }

  void _startCountDown() {
    _otpCountDown = OTPCountDown.startOTPTimer(
      timeInMS: _otpTimeInMS,
      currentCountDown: (String countDown) {
        _countDown = countDown;
        setState(() {});
      },
      onFinish: () {
        print("Count down finished!");
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Padding(
        padding:
            EdgeInsets.only(top: 10, right: 20.0, left: 20.0, bottom: 10.0),
        child: _otpverifcation(),
      ),
    );
  }
  Widget _otpverifcation() {
    return ListView(
      children: <Widget>[
        SizedBox(height: 50.0,),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top:50),
          child: Text(
            AppConstants.Otpverifcation,
            style: getFormTitleStyle(context),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Container(
          alignment: Alignment.center,
          child: Text(AppConstants.otpverficationsubtitle,
              style: getFormsubtitleStyle(context)),
        ),
        SizedBox(
          height: 30.0,
        ),
        Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 10.0,
              ),
              Container(

                child: PinCodeTextField(
                  autofocus: false,
                  controller: controller,
                  highlight: true,
                  highlightColor: appcolor,
                  defaultBorderColor: otpbordercolor,
                  hasTextBorderColor: appcolor,
                  maxLength: pinLength,
                  hasError: hasError,
                  pinBoxRadius: 10.0,
                  onTextChanged: (text) {
                    setState(() {
                      hasError = false;
                    });
                  },
                  onDone: (text) {
                    print("DONE $text");
                    print("DONE CONTROLLER ${controller.text}");
                  },
                  pinBoxWidth: 50,
                  pinBoxHeight: 54,
                  hasUnderline: false,

                  wrapAlignment: WrapAlignment.spaceAround,
                  pinBoxDecoration:
                  ProvidedPinBoxDecoration.defaultPinBoxDecoration,
                  pinTextStyle: TextStyle(fontSize: 22.0),
                  pinTextAnimatedSwitcherTransition:
                  ProvidedPinBoxTextAnimation.scalingTransition,
//                   pinBoxColor: Colors.green[100],
                  pinTextAnimatedSwitcherDuration:
                  Duration(milliseconds: 200),
//                    highlightAnimation: true,
                  highlightAnimationBeginColor: Colors.black,
                  highlightAnimationEndColor: Colors.white12,
                  keyboardType: TextInputType.number,

                ),
              ),

              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Visibility(
                  child: Text(
                    "Inncorrect OTP",style: getFormsubtitleredStyle(context),
                  ),
                  visible: hasError,
                ),
              ),
              Visibility(
                child: Text(
                  "OTP Verified",style: getFormsubtitlegreenStyle(context),
                ),
                visible: hassucess,
              ),

              SizedBox(
                height: 10.0,
              ),
              Container(
                  margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        AppConstants.otpcodeexprie,
                        style: getFormsubtitleStyle(context),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        _countDown != null ? _countDown : "",
                        style: getFormsubtitleredStyle(context),
                      ),
                    ],
                  )),
              SizedBox(
                height: 50.0,
              ),

              Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 0),
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: appcolor,
                    child: Text(
                      AppConstants.otpverfiy,
                      style: getFormbuttonStyle(context),
                    ),
                    onPressed: () {
                      _signupotpverify(context);

                      /* setState(() {
                    this.hasError = true;
                  });*/
                      /*  setState(() {
                    this.thisText = controller.text;
                  });*/
                      setState(() {
                        this.hasError = true;
                      });
                    },
                  )),
              Container(
                  margin: EdgeInsets.only(top: 25.0, bottom: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        AppConstants.otpnotget,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      InkWell(
                        onTap: () {},
                        child: Text(
                          AppConstants.otpresend,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      )
                    ],
                  )),
              //SizedBox(height: 20.0),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildOtpVerification() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 50.0,
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(
                AppConstants.Otpverifcation,
                style: getFormTitleStyle(context),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  child: Text(AppConstants.otpverficationsubtitle,
                      style: getFormsubtitleStyle(context)),
                ),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  PinCodeTextField(
                    autofocus: true,
                    controller: controller,
                    highlight: true,
                    highlightColor: appcolor,
                    defaultBorderColor: otpbordercolor,
                    hasTextBorderColor: appcolor,
                    maxLength: pinLength,
                    hasError: hasError,
                    pinBoxRadius: 10.0,
                    onTextChanged: (text) {
                      setState(() {
                        hasError = false;
                      });
                    },
                    onDone: (text) {
                      print("DONE $text");
                      print("DONE CONTROLLER ${controller.text}");
                    },
                    pinBoxWidth: 50,
                    pinBoxHeight: 54,
                    hasUnderline: false,
                    wrapAlignment: WrapAlignment.spaceAround,
                    pinBoxDecoration:
                        ProvidedPinBoxDecoration.defaultPinBoxDecoration,
                    pinTextStyle: TextStyle(fontSize: 22.0),
                    pinTextAnimatedSwitcherTransition:
                        ProvidedPinBoxTextAnimation.scalingTransition,
//                   pinBoxColor: Colors.green[100],
                    pinTextAnimatedSwitcherDuration:
                        Duration(milliseconds: 300),
//                    highlightAnimation: true,
                    highlightAnimationBeginColor: Colors.black,
                    highlightAnimationEndColor: Colors.white12,
                    keyboardType: TextInputType.number,
                  ),

                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Visibility(
                      child: Text(
                        "Inncorrect OTP",style: getFormsubtitleredStyle(context),
                       ),
                      visible: hasError,
                    ),
                  ),
                  Visibility(
                    child: Text(
                      "OTP Verified",style: getFormsubtitlegreenStyle(context),
                    ),
                    visible: hassucess,
                  ),

                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            AppConstants.otpcodeexprie,
                            style: getFormsubtitleStyle(context),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            _countDown != null ? _countDown : "",
                            style: getFormsubtitleredStyle(context),
                          ),
                        ],
                      )),
                  SizedBox(
                    height: 50.0,
                  ),
                  Container(
                      height: 50,
                      width: 350.0,
                      padding: EdgeInsets.only(top: 0),
                      child: RaisedButton(
                        textColor: Colors.white,
                        color: appcolor,
                        child: Text(
                          AppConstants.otpverfiy,
                          style: getFormbuttonStyle(context),
                        ),
                        onPressed: () {
                          /* setState(() {
                            this.hasError = true;
                          });*/
                          /*  setState(() {
                            this.thisText = controller.text;
                          });*/
                          setState(() {
                            this.hasError = true;
                          });
                        },
                      )),
                  Container(
                      margin: EdgeInsets.only(top: 25.0, bottom: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            AppConstants.otpnotget,
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          InkWell(
                            onTap: () {},
                            child: Text(
                              AppConstants.otpresend,
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          )
                        ],
                      )),
                  //SizedBox(height: 20.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _otpCountDown.cancelTimer();
    super.dispose();
  }
}
