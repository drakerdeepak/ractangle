import 'package:Two/saadh_dev/core/notifiers/common_notifier.dart';
import 'package:Two/saadh_dev/core/notifiers/login/login_notifier.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/login/login_response.dart';
import 'package:Two/saadh_dev/saadh_util/app_validators.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_images.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:Two/saadh_dev/ui/forgot_password/forgot_password_view.dart';
import 'package:Two/saadh_dev/ui/reg/singup_view.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginView extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<LoginView> {
  TextEditingController email_phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool _isPasswordVisible = false;

  final FocusNode _passwordFocus = FocusNode();
  GlobalKey<FormState> _keyValidationForm = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();



  void _onClickLogin(BuildContext context, LoginNotifier loginNotifier) async {
    LoginResponse response = await loginNotifier.apiLogin(
        email_phoneController.text, passwordController.text,);
    if (response.success == true && response.message != null) {
      final snackBar = SnackBar(content: Text('${response.message}'));
      // Find the Scaffold in the widget tree and use it to show a SnackBar.
      _scaffoldKey.currentState.showSnackBar(snackBar);
      loginNotifier.saveUserToken(response.data.accessToken);
      CommonNotifier().userDetail = response.data;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('login', true);
      prefs.setString('Username', response.data.name);
      prefs.setString('useremail', email_phoneController.text);
      prefs.setString('Password', passwordController.text);
      prefs.setString('token', response.data.accessToken);
      prefs.setString('id', response.data.sId.toString());
      print('sucess');
      _onClickTextLoginHome(context);
    } else {
      final snackBar = SnackBar(content: Text('${response.message}'));
      // Find the Scaffold in the widget tree and use it to show a SnackBar.
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }



// sign up
  void _onClickTextsignup(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                SingupView()));
   }
   //forgot password
  void _onClickTextforgotpassword(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ForgotPasswordView()));
    }

  //login
  void _onClickTextLoginHome(BuildContext context) {
    Navigator.pushNamed(context, RoutePaths.HomeviewRoute);
  }



  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginNotifier>(
      create: (context) => LoginNotifier(),
      child: Consumer<LoginNotifier>(
        builder: (BuildContext context, loginNotifier, _) => GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Scaffold(
            key: _scaffoldKey,
            //backgroundColor: colorlighWhite,
            body: ModalProgressHUD(
                inAsyncCall: loginNotifier.isLoading,
                child: Padding(
                  padding: EdgeInsets.only(top: 10,right: 20.0,left: 20.0,bottom: 10.0),
                  child: _buildloginWidget(loginNotifier),
                )),
          ),
        ),
      ),
    );
  }



  Widget _buildloginWidget(loginNotifier){
    return Form(
      key: _keyValidationForm,
      child: ListView(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(10),
            child: Image.asset(AppImages.LoginImage),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top:30),
            child: Text(
              AppConstants.loginwelcome,
              style: getFormTitleStyle(context),
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top:8.0),
            child: Text(
              AppConstants.loginwelcomesubtitle,
              style: getFormsubtitleStyle(context),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 40),
            child: TextFormField(
              controller: email_phoneController,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              validator: validateEmail,
              onFieldSubmitted: (String value) {
                FocusScope.of(context)
                    .requestFocus(_passwordFocus);
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppConstants.loginuseremail,
                prefixIcon:  Icon(
                  CustomIcon.email_login,
                  color: appcolor,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top:20),
            child: TextFormField(
              controller: passwordController,
              obscureText: !_isPasswordVisible,
              focusNode: _passwordFocus,
              keyboardType: TextInputType.text,
              validator: validatePassword,

              decoration: InputDecoration(
                border: OutlineInputBorder(),
                errorMaxLines: 3,
                labelText: AppConstants.loginuserpassword,
                prefixIcon: Icon(
                  CustomIcon.lock_login,
                  color: appcolor,
                ),
                suffixIcon: IconButton(
                  icon: Icon(_isPasswordVisible
                      ? CustomIcon.show_password
                      : Icons.visibility_off),
                  onPressed: () {
                    setState(() {
                      _isPasswordVisible =
                      !_isPasswordVisible;
                    });
                  },
                ),
              ),

            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20),
            alignment: Alignment.centerRight,
            child: FlatButton(
              onPressed: (){
                _onClickTextforgotpassword(context);
                //forgot password screen
              },
              textColor: colorBlack,
              child: Text( AppConstants.loginuserForgotpassword,style: getFormsubtitleStyle(context),),
            ),
          ),
          SizedBox(height: 20.0,),
          Container(
              height: 50,
              padding: EdgeInsets.only(top:0),
              child: RaisedButton(
                textColor: Colors.white,
                color: appcolor,
                child: Text(AppConstants.login,style: getFormbuttonStyle(context),),
                onPressed: () {
                  FocusScope.of(context)
                      .requestFocus(FocusNode());
                  if (_keyValidationForm.currentState
                      .validate()) {
                    _onClickLogin(context, loginNotifier);
                  }
                 // _onClickLogin(context,loginNotifier);
                  print(email_phoneController.text);
                  print(passwordController.text);
                },
              )),
          SizedBox(height: 20.0,),
          Container(
              child: Row(
                children: <Widget>[
                  Text(AppConstants.loginNothaveacount,style:getFormsubtitleStyle(context)),
                  FlatButton(
                    textColor: appcolorlight,
                    child: Text(
                      AppConstants.signup,
                      style: getFormTitleStyle(context),
                    ),
                    onPressed: () {
                      _onClickTextsignup(context);
                      //signup screen
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              )),
          Center(child: Text("version: 1.0"))
        ],
      ),
    );

  }

}