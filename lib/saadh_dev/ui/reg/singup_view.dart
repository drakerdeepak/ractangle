import 'package:Two/saadh_dev/core/notifiers/register/register_account_notifier.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/request_response/registeraccount/register_account_response.dart';
import 'package:Two/saadh_dev/saadh_util/app_validators.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_constants.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_font.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_route_paths.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:Two/saadh_dev/ui/login/login_view.dart';
import 'package:Two/saadh_dev/ui/otp/otp_view.dart';
import 'package:Two/utils/ApiServices.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';


class SingupView extends StatefulWidget {

  @override
  _State createState() => _State();
}

class _State extends State<SingupView> {
  TextEditingController nameController = TextEditingController();
  TextEditingController email_phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  bool _isPasswordVisible = false;
  bool _loading = false;

  GlobalKey<FormState> _keyValidationForm = GlobalKey<FormState>();
  static var _keyValidationForm1 = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();

  void _onClickSignup(
      BuildContext context, CreateAccountNotifier createAccountNotifier) async {
    RegisterAccountResponse response =
    await createAccountNotifier.apiRegisterUser(email_phoneController.text,
        passwordController.text, nameController.text,);
    if (response.success == true && response.message != null) {
      //show success dialogue & navigate to login page
      _showDialogueSuccessRegistration(
          context, response.message);
     } else {
      final snackBar = SnackBar(content: Text('${response.message}'));
      // Find the Scaffold in the widget tree and use it to show a SnackBar.
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
    /*else {
      List errList = response.errors;
      String errMsg = "";
      errList.forEach((element) {
        errMsg += (element + ".");
      });
      final snackBar = SnackBar(content: Text('${errMsg}'));
      // Find the Scaffold in the widget tree and use it to show a SnackBar.
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }*/
  }


  void _showDialogueSuccessRegistration(context, message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              message,
              style: getAppBarTitleTextStyle(context).copyWith(
                  color: colorBlack, fontWeight: AppFont.fontWeightSemiBold),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(AppConstants.OK,
                    style: getStyleSubHeading(context).copyWith(
                        color: colorPrimary,
                        fontWeight: AppFont.fontWeightMedium)),
                onPressed: () {
                  _onClickTextotp(context);

                //for register screen
                },
              )
            ],
          );
        });
  }
 /* void _onClickSignup(BuildContext context) {
    Navigator.pushNamed(context, RoutePaths.SingupOtp);
  }
*/

  void _onClickTextotp(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                SignupOTPView()));


  }


  @override
  void initState() {

    super.initState();
  }

  void _onClickTextlogin()async {
    setState(() {
      _loading = true;
    });
    print("i was hereeee");
    ApiServices.fetchRegisterData(nameController.text,lastnameController.text,usernameController.text,phoneController.text,email_phoneController.text,passwordController.text).then((result) => {
    setState(() {
      _loading = false;
      if(result!=null) {
                  if (result.success == "true") {
                    Fluttertoast.showToast(
                        msg: "Registered Successfully...",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.blue,
                        textColor: Colors.white,
                        fontSize: 16.0);
                  } else {
                    List<String> errors = [];

                    print('aaaaaaaaaaaaaaaaaaaa--->' +
                        result.message[0].name.toString());

                    String errormessage = '';
                    if (result.message[0].email != null) {
                      errormessage = 'Email ,';
                    }
                    if (result.message[0].name != null)
                      errormessage = errormessage + 'UserName ,';

                    if (result.message[0].phone != null)
                      errormessage += "Phone Number";

                    Fluttertoast.showToast(
                        msg: errormessage + ' already exist...',
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0);
                  }
                }else{
        setState(() {
          _loading=false;
        });
        Fluttertoast.showToast(
            msg: 'Something went wrong...',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
              })
    });










    // Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) =>
    //             LoginView()));
  }
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CreateAccountNotifier>(
        create: (context) => CreateAccountNotifier(),
        child: Consumer<CreateAccountNotifier>(
          builder: (BuildContext context, createAccountNotifier, _) =>
              GestureDetector(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                },
                child: MaterialApp(
                  debugShowCheckedModeBanner: false,
                  home: ModalProgressHUD(
                    inAsyncCall: _loading,
                    child: Scaffold(
                    //  backgroundColor: colorlighWhite,
                      resizeToAvoidBottomInset: true,
                      key: _scaffoldKey,
                      body: ModalProgressHUD(
                        inAsyncCall: createAccountNotifier.isLoading,
                        child: Stack(
                          children: [
                           // _buildWidgetBottomImage(),
                           // _buildWidgetCreateAccountForm(createAccountNotifier),
                            Padding(
                              padding: EdgeInsets.only(top: 10,right: 20.0,left: 20.0,bottom: 10.0),
                              child: _singup(createAccountNotifier),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
        ));
  }


  Widget _singup(createAccountNotifier){
    return Form(
      key: _keyValidationForm,
      child: ListView(
        children: <Widget>[
          SizedBox(height: 50.0,),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top:50),
            child: Text(
              AppConstants.loginwelcome,
              style: getFormTitleStyle(context),
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top:8.0),
            child: Text(
              AppConstants.loginwelcomesubtitle,
              style: getFormsubtitleStyle(context),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 40),
            child: TextFormField(
              keyboardType: TextInputType.name,
              textInputAction: TextInputAction.next,
              controller: nameController,
              validator: (value) => validateEmptyCheck(
                  value, "${AppConstants.signupfirstname} can\'t be empty "),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppConstants.signupfirstname,
                prefixIcon:  Icon(
                  CustomIcon.profile_reg,
                  color: appcolor,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 40),
            child: TextFormField(
              keyboardType: TextInputType.name,
              textInputAction: TextInputAction.next,
              controller: lastnameController,
              validator: (value) => validateEmptyCheck(
                  value, "${AppConstants.signuplastname} can\'t be empty "),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppConstants.signuplastname,
                prefixIcon:  Icon(
                  CustomIcon.profile_reg,
                  color: appcolor,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 40),
            child: TextFormField(
              keyboardType: TextInputType.name,
              textInputAction: TextInputAction.next,
              controller: usernameController,
              validator: (value) => validateEmptyCheck(
                  value, "${AppConstants.signuploginname} can\'t be empty "),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppConstants.signuploginname,
                prefixIcon:  Icon(
                  CustomIcon.profile_reg,
                  color: appcolor,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20),
            child: TextFormField(
              controller: email_phoneController,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              validator: validateEmail,
              onFieldSubmitted: (String value) {
                FocusScope.of(context)
                    .requestFocus(_passwordFocus);
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppConstants.signupuseremail,
                prefixIcon:  Icon(
                  CustomIcon.email_login,
                  color: appcolor,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top:20),
            child: TextFormField(
              controller: passwordController,
              obscureText: !_isPasswordVisible,
              //focusNode: _passwordFocus,
              keyboardType: TextInputType.text,
              validator: validatePassword,
              onFieldSubmitted: (String value) {
                FocusScope.of(context)
                    .requestFocus(_phoneFocus);
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                errorMaxLines: 3,
                labelText: AppConstants.loginuserpassword,
                prefixIcon: Icon(
                  CustomIcon.lock_login,
                  color: appcolor,
                ),
                suffixIcon: IconButton(
                  icon: Icon(_isPasswordVisible
                      ? CustomIcon.show_password
                      : Icons.visibility_off),
                  onPressed: () {
                    setState(() {
                      _isPasswordVisible =
                      !_isPasswordVisible;
                    });
                  },
                ),
              ),

            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20),
            child: TextFormField(
              controller: phoneController,
              keyboardType: TextInputType.phone,
              validator: validateMobile,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppConstants.signupuserphone,
                prefixIcon:  Icon(
                  CustomIcon.call_reg,
                  color: appcolor,
                ),
              ),
            ),
          ),

          SizedBox(height: 40.0,),
          Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top:0),
              child: RaisedButton(
                textColor: Colors.white,
                color: appcolor,
                child: Text(AppConstants.signupsignup,style: getFormbuttonStyle(context),),
                onPressed: () {
                  FocusScope.of(context)
                      .requestFocus(FocusNode());
                  if (_keyValidationForm.currentState
                      .validate()) {
                    _onClickTextlogin();
                  }

                //  _onClickSignup(context, createAccountNotifier);
                //  _onClickTextotp(context);
                  print(email_phoneController.text);
                  print(passwordController.text);
                },
              )),
          SizedBox(height: 20.0,),
          Container(
              child: Row(
                children: <Widget>[
                  Text(AppConstants.signalready,style:getFormsubtitleStyle(context)),
                  FlatButton(
                    textColor: appcolorlight,
                    child: Text(
                      AppConstants.loginin,
                      style: getFormTitleStyle(context),
                    ),
                    onPressed: () {

                      _onClickTextlogin();
                      //signup screen
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ))
        ],
      ),
    );

  }



}