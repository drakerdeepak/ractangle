import 'package:Two/model/response_all_news.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/model/news_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'app/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final docPath = await getApplicationDocumentsDirectory();
  Hive.init(docPath.path);
  Hive.registerAdapter(ArticlesAdapter());

  await Hive.openBox('settingsBox');
  await Hive.openBox<NewsInfo>('bookmarks');
  await Hive.openBox<NewsInfo>('unreads');

  final _isDarkModeOn = await Hive.box('settingsBox').get('isDarkModeOn');
  SettingsProvider().darkTheme(_isDarkModeOn ?? false);

  final _lang = await Hive.box('settingsBox').get('activeLang');
  SettingsProvider().setLang(_lang);


  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.black,
      statusBarColor: Colors.white,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.light,
      systemNavigationBarIconBrightness:Brightness.light,
    ),
  );

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<SettingsProvider>(
            create: (_) => SettingsProvider()),
        ChangeNotifierProvider<FeedProvider>(create: (_) => FeedProvider()),
      ],
      child: App(),
    ),
  );
}
