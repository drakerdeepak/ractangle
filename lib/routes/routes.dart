import 'package:auto_route/auto_route_annotations.dart';
import 'package:Two/view/app_base/app_base.dart';
import 'package:Two/view/bookmarked_screen/bookmark.dart';
import 'package:Two/view/discover_screen/discover.dart';
import 'package:Two/view/feed_screen/feed.dart';
import 'package:Two/view/photo_view/photo_expanded_screen.dart';
import 'package:Two/view/search_screen/search.dart';
import 'package:Two/view/settings_screen/settings.dart';
import 'package:Two/view/web_screen/web.dart';

@autoRouter
class $Router {
  SearchScreen searchScreen;
  SettingsScreen settingsScreen;
  BookmarkScreen bookmarkScreen;
  WebScreen webScreen;
  DiscoverScreen discoverScreen;
  FeedScreen feedScreen;
  ExpandedImageView expandedView;
  @initial
  AppBase appBase;
}
