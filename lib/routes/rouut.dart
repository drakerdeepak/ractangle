import 'package:Two/app/home_screen.dart';
import 'package:Two/view/post_screen/post_article.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:Two/view/interest/interest_screen.dart';
import 'package:Two/view/photo_view/photo_expanded_screen.dart';
import 'package:Two/view/profile/profile_screen.dart';
import 'package:Two/view/search_screen/search.dart';
import 'package:Two/view/settings_screen/settings.dart';
import 'package:Two/view/bookmarked_screen/bookmark.dart';
import 'package:Two/view/video/video_screen.dart';
import 'package:Two/view/web_screen/web.dart';
import 'package:Two/view/discover_screen/discover.dart';
import 'package:Two/view/feed_screen/feed.dart';
import 'package:Two/model/news_model.dart';
import 'package:Two/view/app_base/app_base.dart';

class Rouut {
  static const searchScreen = '/search-screen';
  static const settingsScreen = '/settings-screen';
  static const newsScreen = '/HomeScreenNew';
  static const videoScreen = '/video-screen';
  static const articleScreen = '/article-screen';
  static const postArticleScreen = '/post_article';
  static const profileScreen = '/profile-screen';
  static const bookmarkScreen = '/bookmark-screen';
  static const webScreen = '/web-screen';
  static const discoverScreen = '/discover-screen';
  static const feedScreen = '/feed-screen';
  static const appBase = '/';
  static const expandedImageView = '/expandedImageView';

  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<Rouut>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Rouut.searchScreen:
        return MaterialPageRoute(
          builder: (_) => SearchScreen(),
          settings: settings,
        );
      case Rouut.settingsScreen:
        return MaterialPageRoute(
          builder: (_) => SettingsScreen(),
          settings: settings,
        );
      case Rouut.newsScreen:
        return MaterialPageRoute(
          builder: (_) => HomeScreenNew(),
          settings: settings,
        );
      case Rouut.postArticleScreen:
        return MaterialPageRoute(
          builder: (_) => PostArticleScreen(),
          settings: settings,
        );
      case Rouut.bookmarkScreen:
        return MaterialPageRoute(
          builder: (_) => BookmarkScreen(),
          settings: settings,
        );
      case Rouut.videoScreen:
        return MaterialPageRoute(
          builder: (_) => VideoScreen(),
          settings: settings,
        );
      case Rouut.profileScreen:
        return MaterialPageRoute(
          builder: (_) => ProfileScreen(),
          settings: settings,
        );
      case Rouut.articleScreen:
        return MaterialPageRoute(
          builder: (_) => InterestScreen(),
          settings: settings,
        );
      case Rouut.webScreen:
        if (hasInvalidArgs<WebViewArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<WebViewArguments>(args);
        }
        final typedArgs = args as WebViewArguments;
        return MaterialPageRoute(
          builder: (_) => WebScreen(
            url: typedArgs.url,
            isFromBottom: typedArgs.isFromBottom,
          ),
          settings: settings,
        );
      case Rouut.discoverScreen:
        return MaterialPageRoute(
          builder: (_) => DiscoverScreen(),
          settings: settings,
        );
      case Rouut.expandedImageView:
        if (hasInvalidArgs<ExpandedImageViewArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<ExpandedImageViewArguments>(args);
        }
        final typedArgs = args as ExpandedImageViewArguments;
        return MaterialPageRoute(
          builder: (_) => ExpandedImageView(
            image: typedArgs.image,
          ),
          settings: settings,
        );

      case Rouut.feedScreen:
        if (hasInvalidArgs<FeedScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<FeedScreenArguments>(args);
        }
        final typedArgs = args as FeedScreenArguments;
        return MaterialPageRoute(
          builder: (_) => FeedScreen(
              key: typedArgs.key,
            articalIndex: typedArgs.articalIndex,
             // articals: typedArgs.articals,
              isFromSearch: typedArgs.isFromSearch),
          settings: settings,
        );

      case Rouut.appBase:
        return MaterialPageRoute(
          builder: (_) => AppBase(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//FeedScreen arguments holder class
class FeedScreenArguments {
  final Key key;
  final int articalIndex;
  final List<Articles> articals;
  final bool isFromSearch;
  FeedScreenArguments(
      {this.key,
      @required this.articalIndex,
      @required this.articals,
      @required this.isFromSearch});
}

class ExpandedImageViewArguments {
  final List image;
  ExpandedImageViewArguments({
    @required this.image,
  });
}

class WebViewArguments {
  final String url;
  final bool isFromBottom;

  WebViewArguments({@required this.url, @required this.isFromBottom});
}
