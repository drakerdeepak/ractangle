

import 'package:Two/aplication_localization.dart';
import 'package:Two/model/response_all_news.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:Two/model/news_model.dart';

final Box<NewsInfo> bookmarksBox = Hive.box('bookmarks');
final Box<NewsInfo> unreadsBox = Hive.box('unreads');

void handleBookmarks(NewsInfo article) async {
  final bool isPresent = bookmarksBox.containsKey(article.sourceURL);
  BuildContext context;

  if (!isPresent) {
    bookmarksBox.put(article.sourceURL, article);
    print(bookmarksBox.length);
    Fluttertoast.showToast(
      msg: /*AppLocalizations.of(context).translate("add_bookmark"),*/ 'Added to Bookmark',
      backgroundColor: Colors.red,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 3,
      toastLength: Toast.LENGTH_LONG,
    );
  } else {
    bookmarksBox.delete(article.sourceURL);
    print(bookmarksBox.length);
    Fluttertoast.showToast(
      msg: 'Removed from Bookmarks',/*AppLocalizations.of(context).translate("remove_bookmark"),*/
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.red,
      timeInSecForIosWeb: 3,
      toastLength: Toast.LENGTH_SHORT,
    );
  }
}

void addArticlesToUnreads(List<NewsInfo> articles) async {
  articles.forEach((element) {
    if (!unreadsBox.containsKey(element.sourceURL)) {
      unreadsBox.put(element.sourceURL, element);
      print('added' + "${unreadsBox.length}");
    }
  });
}

void removeArticleFromUnreads(NewsInfo articles) {
  if (unreadsBox.containsKey(articles.sourceURL)) {
    unreadsBox.delete(articles.sourceURL);
    print('removed' + "${unreadsBox.length}");
  }
}
