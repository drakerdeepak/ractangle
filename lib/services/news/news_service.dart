import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:Two/app/dio/dio.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/model/news_model.dart';
import 'package:Two/services/news/offline_service.dart';
import 'package:provider/provider.dart';

abstract class NewsFeedRepository {
  Future<List<Articles>> getNewsByTopic(String topic);

  Future<List<Articles>> getNewsByCategory(String category);

  List<Articles> getNewsFromLocalStorage(String box);
}

class NewsFeedRepositoryImpl implements NewsFeedRepository {
  final BuildContext context;
  NewsFeedRepositoryImpl(this.context);

  @override
  Future<List<Articles>> getNewsByTopic(String topic) async {
    final String url = "everything?q=$topic";
    final provider = Provider.of<FeedProvider>(context, listen: false);
    provider.setDataLoaded(false);
    provider.setLastGetRequest("getNewsByTopic", topic);
    print("getNewsByTopic" + " " + topic);
    final Map<String, dynamic> header = {'X-location': 'locale'};
    header.putIfAbsent('token', () => 'Bearer xyz ');
    Response response = await GetDio.getDio().get(url, options: Options(headers: header));
    print("getNewsByTopicRes "+response.toString());
    if (response.statusCode == 200) {
      List<Articles> articles = NewsModel.fromJson(response.data).articles;
      provider.setDataLoaded(true);
     // addArticlesToUnreads(articles);

      return articles;
    } else {
      provider.setDataLoaded(true);
      throw Exception();
    }
  }


  @override
  Future<List<Articles>> getNewsByCategory(String category) async {
    print('category'+category);
    final String url = "post?lang=en&category=$category";
    final provider = Provider.of<FeedProvider>(context, listen: false);

    provider.setDataLoaded(false);
    provider.setLastGetRequest("getNewsByCategory", category);

    Response response = await GetDio.getDio().get(url);

    print("getNewsByCategoryRes "+response.toString());
    if (response.statusCode == 200) {
      List<Articles> articles = NewsModel.fromJson(response.data).articles;

      print('article11list '+articles.toString());
      provider.setDataLoaded(true);
   //   addArticlesToUnreads(articles);

      return articles;
    } else {
      provider.setDataLoaded(true);
      throw Exception();
    }
  }


/*
  @override
  Future<List<Articles>> getNewsByCategory(String category) async {
    final String url = "top-headlines?country=in&category=$category";
    final provider = Provider.of<FeedProvider>(context, listen: false);

    provider.setDataLoaded(false);
    provider.setLastGetRequest("getNewsByTopic", category);

    Response response = await GetDio.getDio().get(url);

    print("getNewsByCategoryRes "+response.toString());
    if (response.statusCode == 200) {
      List<Articles> articles = NewsModel.fromJson(response.data).articles;

      provider.setDataLoaded(true);
      addArticlesToUnreads(articles);

      return articles;
    } else {
      provider.setDataLoaded(true);
      throw Exception();
    }
  }
*/


  @override
  List<Articles> getNewsFromLocalStorage(String fromBox) {
    List<Articles> articles = [];
    final Box<Articles> hiveBox = Hive.box<Articles>(fromBox);
    final provider = Provider.of<FeedProvider>(context, listen: false);

    provider.setLastGetRequest("getNewsFromLocalStorage", fromBox);

    print(fromBox);

    if (hiveBox.length > 0) {
      for (int i = 0; i < hiveBox.length; i++) {
        Articles article = hiveBox.getAt(i);
        articles.add(article);
      }
      provider.setDataLoaded(true);

      return articles;
    } else {
      provider.setDataLoaded(true);
      List<Articles> articles = [];
      return articles;
    }
  }
}
