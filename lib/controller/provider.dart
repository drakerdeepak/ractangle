import 'package:Two/app/home_screen.dart';
import 'package:Two/model/filter_news_request.dart';
import 'package:Two/model/response_all_catogries.dart';
import 'package:Two/model/response_all_news.dart';
import 'package:Two/model/response_filter_news.dart';
import 'package:Two/saadh_dev/core/notifiers/base/base_notifier.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/all_news_feed_repository.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/filter_news_feed_repository.dart';
import 'package:Two/saadh_dev/core/service/dependent_services/news_feed_repository.dart';
import 'package:Two/view/discover_screen/discover.dart';
import 'package:Two/view/web_screen/web.dart';
import 'package:flutter/material.dart';

class FeedProvider extends BaseNotifier {
  final _repository = NewsFeedRepository();
  final _allNewrepository = AllNewsFeedRepository();
  final _filterNewrepository = FilterNewsFeedRepository();
  String _appBarTitle;
  int _activeCategory = 1;
  bool _hasDataLoaded = false;
  bool _searchAppBarVisible = true;
  bool _appBarVisible = false;
  bool _watermarkVisible = false;
  bool _feedBottomActionbarVisible = false;
  int _curentArticalIndex = 0;
  PageController _feedPageController;
  PageController _screenController;
  List<CategoryInfo> _allCategoriesList = <CategoryInfo>[];
  List<CategoryInfo> get allCategoriesList => _allCategoriesList;
  set allCategoriesList(List<CategoryInfo> value) {
    _allCategoriesList = value;
    notifyListeners();
  }
  //for all news
  List<NewsInfo> _allNewsList = <NewsInfo>[];

  List<NewsInfo> get allNewsList => _allNewsList;

  set allNewsList(List<NewsInfo> value) {
    _allNewsList = value;
    notifyListeners();
  }
  List<FilterNewsInfo> _filterNewsList = <FilterNewsInfo>[];


  List<FilterNewsInfo> get filterNewsList => _filterNewsList;

  set filterNewsList(List<FilterNewsInfo> value) {
    _filterNewsList = value;
    notifyListeners();
  }

  List<Widget> _baseScreen = [
    DiscoverScreen(),
    HomeScreenNew(),
   WebScreen(isFromBottom: false,url: "",),
  ];
  String _newsURL = "https://google.com/";
  bool _webviwAdded = false;

  bool _discoverviwAdded = false;
  List<String> _lastGetRequest = <String>[];

  //

  bool get gethasDataLoaded => this._hasDataLoaded;

  int get getActiveCategory => this._activeCategory;

  String get getAppBarTitle => this._appBarTitle;

  bool get getSearchAppBarVisible => this._searchAppBarVisible;

  bool get getAppBarVisible => this._appBarVisible;

  bool get getWatermarkVisible => this._watermarkVisible;

  bool get getFeedBottomActionbarVisible => this._feedBottomActionbarVisible;

  int get getCurentArticalIndex => this._curentArticalIndex;

  PageController get getfeedPageController => this._feedPageController;

  PageController get getScreenController => this._screenController;

  List<Widget> get getBaseScreenList => this._baseScreen;

  String get getNewsURL => _newsURL;

  List<String> get getLastGetRequest => _lastGetRequest;

  bool get webviwAdded => _webviwAdded;

  bool get discoverviwAdded => _discoverviwAdded;

  ///

  void setActiveCategory(int activeCategory) {
    this._activeCategory = activeCategory;
    notifyListeners();
  }

  void setAppBarTitle(String appBarTitle) {
    this._appBarTitle = appBarTitle;
    notifyListeners();
  }

  void setDataLoaded(bool status) {
    this._hasDataLoaded = status;
    notifyListeners();
  }

  void setSearchAppBarVisible(bool searchAppBarVisible) {
    this._searchAppBarVisible = searchAppBarVisible;
    notifyListeners();
  }

  void setAppBarVisible(bool appBarVisible) {
    this._appBarVisible = appBarVisible;
    notifyListeners();
  }

  void setWatermarkVisible(bool visible) {
    this._watermarkVisible = visible;
    notifyListeners();
  }

  void setFeedBottomActionbarVisible(bool feedBottomActionbarVisible) {
    this._feedBottomActionbarVisible = feedBottomActionbarVisible;
    notifyListeners();
  }

  void setCurentArticalIndex(int curentArticalIndex) {
    this._curentArticalIndex = curentArticalIndex;
    notifyListeners();
  }

  void setfeedPageController(PageController pageController) {
    this._feedPageController = pageController;
    notifyListeners();
  }

  void setScreenController(PageController pageController) {
    this._screenController = pageController;
    notifyListeners();
  }

  void addDiscoverScren(Widget widget) {
    _baseScreen.add(widget);
    setDiscoverViewAdded();
    notifyListeners();
  }

  void addWebScren(Widget widget) {
    _baseScreen.add(widget);
    setWebViewAdded();
    notifyListeners();
  }

  void removeWebScren() {
    _baseScreen.removeAt(2);
    removeWebViewAdded();
    notifyListeners();
  }

  void removeDiscoverScren(Widget widget) {
    _baseScreen.remove(widget);
    notifyListeners();
  }

  void setNewsURL(String newsURL) {
    this._newsURL = newsURL;
    notifyListeners();
  }

  void setWebViewAdded() {
    this._webviwAdded = true;
    notifyListeners();
  }

  void setDiscoverViewAdded() {
    this._discoverviwAdded = true;
    notifyListeners();
  }

  void removeWebViewAdded() {
    this._webviwAdded = false;
    notifyListeners();
  }

  void setLastGetRequest(String request, String value) {
    _lastGetRequest.clear();
    _lastGetRequest.add(request);
    _lastGetRequest.add(value);
    notifyListeners();
  }


  Future<void> getAllCategoriesList(String local) async {
    print('api ::: categories  called');
    super.isLoading = true;
    try {
      AllCategoriesResponse response =
          await _repository.apiGetAllCategories(local);
      _onResponseAllCategoriesList(response);
      super.isLoading = false;
    } catch (e) {
     // log.e(e.toString());
      isLoading = false;
    }

  }

  void _onResponseAllCategoriesList(AllCategoriesResponse response) {
   if (response != null && response.success) {
     allCategoriesList = response.data;
   }
  }
  //for news
  Future<void> getAllNewsList(String local) async {
    print('api ::: apinews  called');
    super.isLoading = true;
    try {

      FilterNewsRequest filterNewsRequest = FilterNewsRequest(categoryId: "0",);
      AllNewsResponse response = await _allNewrepository.apiGetAllNews(local,filterNewsRequest,"0");
      _onResponseAllNewsList(response);
      super.isLoading = false;
    } catch (e) {
      isLoading = false;
    }
  }

  void _onResponseAllNewsList(AllNewsResponse response) {
    if (response != null && response.success) {
      allNewsList.clear();
      allNewsList = response.data;
      print("DATAAA ${allNewsList}");
    }
  }

  //filter  catogry
  Future<void> getFilterNewsList(String local, String categoryid) async {
    print('api :::  api filter news  called');
    super.isLoading = true;
    try {
      FilterNewsRequest filterNewsRequest = FilterNewsRequest(categoryId: categoryid,);
      AllNewsResponse response = await _filterNewrepository.apiGetFilterNewsList(local,filterNewsRequest);
      _onResponseFilterNewsList(response);
      super.isLoading = false;
    } catch (e) {
      isLoading = false;
    }
  }

  void _onResponseFilterNewsList(AllNewsResponse response) {
    if (response != null && response.success) {
      allNewsList.clear();
      allNewsList = response.data;
    }
  }

}
