import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:Two/model/news_model.dart';
import 'package:Two/services/news/news_service.dart';

import 'search_feed_event.dart';
import 'search_feed_state.dart';

class SearchFeedBloc extends Bloc<SearchFeedEvent, SearchFeedState> {
  NewsFeedRepository repository;
  SearchFeedBloc({@required this.repository});

  @override
  SearchFeedState get initialState => SearchFeedInitialState();

  @override
  Stream<SearchFeedState> mapEventToState(SearchFeedEvent event) async* {
  }
}
