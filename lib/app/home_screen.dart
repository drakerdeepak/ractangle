import 'dart:collection';

import 'package:Two/constant/shared_pref.dart';
import 'package:Two/controller/feed_controller.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/custom_icon_icons.dart';
import 'package:Two/saadh_dev/ui/home/article/article_view.dart';
import 'package:Two/saadh_dev/ui/home/community/community_view.dart';
import 'package:Two/saadh_dev/ui/home/post_article/post_article_view.dart';
import 'package:Two/saadh_dev/ui/home/profile/profile_view.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/view/app_base/app_base.dart';
import 'package:Two/view/post_screen/post_article.dart';
import 'package:Two/view/web_screen/web.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:Two/utils/widgethelper/widget_helper.dart';
import 'package:Two/view/feed_screen/feed.dart';
import 'package:Two/view/interest/interest_screen.dart';
import 'package:Two/view/profile/profile_screen.dart';
import 'package:Two/view/video/video_screen.dart';
import 'package:provider/provider.dart';

import '../aplication_localization.dart';



class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

/*Color color2 = HexColor("#dbf0fd");*/
Color color2 = HexColor("#63599E");
Color color_app = HexColor("#63599E");
PageController _pageController;
class HomeScreenNew extends StatefulWidget {

  @override
  _HomeScreenNewState createState() => _HomeScreenNewState();
}

class _HomeScreenNewState extends State<HomeScreenNew>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  ListQueue<int> _navigationQueue =ListQueue();

  BuildContext ctx;
  double width;
  double height;
 // PageController _pageController;
  int _page = 0;
  Color color_background = HexColor("#DEE7F7");
  Color color = HexColor("#63599E");
  Color color2 = HexColor("#ed0932");
  final TextStyle subtitle = TextStyle(fontSize: 12.0, color: Colors.grey);
  final TextStyle label = TextStyle(fontSize: 14.0, color: Colors.grey);
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  var provider;
  int currentPage = FeedController.getCurrentPage((page) {
   PageController _pageController;
    _pageController.jumpToPage(page);

    // print("jump+ "+page+" "+currentPage.toString());
  });
  @override
  void initState() {
     provider = Provider.of<SettingsProvider>(context, listen: false);
    super.initState();
   // getfeedPageController

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));

    _firebaseMessaging.getToken().then((token){
      print('fcm_token ,$token');
      SharePreference.setFcmToken(token);
    });
   // _tabController = new TabController(length: 4, vsync: this);
  }

  final List<Widget> _children = [

    //new DashboardPage(),
    //new DashboardNew(),
    //FeedScreen(articals: [],),
   /* BuildNewsScreen(),
    InterestScreen(),
    PostArticleScreen(),
    VideoScreen(),
    ProfileScreen(),*/
    BuildNewsScreen(),
    ArticleView(),
    PostArticleView(),
    CommunityView(),
    ProfileView(),

  ];

/*
  Widget _createUi(BuildContext context) {
    ctx = context;
    return Stack(
      children: <Widget>[
        VideoScreen(),
        InterestScreen(),
        ProfileScreen(),
        FeedScreen(articalIndex: 0, articals: getArticle(), isFromSearch: true),
      ],
    );
  }*/


  @override
  Widget build(BuildContext context) {
   // final GlobalBloc _globalBloc = Provider.of<GlobalBloc>(context);
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor:provider.isDarkThemeOn ? Colors.white
            : Colors.black ,
        statusBarColor: provider.isDarkThemeOn ? Colors.white
            : Colors.black ,
        statusBarIconBrightness: provider.isDarkThemeOn ? Brightness.dark
            : Brightness.light,
        statusBarBrightness: provider.isDarkThemeOn ? Brightness.dark
            : Brightness.light,
        systemNavigationBarIconBrightness: provider.isDarkThemeOn ? Brightness.dark
            : Brightness.light,
      ),
    );
    return new WillPopScope(
   onWillPop: _onWillPop,
      child: SafeArea(
        child: new Scaffold(

          body: _children[_page],
        //  body: _createUi(context),

         /* floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              *//*Rouut.navigator.pushNamed(
                  Rouut.postArticleScreen);*//*

              onTabTapped(2);

            },
            child: Icon(Icons.add),
            elevation: 4.0,
            backgroundColor: AppColor.grey2,
          ),*/
          bottomNavigationBar: new Theme(

            data: Theme.of(context).copyWith(
              // sets the background color of the `BottomNavigationBar`
           //   canvasColor: Colors.white,
              canvasColor: Theme.of(context).bottomAppBarColor,
              // sets the active color of the `BottomNavigationBar` if `Brightness` is light

            ), // sets the inactive color of the `BottomNavigationBar`
            child: new BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: _getNavBarItems(),
              onTap: onTabTapped,
              currentIndex: _page,
            ),
          ),
        ),
      ),
    );
  }



  Future<bool> _onWillPop() async {
    print("Current page ${currentPage}");
  if (currentPage == 0) {
    return (await showDialog(

      context: context,
      builder: (context) => AlertDialog(

        title: Text('Are you sure?',style: getFormarticletitleStyle(context).copyWith(color: provider.isDarkThemeOn ? Colors.white
          : Colors.black),),
        content:
        Text('Do you want to exit the App',style: getFormarticlesubtitleStyle(context).copyWith(color: provider.isDarkThemeOn ? Colors.white
            : Colors.black,)),
        actions: <Widget>[
          OutlineButton(
            padding: EdgeInsets.symmetric(horizontal: 50),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
      // AppTextStyle.newsTitle
      //     .copyWith(color: theme.isDarkThemeOn
      //     ? Colors.white
      //     : Colors.black,
            child: Text("No",
                style: TextStyle(
                    fontSize: 14,
                    letterSpacing: 2.2,
                    color:provider.isDarkThemeOn ? Colors.white
                        : Colors.black)),
          ),
          RaisedButton(
            onPressed: () {
              Navigator.of(context).pop(true);
            },
            color: appcolor,
            padding: EdgeInsets.symmetric(horizontal: 50),
            elevation: 2,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)),
            child: Text(
              "Yes",
              style: TextStyle(
                  fontSize: 14,
                  letterSpacing: 2.2,
                  color: provider.isDarkThemeOn ? Colors.white
                      : Colors.white),
            ),
          ),

        ],
      ),
    )) ??
        false;
  }


  else if (currentPage == 2) {
    FeedController.addCurrentPage(1);
  }
  else if (currentPage == 1) {
    FeedController.addCurrentPage(1);
  }

  }


  // Future<bool> _onWillPop() {
  //   return showDialog(
  //     context: context,
  //     builder: (context) => Dialog(
  //       shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.circular(10),
  //       ),
  //       elevation: 0.0,
  //       backgroundColor: Colors.transparent,
  //       child: dialogContent(context),
  //     ),
  //   ) ?? false;
  // }


  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        //...bottom card part,
        //...top circlular image part,
        Container(
          padding: EdgeInsets.only(
            top: Consts.avatarRadius /*+ Consts.padding*/,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          margin: EdgeInsets.only(top: Consts.avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              SizedBox(height: 7.0),
              Text("ARE YOU SURE?",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 19,
                ), textAlign: TextAlign.center,),
              SizedBox(height: 5.0),
              Flexible(
                child: Text(
                  "you want to exit this app!!", style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 15,
                ), textAlign: TextAlign.center,),
              ),
              SizedBox(height: 25.0),
              Container(color: color_background, width: width, height: 1.5,),
              SizedBox(height: 15.0),
              Row(children: <Widget>[
                Expanded(
                  child:
                  RaisedButton(
                    child: Text("No", style: TextStyle(fontSize: 15),),
                    color: color,
                    padding: EdgeInsets.all(13.0),
                    colorBrightness: Brightness.dark,
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  child: RaisedButton(
                    child: Text("Yes", style: TextStyle(fontSize: 15),),
                    color: color,
                    padding: EdgeInsets.all(13.0),
                    colorBrightness: Brightness.dark,
                    onPressed: () {
                      // Navigator.of(context).pop(true);
                      SystemNavigator.pop();
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ),
                // SizedBox(height: 10.0,),
              ],

              ),
            ],
          ),
        ),
        Positioned(
          left: Consts.padding,
          right: Consts.padding,
          child: new Image.asset("", width: 85, height: 85,),
        ),
      ],
    );
  }


  List<BottomNavigationBarItem> _getNavBarItems() {
    final provider = Provider.of<SettingsProvider>(context, listen: false);
    //  if (mediaType == MediaType.movie) {
    return [

      BottomNavigationBarItem(
          icon: Image.asset("assets/icons/icon_news.png",color: provider.isDarkThemeOn
              ? AppColor.surface
              : AppColor.grey2,),

          // Icon(
          //   CustomIcon.home_news_icon,size: 17.0,
          //   color: provider.isDarkThemeOn
          //       ? AppColor.surface
          //       : AppColor.grey2,
          // ),
          title: Text(AppLocalizations.of(context).translate("news"),style: TextStyle(
            color: _page == 0 ? Colors.black : Colors.grey,
          ),)),
      BottomNavigationBarItem(
          icon:Image.asset("assets/icons/article.png",color: provider.isDarkThemeOn
              ? AppColor.surface
              : AppColor.grey2,),
    // Icon(
    //         CustomIcon.home_artical_icon,size: 17.0,
    //         color: provider.isDarkThemeOn
    //             ? AppColor.surface
    //             : AppColor.grey2,
    //       ),
          title: Text(AppLocalizations.of(context).translate("article"),style: TextStyle(
            color: _page == 1 ? Colors.black : Colors.grey,
          ),)),


     BottomNavigationBarItem(
          icon:    Container(

            decoration: BoxDecoration(
                color:Colors.grey.shade700,
                border: Border.all(
                    color: Colors.grey.shade700,
                ),
                borderRadius: BorderRadius.circular(30)
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child:
              Icon(Icons.add,
                color: provider.isDarkThemeOn
                    ? AppColor.surface
                    : AppColor.onPrimary,
               size: 25.0,),
            ),
          ),
         //new Image.asset("assets/icons/news_icon.png",color:Colors.red),
          title: Text('')
          ),


      BottomNavigationBarItem(
          /*icon:  new Image.asset(reports_icon,width: 26,height: 26,),
          activeIcon:new Image.asset(reports_icon,width: 26,height: 26,color: color_app,),*/
          icon:Image.asset("assets/icons/community.png",color: provider.isDarkThemeOn
              ? AppColor.surface
              : AppColor.grey2,),


          // Icon(
          //   CustomIcon.home_community_icon,size: 17.0,
          //   color: provider.isDarkThemeOn
          //       ? AppColor.surface
          //       : AppColor.grey2,
          // ),
          title: Text(AppLocalizations.of(context).translate("community"), style: TextStyle(
            color: _page == 3 ? Colors.black : Colors.grey,
          ), )),
      BottomNavigationBarItem(
          /*icon:  new Image.asset(about_icon,width: 26,height: 26,),
          activeIcon:new Image.asset(about_icon,width: 26,height: 26,color: color_app,),*/
         icon:Image.asset("assets/icons/profile.png",color: provider.isDarkThemeOn
             ? AppColor.surface
             : AppColor.grey2,),
          // Icon(
          //   CustomIcon.home_profile_icon,size: 17.0,
          //   color: provider.isDarkThemeOn
          //       ? AppColor.surface
          //       : AppColor.grey2,
          // ),
          title: Text(AppLocalizations.of(context).translate("profile"),style: TextStyle(
            color: _page == 4 ? Colors.black : Colors.grey,
          ), )),


    ];
  }


  void onTabTapped(int index) {
    setState(() {
      final provider1 = Provider.of<FeedProvider>(context, listen: false);
     // PageController _pageController;
     // int currentPage = 1;

      if(index==0) {
        print("pages12 +" + index.toString());
        provider1.setAppBarVisible(true);

        // provider1.webviwAdded == false;
        if (provider1.webviwAdded == false) {
          print("webviwAdded - " + provider1.webviwAdded.toString());
          provider1.addWebScren(
            WebScreen(
              url: provider1.getNewsURL,
              isFromBottom: false,
            ),
          );
        }

       // print("index0 - " + index.toString());
       // _page = index;
      }


   //   if(index==1 || index==2 || index==3 || index==4){

      else {

        print("page11 +"+ index.toString());

     //   _page = index;

        provider1.setAppBarVisible(false);
       // provider1.removeDiscoverScren(widget)

        print("webviwAdded - " + provider1.webviwAdded.toString());

       if (provider1.webviwAdded == true) {
          print("11 + ");
          provider1.webviwAdded == false;
          provider1.removeWebScren();
        }


       /* if (provider1.webviwAdded == true && provider1.discoverviwAdded == true) {
          print("11 + ");
          provider1.webviwAdded == false;
          provider1.discoverviwAdded == false;
          provider1.removeWebScren();
          provider1.removeDiscoverScren(widget);
        }*/

       // print("app_bar11 - " + provider1.getAppBarVisible.toString());
        /*print("index - " + index.toString());
        _page = index;*/

      }
      print("index - " + index.toString());
/*
      _navigationQueue.addLast(index);
      setState(() => index = index);
      print(index);*/
      _page = index;
    });

  }

     /* else if(index==2) {

        print("page13 +"+ index.toString());

        provider1.setAppBarVisible(false);

       // provider1.removeWebScren();
        if (provider1.webviwAdded == true) {
          provider1.removeWebScren();
        }


       // print("app_bar13 - " + provider1.getAppBarVisible.toString());
        print("index2 - " + index.toString());
        _page = index;

      }

      else if(index==3) {

        print("page14 +"+ index.toString());

        provider1.setAppBarVisible(false);

      //  provider1.removeWebScren();
        if (provider1.webviwAdded == true) {
          provider1.removeWebScren();
        }


      //  print("app_bar14 - " + provider1.getAppBarVisible.toString());
        print("index3 - " + index.toString());
        _page = index;

      }

      else if(index==4) {

        print("page15 +"+ index.toString());

        provider1.setAppBarVisible(false);

       // provider1.removeWebScren();
        if (provider1.webviwAdded == true) {
          provider1.removeWebScren();
        }

       // print("app_bar15 - " + provider1.getAppBarVisible.toString());

       // print("index4 - " + index.toString());

      }

    });*/



  void _openPage(Object page, BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context) => page
        )
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 40;
}

