import 'package:shared_preferences/shared_preferences.dart';

class SharePreference {
  static setLastIndex(int index) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('lastIndex', index);
  }

  static Future<int> getLastIndex() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('lastIndex');
  }

  static clear() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
  }

  static Future<String> getFcmToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(SharedPreferenceKeys.FCM_TOKEN);
  }

  static Future<void> setFcmToken(String fcm_token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('fcm_token_save: $fcm_token');
    return prefs.setString(SharedPreferenceKeys.FCM_TOKEN, fcm_token);
  }

}


class SharedPreferenceKeys {
  static const String IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN";
  static const String USER = "USER";
  static const String USER_ID = "USER_ID";
  static const String NUMBER_OF_DAYS = "NUMBER_OF_DAYS";
  static const String APPO_BOOK_ID = "APPO_BOOK_ID";
  static const String MEDICATION_ID = "MEDICATION_ID";
  static const String PRESCRIPTION_ID = "PRESCRIPTION_ID";
  static const String FCM_TOKEN = "FCM_TOKEN";
  static const String PROFILE_URL = "PROFILE_URL";
  static const String MORNINGSLOT = "MORNINGSLOT";
  static const String APPOINTMENT_DATE = "APPOINTMENT_DATE";
  static const String APPOINTMENT_TIME = "APPOINTMENT_TIME";
  static const String SLOT_TIME = "SLOT_TIME";
  static const String PATIENT_NAME = "PATIENT_NAME";
  static const String PATIENT_AGE = "PATIENT_AGE";
  static const String PATIENT_PHONE = "PATIENT_PHONE";
  static const String PATIENT_PROBLEM_DESCRIPTION = "PATIENT_PROBLEM_DESCRIPTION";
}
