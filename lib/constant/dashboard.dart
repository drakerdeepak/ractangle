class Dashboard {
  int id;
  String category_id;
   String category_name;
   String category_code;
   String image;

 /* "categoryList": [
  {
  "category_id": "609bd1707444804edb217e02",
  "category_name": "Automobile",
  "category_code": "automobile",
  "image": "https://twogroup.piousindia.com/uploads/category/automobile.png"
  },*/

  Dashboard.fromJSON(Map<String, dynamic> jsonMap) :
       // id = jsonMap['id'],
        category_id = jsonMap['category_id'],
        category_name = jsonMap['category_name'],
        category_code = jsonMap['category_code'],
        image = jsonMap['image'];

/*
  Dashboard.fromTIMEJSON(Map<String, dynamic> jsonMap) :
        time = jsonMap['time'],
        slot_status  = jsonMap['slot_status'],
        slot_time  = jsonMap['slot'],
        empty_msg = jsonMap['empty_msg'];
*/

}