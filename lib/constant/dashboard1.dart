class Dashboard1  {
  String status;
  String message;
  List<Categories> categoryList;

  Dashboard1({this.status, this.message, this.categoryList});

  Dashboard1.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];

    print('message'+message);

    if (json['categoryList'] != null) {
      categoryList = new List<Categories>();

      json['categoryList'].forEach((v) {
        categoryList.add(new Categories().fromJson(v));

      });
    }

  }

 /* Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.categoryList != null) {
      data['categoryList'] = this.categoryList.map((v) => v.toJson()).toList();
    }
    return data;
  }*/
}


class Categories {

  String category_id;
  String category_name;
  String category_code;
  String image;

  Categories(
      {this.category_id,
        this.category_name,
        this.category_code,
        this.image,
        });

  /*Categories.fromJson(Map<String, dynamic> json) {

    category_id = json['category_id'];
    category_name = json['category_name'];
    category_code = json['category_code'];
    image = json['image'];
  }*/

  Categories fromJson(Map<String, dynamic> json) {
    category_id = json['category_id'];
    category_name = json['category_name'];
    category_code = json['category_code'];
    image = json['image'];

  }

 /* Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['author'] = this.author;
    data['title'] = this.title;
    data['description'] = this.description;
    data['url'] = this.url;
    data['urlToImage'] = this.urlToImage;
    data['publishedAt'] = this.publishedAt;
    data['content'] = this.content;
    return data;
  }*/
}

/*{
  int id;
  String category_id;
   String category_name;
   String category_code;
   String image;

  Dashboard1.fromJSON(Map<String, dynamic> jsonMap) :
       // id = jsonMap['id'],
        category_id = jsonMap['category_id'],
        category_name = jsonMap['category_name'],
        category_code = jsonMap['category_code'],
        image = jsonMap['image'];

}*/