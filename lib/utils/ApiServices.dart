import 'dart:convert';

import 'package:Two/saadh_dev/core/service/independent_services/constant.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

import 'APIResponse/RegisterResponse.dart';



class ApiServices{

    static Future<RegisterResponse> fetchRegisterData(String FirstName,
        String LastName,String UserName,
        String phone, String email,
        String password) async {
        try {


            Map data = {
                /*'branch': branch,*/
                'firstname': FirstName.trim(),
                'lastname':LastName.trim(),
                'name': UserName.trim(),
                'phone': phone.trim(),
                'password': password.trim(),
                'password_confirmation': password.trim(),
                'email':email.trim(),
            };
            print("data ${data}");
            //encode Map to JSON
            var body = json.encode(data);

            var res = await http.post(
                Uri.parse(Constant.BASE_URL + 'register'),  body: data);
            print("${res.statusCode}");
            print("${res.body}");

                var decodedValue = jsonDecode(res.body);
                print("$decodedValue");
                RegisterResponse craeteLiveClassNowRes = RegisterResponse.fromJson(decodedValue);


                return craeteLiveClassNowRes;


        } on Exception catch (e) {
            return null;
        }
    }



    static Future<RegisterResponse> fetchLoginData(String password,  String email, ) async {
        try {


            Map data = {
                /*'branch': branch,*/
                'password': password.trim(),
                'email':email.trim(),
            };
            print("data ${data}");
            //encode Map to JSON
            var body = json.encode(data);

            var res = await http.post(
                Uri.parse(Constant.BASE_URL + 'login'),  body: data);
            print("${res.statusCode}");
            print("${res.body}");

            var decodedValue = jsonDecode(res.body);
            print("$decodedValue");
            RegisterResponse craeteLiveClassNowRes = RegisterResponse.fromJson(decodedValue);


            return craeteLiveClassNowRes;


        } on Exception catch (e) {
            return null;
        }
    }
}