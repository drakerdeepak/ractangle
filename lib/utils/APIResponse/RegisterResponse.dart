class RegisterResponse {
  bool success;
  List<Message> message;
  Data data;

  RegisterResponse({this.success, this.message, this.data});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Message {
  String message,name,email,phone;

  Message({this.message,this.name,this.email,this.phone});

  Message.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    return data;
  }
}

class Data {
  String name;
  String firstname;
  String email;
  String phone;
  String roleId;
  String updatedAt;
  String createdAt;
  String sId;
  String accessToken;

  Data(
      {this.name,
        this.firstname,
        this.email,
        this.phone,
        this.roleId,
        this.updatedAt,
        this.createdAt,
        this.sId,
        this.accessToken});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    firstname = json['firstname'];
    email = json['email'];
    phone = json['phone'];
    roleId = json['role_id'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    sId = json['_id'];
    accessToken = json['accessToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['firstname'] = this.firstname;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['role_id'] = this.roleId;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['_id'] = this.sId;
    data['accessToken'] = this.accessToken;
    return data;
  }
}
