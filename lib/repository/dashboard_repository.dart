import 'package:Two/app/dio/dio.dart';
import 'package:Two/constant/dashboard.dart';
import 'package:Two/constant/dashboard1.dart';
import 'package:Two/model/news_model.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


Future<Stream<Dashboard>> getDashboardCategories(String url) async {

  final String url_new = url;
  print(url_new);

  final client = new http.Client();
  final streamedRest = await client.send(
      http.Request('get', Uri.parse(url_new))
  );

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .expand((data) => (data as List))
      .map((data) => Dashboard.fromJSON(data));
}


/*
@override
Future<List<Categories>> getDashboardCategories(String url) async {

  final String url_new = url;
  print(url_new);

  Response response = await GetDio.getDio().get(url);

  print("getDashboardRes "+response.toString());
  if (response.statusCode == 200) {
    print('status_code '+response.statusCode.toString());
    List<Categories> categoryList = Dashboard1.fromJson(response.data).categoryList;
    print('categoryList '+categoryList.toString());
    return categoryList;
  } else {
   // provider.setDataLoaded(true);
    throw Exception();
  }
}*/
