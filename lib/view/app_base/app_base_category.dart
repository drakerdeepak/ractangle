import 'package:Two/view/discover_screen/discover.dart';
import 'package:Two/view/feed_screen/feedbookmark.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Two/bloc/feed/news_feed_bloc.dart';
import 'package:Two/bloc/feed/news_feed_event.dart';
import 'package:Two/bloc/feed/news_feed_state.dart';
import 'package:Two/common/loading_shorts.dart';
import 'package:Two/common/widgets/appbar.dart';
import 'package:Two/controller/feed_controller.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/style/text_style.dart';
import 'package:Two/view/feed_screen/feed.dart';
import 'package:Two/view/web_screen/web.dart';
import 'package:provider/provider.dart';

import '../../aplication_localization.dart';

class AppBaseCategory extends StatefulWidget {
  @override
  _AppBaseState createState() => _AppBaseState();
}

class _AppBaseState extends State<AppBaseCategory> with AutomaticKeepAliveClientMixin {
  int currentPage = 1;
  PageController _pageController;
  FeedProvider provider;

  @override
  Future<void> initState()  {
    provider = Provider.of<FeedProvider>(context, listen: false);

    // provider.getAllNewsList("languagecode");

    BlocProvider.of<NewsFeedBloc>(context)
      ..add(
        FetchNewsByCategoryEvent(category: "general"),
      );

    _pageController = PageController(
      initialPage: currentPage,
    );

    provider.setScreenController(_pageController);

    FeedController.getCurrentPage((page) {
        _pageController.jumpToPage(page);

     // print("jump+ "+page+" "+currentPage.toString());
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Consumer<FeedProvider>(
      builder: (context, value, child) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            PageView(
              controller: _pageController,
              onPageChanged: (page) {
                currentPage = _pageController.page.round();
                if (currentPage == 2) {
                  value.setAppBarVisible(false);
                } else {
                  value.setAppBarVisible(false);
                }
              },
              children: provider.getBaseScreenList,
            ),
            value.getAppBarVisible
                ? Align(
                    alignment: Alignment.topCenter,
                    child: CustomAppBar(
                      index: currentPage,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class BuildNewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<FeedProvider>(context, listen: false);
    String languagecode = AppLocalizations.of(context).locale.languageCode;
    print(" languagecode "+languagecode);
      provider.getFilterNewsList(languagecode,"");
    return BlocBuilder<NewsFeedBloc, NewsFeedState>(
      builder: (context, state) {
        if (state is NewsFeedInitialState) {
          return Container();
        } else if   (state is NewsFeedLoadingState) {
          return LoadingShorts();
        } else if (state is NewsFeedLoadedState) {
          if (provider.filterNewsList.length == 1) {
          //  provider.getAllNewsList("en");
            return Center(
              child: Text(
                "${AppLocalizations.of(context).translate('not_found')}\n",
                style: AppTextStyle.newsTitle,
              ),
            );
          }

         if (provider.webviwAdded == false) {
            provider.addWebScren(
              WebScreen(
                url: provider.getNewsURL,
                isFromBottom: false,
              ),
            );
          }

/*
          if (provider.discoverviwAdded == false) {
            provider.addDiscoverScren(
              DiscoverScreen(),
            );
          }*/

          if (provider.filterNewsList.length == 0) {
         //   provider.getAllNewsList("en");
            return Center(
              child: Text(
                "${AppLocalizations.of(context).translate('not_found')}\n",
                style: AppTextStyle.newsTitle,
              ),
            );
          }
          else if(provider.filterNewsList.length>1) {
             print("feeddd");
             return FeedScreen(
              isFromSearch: false,
              articalIndex: provider.filterNewsList.length,
          //    articals: state.news,
            );
          } else {
             print("feebookkk");
             return FeedBookmarkScreen(
               isFromSearch: false,
               articalIndex: provider.filterNewsList.length,
              // articals: state.news,
             );
          }
        } else if (state is NewsFeedErrorState) {
          provider.setDataLoaded(true);
          print(state.message);
          return Container(
            height: double.maxFinite,
            width: double.maxFinite,
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  AppLocalizations.of(context).translate('error'),
                  style: AppTextStyle.newsTitle,
                ),
                SizedBox(height: 8),
                Text(
                  AppLocalizations.of(context).translate('error_message'),
                  style: AppTextStyle.searchbar,
                  textAlign: TextAlign.center,
                )
              ],
            ),
          );
        }
        return Container();
      },
    );
  }
}




