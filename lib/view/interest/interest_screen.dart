import 'dart:async';

import 'package:Two/controller/feed_controller.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/style/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';


class InterestScreen extends StatefulWidget {
  @override
  _InterestScreenState createState() => _InterestScreenState();
}

class _InterestScreenState extends State<InterestScreen> {

  bool loading = true;

  final Completer<WebViewController> _webViewController =
  Completer<WebViewController>();

  Icon cusIcon = Icon(Icons.search);
  Widget cusSearchBar = Image.asset("assets/icons/community_icon.png",width: 40,height: 50,
    color: Colors.black);

  String articleUrl = '';

  @override
  Widget build(BuildContext context) {

    String url = "https://twogroup.piousindia.com/article-listing";
    final provider1 = Provider.of<FeedProvider>(context, listen: false);
    final provider = Provider.of<SettingsProvider>(context, listen: false);

    provider1.setAppBarVisible(false);

      IconData _backIcon() {
      switch (Theme
          .of(context)
          .platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.arrow_back_ios;
        case TargetPlatform.iOS:
          return Icons.arrow_back;
        case TargetPlatform.linux:
        // TODO: Handle this case.
          break;
        case TargetPlatform.macOS:
        // TODO: Handle this case.
          break;
        case TargetPlatform.windows:
        // TODO: Handle this case.
          break;
      }
      assert(false);
      return null;
    }

    return new Scaffold(

        appBar: AppBar(
          elevation: 4,
          centerTitle: true,

          leading: IconButton(
              icon: new Image.asset("assets/icons/back_arrow.png",color: provider.isDarkThemeOn
                  ? Colors.white
                  : Colors.black),
              onPressed: () {

                Rouut.navigator.pushNamed(
                    Rouut.appBase);

              }),
          actions: <Widget>[

        IconButton(
          icon: cusIcon,
      onPressed: () {
        setState(() {

          if(cusIcon.icon== Icons.search){

            print("icon");

            cusIcon = Icon(Icons.cancel);

            cusSearchBar = TextField(
               // textInputAction: TextInputAction.go,
              textInputAction: TextInputAction.search,
              onSubmitted: (value) {
                Fluttertoast.showToast(
                  msg: value,
                  backgroundColor: Colors.black,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 3,
                  toastLength: Toast.LENGTH_SHORT,
                );

                print("articleUrl1+ "+value);
              },
                decoration: InputDecoration(
                    border: InputBorder.none,
                  hintText: "Search Article"
                ),

               style: TextStyle(
                  color: provider.isDarkThemeOn
               ? Colors.white
                   : Colors.black,

                 fontSize: 16,
               ),
              );

          } else{

            print("icon1");
            cusIcon = Icon(Icons.search,color: provider.isDarkThemeOn
                ? Colors.white
                : Colors.black);

            cusSearchBar = Image.asset("assets/icons/community_icon.png",width: 40,height: 50,
              color: provider.isDarkThemeOn
                  ? Colors.white
                  : Colors.black,);

          }

        });

      },

    ),


    Padding(
    padding: EdgeInsets.only(right: 0.0),
    child:FutureBuilder<WebViewController>(
                future: _webViewController.future,
                builder: (BuildContext context,
                    AsyncSnapshot<WebViewController> snapshot) {
                  final bool webViewReady =
                      snapshot.connectionState == ConnectionState.done;
                  final WebViewController controller = snapshot.data;
                  return IconButton(

                    icon: new Image.asset("assets/icons/refresh_icon.png",
                    color: provider.isDarkThemeOn
                        ? Colors.white
                        : Colors.black),
                    onPressed: !webViewReady
                        ? null
                        : () {
                      setState(() {
                        loading = true;
                      });
                      controller.reload();
                    },
                  );
                })),

            Padding(
                padding: EdgeInsets.only(right: 0.0),
                child: GestureDetector(
                  onTap: () {},
                  child: new Image.asset("assets/icons/notification_icon.png",
                color: provider.isDarkThemeOn
                ? Colors.white
                : Colors.black),
                  ),
                )

          ],
          title: cusSearchBar,

          backgroundColor: provider.isDarkThemeOn
              ? Theme.of(context).bottomAppBarColor
              : AppColor.background,
        ),

        body:  Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            loading
                ? SizedBox(height: 3, child: LinearProgressIndicator())
                : Container(),
            Expanded(
              child: WebView(
                initialUrl: url,
                debuggingEnabled: true,
                javascriptMode: JavascriptMode.unrestricted,
                gestureNavigationEnabled: true,
                onPageFinished: (d) {
                  setState(() {
                    loading = false;
                  });
                },
                onWebViewCreated: (WebViewController webViewController) {
                  _webViewController.complete(webViewController);
                },
              ),
            ),
          ],
        ),

    );
  }
}



