import 'package:carousel_slider/carousel_slider.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ExpandedImageView extends StatelessWidget {
  final List image;

  ExpandedImageView({@required this.image});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: Container(
                height: 500.0,
                child: CarouselSlider.builder(
                  options: CarouselOptions(
                    //height: MediaQuery.of(context).size.height,
                    autoPlay: false,
                  ),
                  itemCount: image.length,
                  itemBuilder: (context, itemIndex, realIndex) {
                    return Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: GestureDetector(
                        child: Container(
                          margin: const EdgeInsets.only(
                              top: 0.0, left: 5.0),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(image[itemIndex]),
                              fit: BoxFit.cover,
                            ),
                            // border:
                            //     Border.all(color: Theme.of(context).accentColor),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                      ),

                      //Text(newsInfo.newsImage[itemIndex]),
                    );
                  },
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                    icon: Icon(
                      FeatherIcons.x,
                      size: 30,
                      color: Colors.black,
                    ),
                    onPressed: () => Navigator.pop(context)),
              ),
            )
          ],
        ),
      ),
    );
  }
}
