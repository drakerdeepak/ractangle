import 'package:Two/common/widgets/news_appbar.dart';
import 'package:Two/constant/shared_pref.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/view/feed_screen/news_card_view.dart';
import 'package:Two/view/web_screen/web.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FeedBookmarkScreen extends StatefulWidget {
  final int articalIndex;
  final bool isFromSearch;

  const FeedBookmarkScreen(
      {Key key,
      @required this.articalIndex,
      @required this.isFromSearch})
      : super(key: key);

  @override
  _FeedBookmarkScreenState createState() => _FeedBookmarkScreenState();
}

class _FeedBookmarkScreenState extends State<FeedBookmarkScreen>
    with AutomaticKeepAliveClientMixin {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    PageController _pageController =
        PageController(initialPage: widget.articalIndex);
    PageController _searchPageController = PageController();

    int lastPage = 0;
    final provider = Provider.of<FeedProvider>(context, listen: false);

    provider.setfeedPageController(_pageController);
    int prevIndex = index <= 0 ? 0 : index - 1;
    int nextIndex = index == provider.allNewsList.length - 1 ? 0 : index + 1;

    return Scaffold(
      backgroundColor: Colors.black,
      body: widget.isFromSearch
          ? PageView(
              controller: _searchPageController,
              children: [
                Stack(
                  fit: StackFit.expand,
                  children: [
                    PageView.builder(
                      onPageChanged: (page) {
                        if (page > lastPage) {
                          provider.setSearchAppBarVisible(false);
                          provider.setAppBarVisible(false);
                        } else {
                          provider.setSearchAppBarVisible(true);
                          provider.setAppBarVisible(true);
                        }
                        provider.setFeedBottomActionbarVisible(false);

                        lastPage = page;
                        provider.setCurentArticalIndex(page);
                      },
                      controller: _pageController,
                      itemCount: provider.allNewsList.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) => NewsCardView(
                        provider.allNewsList[index],
                      ),
                    ),
                    widget.isFromSearch
                        ? Consumer<FeedProvider>(
                            builder: (context, value, child) =>
                                value.getSearchAppBarVisible
                                    ? Align(
                                        alignment: Alignment.topCenter,
                                        child: NewsCardAppBar(),
                                      )
                                    : Container(),
                          )
                        : Container()
                  ],
                ),
                WebScreen(
                  url: provider.getNewsURL,
                  isFromBottom: false,
                  pageController: _searchPageController,
                )
              ],
            )
          : Stack(
              fit: StackFit.expand,
              children: [
                PageView.builder(
                  allowImplicitScrolling: false,
                  onPageChanged: (page) {
                    if (page > lastPage) {
                      provider.setSearchAppBarVisible(false);
                      provider.setAppBarVisible(false);
                    } else {
                      provider.setSearchAppBarVisible(true);
                      provider.setAppBarVisible(true);
                    }
                    lastPage = page;
                    provider.setCurentArticalIndex(page);
                    provider.setFeedBottomActionbarVisible(false);
                  },
                  controller: _pageController,
                  itemCount: provider.allNewsList.length,
                  scrollDirection: Axis.vertical,
                  pageSnapping: true,
                  physics: ScrollPhysics(),
                  itemBuilder: (context, index) => NewsCardView(
                    provider.allNewsList[index],
                  ),
                ),
                widget.isFromSearch
                    ? Consumer<FeedProvider>(
                        builder: (context, value, child) =>
                            value.getSearchAppBarVisible
                                ? Align(
                                    alignment: Alignment.topCenter,
                                    child: NewsCardAppBar(),
                                  )
                                : Container(),
                      )
                    : Container()
              ],
            ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  void updateContent(direction) {
    final provider = Provider.of<FeedProvider>(context, listen: false);
    if (index <= 0 && direction == DismissDirection.down) {
      index = provider.allNewsList.length - 1;
    } else if (index == provider.allNewsList.length - 1 &&
        direction == DismissDirection.up) {
      index = 0;
    } else if (direction == DismissDirection.up) {
      index++;
    } else {
      index--;
    }
    updateIndex(index);
  }

  void updateIndex(newIndex) {
    setState(() {
      index = newIndex;
    });
    SharePreference.setLastIndex(newIndex);
  }

  void setupLastIndex() async {
    final provider = Provider.of<FeedProvider>(context, listen: false);
    int lastIndex = await SharePreference.getLastIndex();
    if (lastIndex != null && lastIndex < provider.allNewsList.length - 1) {
      updateIndex(lastIndex);
    }
  }
}
