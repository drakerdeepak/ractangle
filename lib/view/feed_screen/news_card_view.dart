import 'dart:ui';

import 'package:Two/aplication_localization.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/model/response_all_news.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_images.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/slide_right_roure.dart';
import 'package:Two/saadh_dev/ui/login/login_view.dart';
import 'package:Two/services/news/offline_service.dart';
import 'package:Two/services/news/share_service.dart';
import 'package:Two/style/text_style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_multi_carousel/carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view.dart';
import 'package:pinch_zoom/pinch_zoom.dart';
import 'package:provider/provider.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import 'package:carousel_slider/carousel_slider.dart';
class NewsCardView extends StatefulWidget {
  final NewsInfo newsInfo;
  NewsCardView(this.newsInfo);

  @override
  _NewsCardViewState createState() => _NewsCardViewState();
}

class _NewsCardViewState extends State<NewsCardView> {
  GlobalKey _containerKey = GlobalKey();

  PlayerState _playerState;
  YoutubeMetaData _videoMetaData;
  double _volume = 100;
  bool _muted = false;
  bool _isPlayerReady = true;

  GlobalKey<CarouselSliderState> _sliderKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  build(BuildContext context) {
    final provider = Provider.of<FeedProvider>(context, listen: true);
    print("URL card_article - " + widget.newsInfo.sourceURL);
    provider.setNewsURL(widget.newsInfo.sourceURL);
    // print("card_article - " + widget.newsInfo.sourceURL);

    
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          provider.setAppBarVisible(!provider.getAppBarVisible);
          provider.setSearchAppBarVisible(!provider.getSearchAppBarVisible);
          provider.setFeedBottomActionbarVisible(!provider.getFeedBottomActionbarVisible);
        },
        child: SafeArea(
          // bottom: true,
          child: _buildCard(context, provider),
        ),
      ),
    );
  }

  Widget _buildCard(BuildContext context, provider) {
    GlobalKey _containerKey = GlobalKey();
    return Consumer<FeedProvider>(builder: (context, value, _) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(0),
        child: RepaintBoundary(
          key: _containerKey,
          child: Column(
            children: <Widget>[
              if (widget.newsInfo.newsImage.length > 0)
                _buildPostImage(context),
              if (widget.newsInfo.youTubeUrl != null)
                _newsimage(context),
              newscardview(context),
              Flexible(
                child:  Consumer<SettingsProvider>(
                  builder: (context,
                      theme, child) =>
                      Container(
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0),
                        width:
                        MediaQuery.of(context).size.width,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 2.0, right: 2.0),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment
                                  .spaceEvenly,
                              children: [
                                Row(children: [
                                  Consumer<SettingsProvider>(
                                      builder: (context,
                                          theme, child) =>
                                          IconButton(
                                            icon: Icon(
                                              Icons
                                                  .favorite_border_outlined,
                                              //CustomIcon.like_icon,
                                              color: theme
                                                  .isDarkThemeOn
                                                  ? Colors
                                                  .white
                                                  : Colors
                                                  .black,
                                              size: 20,
                                            ),
                                            alignment:
                                            Alignment
                                                .topCenter,
                                            onPressed: () {
                                              Navigator.push(
                                                context,
                                                SlideRightRoute(
                                                    page:
                                                    LoginView()),
                                              );
                                            },
                                          )),
                                ]),
                                Row(
                                  children: [
                                    Consumer<SettingsProvider>(
                                        builder: (context,
                                            theme,
                                            child) =>
                                            IconButton(
                                              icon:  Image.asset(
                                                'assets/new_icons/swap.png',
                                                //CustomIcon.comments_like,
                                                color: theme
                                                    .isDarkThemeOn
                                                    ? Colors
                                                    .white
                                                    : Colors
                                                    .black,
                                                height: 25,width: 20,
                                              ),
                                              alignment:
                                              Alignment
                                                  .topCenter,
                                              onPressed:
                                                  () {},
                                            )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Consumer<   SettingsProvider>(
                                      builder: (context,
                                          theme, child) =>
                                          IconButton(
                                            icon:  Image.asset(
                                            'assets/new_icons/share.png',
                                            //CustomIcon.comments_like,
                                              color: theme
                                                  .isDarkThemeOn
                                                  ? Colors
                                                  .white
                                                  : Colors
                                                  .black,
                                            height: 25,width: 20,
                                            ),
                                            alignment: Alignment
                                                .topCenter,
                                            onPressed: () {
                                              Provider.of<FeedProvider>(
                                                  context,
                                                  listen:
                                                  false)
                                                  .setWatermarkVisible(
                                                  true);

                                              Future.delayed(
                                                  Duration(
                                                      seconds: 2),
                                                      () => convertWidgetToImageAndShare(
                                                      context,
                                                      _containerKey
                                                  ));
                                            },
                                          ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Consumer<
                                        SettingsProvider>(
                                        builder: (context,
                                            theme,
                                            child) =>
                                            IconButton(
                                              icon:  Image.asset(
                                                'assets/new_icons/bookmark.png',
                                                //CustomIcon.comments_like,
                                                color: theme
                                                    .isDarkThemeOn
                                                    ? Colors
                                                    .white
                                                    : Colors
                                                    .black,
                                                height: 25,width: 20,
                                              ),
                                              alignment:
                                              Alignment
                                                  .topCenter,
                                              onPressed: () {
                                                handleBookmarks(widget.newsInfo);
                                              },
                                            )),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                ),



              ),
            ],
          ),
        ),
      );
    }
    );
  }


  Widget _buildPostImage(BuildContext context) {
    return Stack(
      // crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          // height: MediaQuery.of(context).size.height / 3,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: widget.newsInfo.newsImage.length,
              itemBuilder: (BuildContext context, int index) {
                return _buildImageItem(index, widget.newsInfo.newsImage);
              }), // CarouselSlider(
        ),
    widget.newsInfo.newsImage.length>1?
    Positioned(
        right: 5,
        bottom: 5,
        child: Container(
          height: 50,
            width: 50,
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.5),
                shape: BoxShape.circle
            ),
            child: Icon(Icons.arrow_forward_ios_rounded,color: Colors.white,)))
        :
    Container()
      ],
    );
  }

  Widget _buildImageItem(int index, List<dynamic> images) {

    print("LENGTH ${images.length}");
    return Container(
      // decoration: BoxDecoration(
      //     border: Border.all(color: Colors.grey[300])
      // ),
      child: Padding(
        padding: const EdgeInsets.only(bottom:0.0),
        child: Row(
          mainAxisSize:MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          /*  Container(
                child: CarouselSlider.builder(
                  itemCount: images.length,
                  options: CarouselOptions(
                    autoPlay: false,
                    aspectRatio: 20/10,
                    enlargeCenterPage: true,
                    reverse: false,
                    enableInfiniteScroll: false
                  ),
                  itemBuilder: (context, index, realIdx) {
                    return Container(
                      child: Center(
                          child: Image.network( images[index],
                              fit: BoxFit.fill, width: 2000)),
                    );
                  },
                ))*/
            GestureDetector(
              onTap: (){
                showAlert(images[index]);

              },
              child:
              // images.length>1?
              // Container(
              //   height: 200,
              //   child:
              //   Carousel(
              //       height: 200.0,
              //       width: MediaQuery.of(context).size.width-10,
              //       // initialPage: 3,
              //       // allowWrap: false,
              //       // type: Types.slideSwiper,
              //       // indicatorType: IndicatorTypes.bar,
              //       // arrowColor: Colors.black,
              //       axis: Axis.horizontal,
              //       showArrow: true,
              //       // children: List.generate(images.length, (i) => Center(
              //       //       child:Image.network(images[i],),
              //       //     ))
              //   ),
              //   // CarouselSlider.builder(
              //   //   options: CarouselOptions(
              //   //     height:200,
              //   //     autoPlay: true,
              //   //   ),
              //   //   itemCount: images.length,
              //   //   itemBuilder: (context, itemIndex, realIndex) {
              //   //     return Container(
              //   //       child: Image.network(images[itemIndex], fit: BoxFit.cover,),
              //   //       width:  MediaQuery.of(context).size.width,
              //   //       margin: const EdgeInsets.only(
              //   //           top: 0.0, left: 5.0),
              //   //
              //   //     );
              //   //   },
              //   // ),
              //
              // ):
              FadeInImage.assetNetwork(
                placeholder: AppImages.defaultimage,
                image: images[index],
                fit: BoxFit.fill,
                width: MediaQuery.of(context).size.width,
              ),
      ),





            SizedBox(width: 2),
          ],
        ),
      ),
    );
  }


  showAlert(imag){
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(

              backgroundColor: Colors.transparent,
              content:
              Container(
                height: 100,
                width: 100,
                child: PhotoView(
                  initialScale: 4/4,
                  minScale: 4/4,
                  imageProvider:
                  NetworkImage(imag),
                ),
              )
              // PinchZoom(
              //   child: Image.network(imag),
              //
              //   // resetDuration: const Duration(milliseconds: 100),
              //  zoomEnabled: true,
              //   onZoomStart: (){print('Start zooming');},
              //   onZoomEnd: (){print('Stop zooming');},
              // )
          );
        });
  }

  Widget _newsimage(BuildContext context) {
    return Column(
      children: [
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width,
          child:  YoutubePlayer(
            aspectRatio:16/9,
            topActions: <Widget>[
              const SizedBox(width: 8.0),
              Expanded(
                child: Text(
                  "",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
              /*IconButton(
                icon: const Icon(
                  Icons.settings,
                  color: Colors.white,
                  size: 25.0,
                ),
                onPressed: () {
                  //  log('Settings Tapped!');
                },
              ),*/
            ],
            controller: YoutubePlayerController(
              initialVideoId: YoutubePlayer.convertUrlToId(widget.newsInfo.youTubeUrl), //Add videoID.
              flags: YoutubePlayerFlags(
                controlsVisibleAtStart: true,
                hideThumbnail: true,
                autoPlay: false,
                mute: false,
              ),
            ),
            showVideoProgressIndicator: true,
            bottomActions: [
              CurrentPosition(),
              ProgressBar(isExpanded: true),
            ],
          ),
        ),
        /*   if (widget.newsInfo.newsImage.length > 0)
          Container(
            height: MediaQuery.of(context).size.height * 0.3,
            width: MediaQuery.of(context).size.width,
            child:  CarouselSlider.builder(
              options: CarouselOptions(
                height: MediaQuery.of(context).size.height,
                autoPlay: false,
              ),
              itemCount: widget.newsInfo.newsImage.length,
              itemBuilder: (context, itemIndex, realIndex) {
                return GestureDetector(
                  onTap: () =>  Rouut.navigator.pushNamed(
                    Rouut.expandedImageView,
                    arguments: ExpandedImageViewArguments(
                      image: widget.newsInfo.newsImage,
                    ),
                  ),
                  child: Container(
                    width:  MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(
                        top: 0.0, left: 5.0),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(widget.newsInfo
                            .newsImage[itemIndex]),
                        fit: BoxFit.cover,
                      ),
                      // border:
                      //     Border.all(color: Theme.of(context).accentColor),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                );
              },
            ),
          ),*/
      ],
    );
  }


  Widget newscardview(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    int lineNumbers = h<800.0?4:16;
    // print('Printingggggg News Card Details hereeeeeeee '+widget.newsInfo.newsDate+widget.newsInfo.newsSource);
    return Padding(
      padding: const EdgeInsets.only(left:8.0,right: 8.0,top: 5.0),
      child: WatchBoxBuilder(
        box: Hive.box<NewsInfo>('bookmarks'),
          builder: (context, box) =>

              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column
        (
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Column(
                children: [
                  GestureDetector(
                   //onTap: () => /*'',*/handleBookmarks(widget.newsInfo),
                    child: Consumer<SettingsProvider>(
                        builder: (context, theme, child) =>
                            Text(
                              widget.newsInfo.newsTitle,
                              style: AppTextStyle.newsTitle
                                  .copyWith(color: theme.isDarkThemeOn
                                  ? Colors.white
                                  : Colors.black,
                              ),
                              maxLines: 2,
                              ),

                            ),
                    ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    widget.newsInfo.newsContent != null
                        ? widget.newsInfo.newsContent
                        : "",
                    maxLines: lineNumbers,
                    style: AppTextStyle.newsSubtitle
                        .copyWith(color: Colors.grey),

                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(

                    children: <Widget>[
                      Padding(
                          padding:
                          const EdgeInsets.fromLTRB(
                              0, 19, 0, 19)),
                      Text( "Swipe left for more at " +
                          (widget.newsInfo.newsSource!= null
                          ? widget.newsInfo.newsSource
                          : ""),
                        maxLines: 2,
                        style: AppTextStyle.newsFooter,
                      ),
                      Spacer(),
                      // EdgeInsets.only(bottom: 10)),
                      Text(
                        "${DateFormat("dd MMM").format(DateTime.parse(widget.newsInfo.newsDate))}",
                        style: AppTextStyle.newsFooter,
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ],
            ),
            SizedBox(height: 40,),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  //read button
                  SizedBox(
                    width: 10,
                  ),
               /*   Flexible(
                    child:  Consumer<SettingsProvider>(
                      builder: (context,
                          theme, child) =>
                          Container(
                            padding: EdgeInsets.only(
                                left: 5.0, right: 5.0),
                            width:
                            MediaQuery.of(context).size.width,
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 2.0, right: 2.0),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .spaceEvenly,
                                  children: [
                                    Row(children: [
                                      Consumer<SettingsProvider>(
                                          builder: (context,
                                              theme, child) =>
                                              IconButton(
                                                icon: Icon(
                                                  Icons
                                                      .favorite_border_outlined,
                                                  //CustomIcon.like_icon,
                                                  color: theme
                                                      .isDarkThemeOn
                                                      ? Colors
                                                      .white
                                                      : Colors
                                                      .black,
                                                ),
                                                alignment:
                                                Alignment
                                                    .topCenter,
                                                onPressed: () {
                                                  Navigator.push(
                                                    context,
                                                    SlideRightRoute(
                                                        page:
                                                        LoginView()),
                                                  );
                                                },
                                              )),
                                    ]),
                                    Row(
                                      children: [
                                        Consumer<SettingsProvider>(
                                            builder: (context,
                                                theme,
                                                child) =>
                                                IconButton(
                                                  icon: Icon(
                                                    Icons
                                                        .repeat_outlined,
                                                    //CustomIcon.group_icon,
                                                    color: theme.isDarkThemeOn
                                                        ? Colors
                                                        .white
                                                        : Colors
                                                        .black,
                                                  ),
                                                  alignment:
                                                  Alignment
                                                      .topCenter,
                                                  onPressed:
                                                      () {},
                                                )),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Consumer<   SettingsProvider>(
                                          builder: (context,
                                              theme, child) =>
                                              IconButton(
                                                icon: FaIcon(
                                                  FontAwesomeIcons
                                                      .shareAlt,
                                                  size: 18,
                                                  color: theme
                                                      .isDarkThemeOn
                                                      ? Colors.white
                                                      : Colors.black,
                                                ),
                                                alignment: Alignment
                                                    .topCenter,
                                                onPressed: () {
                                                  Provider.of<FeedProvider>(
                                                      context,
                                                      listen:
                                                      false)
                                                      .setWatermarkVisible(
                                                      true);

                                                  Future.delayed(
                                                      Duration(
                                                          seconds: 2),
                                                          () => convertWidgetToImageAndShare(
                                                          context,
                                                          _containerKey
                                                      ));
                                                },
                                              ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Consumer<
                                            SettingsProvider>(
                                            builder: (context,
                                                theme,
                                                child) =>
                                                IconButton(
                                                  icon: FaIcon(
                                                    FontAwesomeIcons
                                                        .bookmark,
                                                    size: 18,
                                                    color: theme.isDarkThemeOn
                                                        ? Colors
                                                        .white
                                                        : Colors
                                                        .black,
                                                  ),
                                                  alignment:
                                                  Alignment
                                                      .topCenter,
                                                  onPressed: () {
                                                    handleBookmarks(widget.newsInfo);
                                                  },
                                                )),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                    ),



                  ),*/

                  /*Consumer<SettingsProvider>(
                                          builder: (context, theme, child) =>
                                              IconButton(
                                                icon: FaIcon(
                                                  FontAwesomeIcons.bookmark,
                                                  size: 18,
                                                  color: theme.isDarkThemeOn
                                                      ? Colors.white
                                                      : Colors.black,
                                                ),
                                                alignment: Alignment.topCenter,
                                                onPressed: () {

                                                  handleBookmarks(article);

                                                },
                                              )),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Consumer<SettingsProvider>(
                                        builder: (context, theme, child) =>
                                            IconButton(
                                          icon: FaIcon(
                                            FontAwesomeIcons.share,
                                            size: 18,
                                            color: theme.isDarkThemeOn
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                          alignment: Alignment.topCenter,
                                          onPressed: () {
                                            Provider.of<FeedProvider>(context, listen: false)
                                                .setWatermarkVisible(true);

                                            Future.delayed(Duration(seconds: 2),
                                                    () => convertWidgetToImageAndShare(context, _containerKey));

                                          },
                                        ),
                                      ),*/

                ],
            ),

            // SizedBox(
            //   height: 30.0,
            // ),

            // SizedBox(
            //   height: 10.0,
            // ),

          ],
        ),
              ),
      ),
    );
  }

/*
  Widget _buildThreePostImage(BuildContext context){
    return  Container(
        width: MediaQuery.of(context).size.width,
        child: CarouselSlider(
          options: CarouselOptions(),
          items: newsImage.map((item) => Container(
            child: Center(
              // child: Image.network(item, fit: BoxFit.cover, width: 1000)
              child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(5)),
                  child: Padding(
                    padding: const EdgeInsets.only(left:4.0,right: 4.0),
                    child: Container(
                      height: 180,
                      width: MediaQuery.of(context).size.width,
                      child: Image(
                        image: ResizeImage(
                            AssetImage(
                              image[1],
                            ),
                            width: MediaQuery.of(context).size.width.round(),
                            height: MediaQuery.of(context).size.height.round()),
                        fit: BoxFit.fill,
                        //width: MediaQuery.of(context).size.width,
                      ),
                    ),
                  )),

            ),
          )).toList(),
        )
      */
/*child: Row(
        children: [
          Expanded(
            child: ClipRRect(
                borderRadius: BorderRadius.only(topLeft:Radius.circular(5),),
                child: Container(
                  height: 180,
                  child: Image(
                    image: ResizeImage(
                        AssetImage(
                          image[0],
                        ),
                        width: MediaQuery.of(context).size.width.round(),
                        height: MediaQuery.of(context).size.height.round()),
                    fit: BoxFit.fill,
                  ),
                )),
          ),
          SizedBox(width: 2),
          Expanded(
            child: Column(
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.only(topRight:Radius.circular(5)),
                    child: Container(
                      height: 90,
                      child: Image(
                        image: ResizeImage(
                            AssetImage(
                              image[1],
                            ),
                            width: MediaQuery.of(context).size.width.round(),
                            height: MediaQuery.of(context).size.height.round()),
                        fit: BoxFit.fill,
                        width: MediaQuery.of(context).size.width/2,
                      ),
                    )),
                SizedBox(height: 2),
                ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(1)),
                    child: Container(
                      height: 90,
                      child: Image.asset(
                        image[2],
                        fit: BoxFit.fill,
                        //  height: 150,
                        width: MediaQuery.of(context).size.width/2,
                      ),
                    )),
              ],
            ),
          )
        ],

      ),*/ /*

    );
  }
*/
  Widget _text(String title, String value) {
    return RichText(
      text: TextSpan(
        text: '$title : ',
        style: const TextStyle(
          color: Colors.blueAccent,
          fontWeight: FontWeight.bold,
        ),
        children: [
          TextSpan(
            text: value,
            style: const TextStyle(
              color: Colors.blueAccent,
              fontWeight: FontWeight.w300,
            ),
          ),
        ],
      ),
    );
  }




}
