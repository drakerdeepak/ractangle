
import 'package:Two/aplication_localization.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/slide_right_roure.dart';
import 'package:Two/saadh_dev/ui/login/login_view.dart';
import 'package:Two/services/news/share_service.dart';
import 'package:Two/style/text_style.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';


class NewscardItem extends StatefulWidget {
  final String newsId;
  final String newsTitle;
  final String newsContent;
  final String newsDate;
  final List<dynamic> newsImage;
  final String youTubeUrl;
  final String sourceUrl;

  NewscardItem(this.newsId, this.newsTitle, this.newsContent, this.newsDate, this.newsImage,
      this.youTubeUrl,this.sourceUrl);

  @override
  _NewscardItem createState() => _NewscardItem();
  
}

class _NewscardItem extends State<NewscardItem> {
  GlobalKey _containerKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: _containerKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _newsimage(context),
          newscardview(context),
        ],
      ),
    );
  }

  Widget _newsimage(BuildContext context) {
    return Column(
      children: [
        if (widget.youTubeUrl != null)
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width,
          child:  YoutubePlayer(
            aspectRatio:16/9,
            topActions: <Widget>[
              const SizedBox(width: 8.0),
              Expanded(
                child: Text(
                  "",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
              IconButton(
                icon: const Icon(
                  Icons.settings,
                  color: Colors.white,
                  size: 25.0,
                ),
                onPressed: () {
                  //  log('Settings Tapped!');
                },
              ),
            ],
            controller: YoutubePlayerController(
              initialVideoId: YoutubePlayer.convertUrlToId(widget.youTubeUrl), //Add videoID.
              flags: YoutubePlayerFlags(
                controlsVisibleAtStart: true,
                hideThumbnail: true,
                autoPlay: false,
                mute: false,
              ),
            ),
            showVideoProgressIndicator: true,
            bottomActions: [
              CurrentPosition(),
              ProgressBar(isExpanded: true),

            ],
          ),
        ),


        if (widget.newsImage.length > 0)
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width,
          child:  CarouselSlider.builder(
            options: CarouselOptions(
              height: MediaQuery.of(context).size.height,
              autoPlay: false,
            ),
            itemCount: widget.newsImage.length,
            itemBuilder: (context, itemIndex, realIndex) {
              return GestureDetector(
                onTap: () =>  Rouut.navigator.pushNamed(
                  Rouut.expandedImageView,
                  arguments: ExpandedImageViewArguments(
                    image: widget.newsImage,
                  ),
                ),
                child: Container(
                  width:  MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(
                      top: 0.0, left: 5.0),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(widget
                          .newsImage[itemIndex]),
                      fit: BoxFit.cover,
                    ),
                    // border:
                    //     Border.all(color: Theme.of(context).accentColor),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
              );
            },
          ),
        )

        //Image.asset(AppImages.newsimage,fit: BoxFit.cover,)),
      ],
    );
  }

  Widget newscardview(BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.newsTitle,
        style: AppTextStyle.newsTitle
            .copyWith(color: Colors.black
          ),
            maxLines: 2,
          ),

          SizedBox(
            height: 20.0,
          ),
          Text(
            widget.newsContent != null
                ? widget.newsContent
                : "",
            maxLines: 4,
            style: AppTextStyle.newsSubtitle
                .copyWith(color: Colors.grey),

          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment:
            MainAxisAlignment.spaceBetween,
            crossAxisAlignment:
            CrossAxisAlignment.start,
            children: <Widget>[
              //read button

              /*  Row(
                                  mainAxisSize: MainAxisSize.max,

                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        RaisedButton(
                                          child: Text(AppLocalizations.of(context).translate("read")*/ /*"Read"*/ /*,
                                              style: TextStyle(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w500,
                                                color: provider2.isDarkThemeOn
                                                    ? Colors.black
                                                    : Colors.white, )),
                                          // color: AppColor.onBackground,
                                          color: provider2.isDarkThemeOn
                                              ? Colors.white
                                              : Colors.black,
                                          padding: EdgeInsets.all(10.0),
                                          colorBrightness: Brightness.dark,
                                          onPressed: () {
                                            // Navigator.of(context).pop(true);
                                            // FocusScope.of(context).requestFocus(new FocusNode());

                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => WebScreen(
                                                    url: article.url,
                                                    isFromBottom: true),
                                              ),
                                            );
                                            // _forgotPasswordButtonAction();
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(25.0)),
                                        ),
                                      ]),*/

              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Container(
                  padding: EdgeInsets.only(
                      left: 5.0, right: 5.0),
                  width:
                  MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: appbarcolor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 2.0, right: 2.0),
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment
                            .spaceEvenly,
                        children: [
                          Row(children: [
                            Consumer<SettingsProvider>(
                                builder: (context,
                                    theme, child) =>
                                    IconButton(
                                      icon: Icon(
                                        Icons
                                            .favorite_border_outlined,
                                        //CustomIcon.like_icon,
                                        color: theme
                                            .isDarkThemeOn
                                            ? Colors
                                            .white
                                            : Colors
                                            .black,
                                      ),
                                      alignment:
                                      Alignment
                                          .topCenter,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          SlideRightRoute(
                                              page:
                                              LoginView()),
                                        );
                                      },
                                    )),
                          ]),
                          Row(
                            children: [
                              Consumer<
                                  SettingsProvider>(
                                  builder: (context,
                                      theme,
                                      child) =>
                                      IconButton(
                                        icon: Icon(
                                          Icons
                                              .repeat_outlined,
                                          //CustomIcon.group_icon,
                                          color: theme.isDarkThemeOn
                                              ? Colors
                                              .white
                                              : Colors
                                              .black,
                                        ),
                                        alignment:
                                        Alignment
                                            .topCenter,
                                        onPressed:
                                            () {},
                                      )),
                            ],
                          ),
                          Row(
                            children: [
                              Consumer<
                                  SettingsProvider>(
                                  builder: (context,
                                      theme,
                                      child) =>
                                      IconButton(
                                        icon: FaIcon(
                                          FontAwesomeIcons
                                              .bookmark,
                                          size: 18,
                                          color: theme.isDarkThemeOn
                                              ? Colors
                                              .white
                                              : Colors
                                              .black,
                                        ),
                                        alignment:
                                        Alignment
                                            .topCenter,
                                        onPressed: () {
                                          //  handleBookmarks(article);
                                        },
                                      )),
                            ],
                          ),
                          Row(
                            children: [
                              Consumer<
                                  SettingsProvider>(
                                builder: (context,
                                    theme, child) =>
                                    IconButton(
                                      icon: FaIcon(
                                        FontAwesomeIcons
                                            .share,
                                        size: 18,
                                        color: theme
                                            .isDarkThemeOn
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      alignment: Alignment
                                          .topCenter,
                                      onPressed: () {
                                        Provider.of<FeedProvider>(
                                            context,
                                            listen:
                                            false)
                                            .setWatermarkVisible(
                                            true);

                                        Future.delayed(
                                            Duration(
                                                seconds: 2),
                                                () => convertWidgetToImageAndShare(
                                                context,
                                                _containerKey
                                                ));
                                      },
                                    ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              /*Consumer<SettingsProvider>(
                                          builder: (context, theme, child) =>
                                              IconButton(
                                                icon: FaIcon(
                                                  FontAwesomeIcons.bookmark,
                                                  size: 18,
                                                  color: theme.isDarkThemeOn
                                                      ? Colors.white
                                                      : Colors.black,
                                                ),
                                                alignment: Alignment.topCenter,
                                                onPressed: () {

                                                  handleBookmarks(article);

                                                },
                                              )),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Consumer<SettingsProvider>(
                                        builder: (context, theme, child) =>
                                            IconButton(
                                          icon: FaIcon(
                                            FontAwesomeIcons.share,
                                            size: 18,
                                            color: theme.isDarkThemeOn
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                          alignment: Alignment.topCenter,
                                          onPressed: () {
                                            Provider.of<FeedProvider>(context, listen: false)
                                                .setWatermarkVisible(true);

                                            Future.delayed(Duration(seconds: 2),
                                                    () => convertWidgetToImageAndShare(context, _containerKey));

                                          },
                                        ),
                                      ),*/
              SizedBox(
                width: 10,
              ),

              Row(
                  mainAxisAlignment:
                  MainAxisAlignment.center,
                  crossAxisAlignment:
                  CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                        padding:
                        const EdgeInsets.fromLTRB(
                            0, 19, 0, 19)),
                    // EdgeInsets.only(bottom: 10)),
                    Text(
                      "${DateFormat("dd MMM").format(DateTime.parse(widget.newsDate))}",
                      style: AppTextStyle.newsFooter,
                      textAlign: TextAlign.center,
                    )
                  ]),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                flex: 1,
                child: Text(
                  "${"Source: "}${AppLocalizations.of(context).translate("swipe_message")} ${widget.sourceUrl} / ${DateFormat("MMMM d").format(
                    DateTime.parse(
                        widget.newsDate),
                  )}",
                  maxLines: 2,
                  style: AppTextStyle.newsFooter,
                ),
              ),
            ],
          ),

          SizedBox(
            height: 20.0,
          ),
        ],
      ),
    );
  }
}

const htmlData =
    "<p>The report also aligns with previous reports of the Apple AirPods 3 making its debut in Q2 2021, adding more reason for the September launch rumour to be true. Information around the&nbsp;<a href=\"https://indianexpress.com/about/apple/\">Apple</a>&nbsp;AirPods has been scarce, despite the occasional leaks and rumours.</p>";
