import 'package:Two/constant/shared_pref.dart';
import 'package:Two/global/global.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/app_bar.dart';
import 'package:Two/view/feed_screen/news_card_view.dart';
import 'package:flutter/material.dart';
import 'package:Two/common/widgets/news_appbar.dart';

import 'package:Two/controller/provider.dart';
import 'package:Two/model/news_model.dart';
import 'package:Two/view/web_screen/web.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class FeedViewScreen extends StatefulWidget {
  final List<Articles> articals;
  final int articalIndex;
  final bool isFromSearch;


  const FeedViewScreen(
      {Key key,
      @required this.articalIndex,
      @required this.articals,
      @required this.isFromSearch})
      : super(key: key);

  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedViewScreen>
    with AutomaticKeepAliveClientMixin {

  int index = 0;


  @override
  void initState() {
    super.initState();

   // setupLastIndex();
      // No-Internet Case
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    PageController _pageController =
        PageController(initialPage: widget.articalIndex);
    PageController _searchPageController = PageController();

    int lastPage = 0;
    final provider = Provider.of<FeedProvider>(context, listen: false);

    provider.setfeedPageController(_pageController);
    int prevIndex = index <= 0 ? 0 : index - 1;
    int nextIndex = index == widget.articals.length - 1 ? 0 : index + 1;

    return Scaffold(
      appBar: buildAppBarWithAll(context),
      backgroundColor: Colors.black,
      body: widget.isFromSearch
          ? PageView(
              controller: _searchPageController,
              children: [
                Stack(
                  fit: StackFit.expand,
                  children: [
                    PageView.builder(
                      onPageChanged: (page) {
                        if (page > lastPage) {
                          provider.setSearchAppBarVisible(false);
                          provider.setAppBarVisible(false);
                        } else {
                          provider.setSearchAppBarVisible(true);
                          provider.setAppBarVisible(true);
                        }
                        provider.setFeedBottomActionbarVisible(false);

                        lastPage = page;
                        provider.setCurentArticalIndex(page);
                      },
                      controller: _pageController,
                      itemCount: provider.allNewsList.length,
                      //widget.articals.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) => NewsCardView(
                          provider.allNewsList[index],

                      ),
                     /* itemBuilder: (_, index) {
                        return NewsCard(
                          article: widget.articals[index],
                          isFromSearch: widget.isFromSearch,
                        );
                      },*/
                    ),
                    widget.isFromSearch
                        ? Consumer<FeedProvider>(
                            builder: (context, value, child) =>
                                value.getSearchAppBarVisible
                                    ? Align(
                                        alignment: Alignment.topCenter,
                                        child: NewsCardAppBar(),
                                      )
                                    : Container(),
                          )
                        : Container()
                  ],
                ),
                WebScreen(
                  url: provider.getNewsURL,
                  isFromBottom: false,
                  pageController: _searchPageController,
                )
              ],
            )
          : Stack(
              fit: StackFit.expand,
              children: [
               /* PageView.builder(
                  allowImplicitScrolling: false,
                  onPageChanged: (page) {
                    if (page > lastPage) {
                      provider.setSearchAppBarVisible(false);
                      provider.setAppBarVisible(false);
                    } else {
                      provider.setSearchAppBarVisible(true);
                      provider.setAppBarVisible(true);
                    }
                    lastPage = page;
                    provider.setCurentArticalIndex(page);
                    provider.setFeedBottomActionbarVisible(false);
                  },
                  controller: _pageController,
                  itemCount: widget.articals.length,
                  scrollDirection: Axis.vertical,
                  pageSnapping:true,
                  physics:ScrollPhysics(),
                  itemBuilder: (_, index) {
                    return NewsCard(
                        article: widget.articals[index],
                        isFromSearch: widget.isFromSearch,
                      );

                  },
                ),*/
      Dismissible(
      background: NewsCardView(
        provider.allNewsList[index],
      ),
      child:  NewsCardView(
        provider.allNewsList[index],
      ),
      secondaryBackground: NewsCardView(
        provider.allNewsList[index],
      ),//newsCard(nextIndex),
      resizeDuration: Duration(milliseconds: 10),
      key: Key(index.toString()),

      direction: DismissDirection.vertical,
      onDismissed: (direction) {
         updateContent(direction);
      },
    ),
                widget.isFromSearch
                    ? Consumer<FeedProvider>(
                        builder: (context, value, child) =>
                            value.getSearchAppBarVisible
                                ? Align(
                                    alignment: Alignment.topCenter,
                                    child: NewsCardAppBar(),
                                  )
                                : Container(),
                      )
                    : Container()
              ],
            ),
    );
  }

  @override
  bool get wantKeepAlive => true;


  void updateContent(direction) {

    if (index <= 0 && direction == DismissDirection.down) {
      index = widget.articals.length - 1;
    } else if (index == widget.articals.length - 1 &&
        direction == DismissDirection.up) {
      index = 0;
    } else if (direction == DismissDirection.up) {
      index++;
    } else {
      index--;
    }
    print("index"+index.toString());

   // if(index>0){
     /* Fluttertoast.showToast(
        msg: 'Index to Zero',
        backgroundColor: Colors.black,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 3,
        toastLength: Toast.LENGTH_SHORT,
      );
    } else{*/
      updateIndex(index);
  //  }

  }


  void updateIndex(newIndex) {
    setState(() {
      index = newIndex;
    });
    SharePreference.setLastIndex(newIndex);
  }

  void setupLastIndex() async {
    int lastIndex = await SharePreference.getLastIndex();
    if (lastIndex != null && lastIndex < widget.articals.length - 1) {
      updateIndex(lastIndex);
    }
  }
}
