import 'package:Two/controller/provider.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_colors.dart';
import 'package:Two/saadh_dev/saadh_util/common/app_text_style.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/slide_right_roure.dart';
import 'package:Two/saadh_dev/ui/login/login_view.dart';
import 'package:Two/utils/widgethelper/widget_helper.dart';
import 'package:Two/view/app_base/app_base.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/global/global.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/style/text_style.dart';
import 'package:provider/provider.dart';

import '../../aplication_localization.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    final provider = Provider.of<FeedProvider>(context, listen: false);
    final settingprovider = Provider.of<SettingsProvider>(context, listen: false);


    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor:settingprovider.isDarkThemeOn ? Colors.white
            : Colors.black ,
        statusBarColor: settingprovider.isDarkThemeOn ? Colors.white
            : Colors.black ,
        statusBarIconBrightness: settingprovider.isDarkThemeOn ? Brightness.dark
            : Brightness.light,
        statusBarBrightness: settingprovider.isDarkThemeOn ? Brightness.dark
            : Brightness.light,
        systemNavigationBarIconBrightness: settingprovider.isDarkThemeOn ? Brightness.dark
            : Brightness.light,
      ),
    );
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacement(
          context,
          SlideRightRoute(page: AppBase()), ).whenComplete(() {
          final provider = Provider.of<FeedProvider>(
              context, listen: false);
          String languagecode = AppLocalizations.of(context).locale.languageCode;
          print(" languagecode "+languagecode);
          provider.getAllCategoriesList(languagecode);
          provider.getAllNewsList(languagecode);
        }
        );

        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            tooltip: 'Menu Icon',
            onPressed: () {
              Navigator.pushReplacement(
                context,
                SlideRightRoute(page: AppBase()), ).whenComplete(() {
                final provider = Provider.of<FeedProvider>(
                    context, listen: false);
                String languagecode = AppLocalizations.of(context).locale.languageCode;
                print(" languagecode "+languagecode);
                provider.getAllCategoriesList(languagecode);
                provider.getAllNewsList(languagecode);
              }
              );
              // Navigator.of(context).pop();
            },
          ),
          elevation: 1,
          title: Text(
            AppLocalizations.of(context).translate('settings'),
            style: AppTextStyle.appBarTitle.copyWith(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              color: Provider.of<SettingsProvider>(context, listen: false)
                      .isDarkThemeOn
                  ? AppColor.background
                  : AppColor.onBackground,
            ),
          ),
        ),
        body: Column(
          children: [
            Container(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    Text("save your preferences",style: getFormsettingTitleStyle(context),),
                    SizedBox(height: 10.0,),
                    Text("sign in to save your  likes \n and bookmarks.",style: getFormasettingrticletitleStyle(context),),
                    SizedBox(height: 20.0,),
                    Row(

                      children: [
                        Container(
                            height: 45,
                            padding: EdgeInsets.only(top:0),
                            child: RaisedButton(
                              textColor: Colors.white,
                              color: appcolor,
                              child: Text("Sign In ",style: getFormbuttonStyle(context),),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  SlideRightRoute(page: LoginView()),);

                              },
                            )),
                        Spacer(),
                        Row(
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(),primary: Colors.blue, padding: EdgeInsets.all(10)),
                              child:FaIcon(FontAwesomeIcons.google),
                              onPressed: () {},
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(), primary: Colors.blue,padding: EdgeInsets.all(10)),
                              child: Icon(
                                Icons.face,
                                size: 25,
                              ),
                              onPressed: () {},
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              height: 200.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey,
            ),
            Flexible(
              child:Consumer<SettingsProvider>(
                  builder: (context, settingsProvider, child) => ListView(
                    children: <Widget>[
                      ListTile(
                        leading: Icon(FeatherIcons.sunset),
                        title: Text(AppLocalizations.of(context).translate('dark_theme')),
                        subtitle: Text(
                            AppLocalizations.of(context).translate('darktheme_message')),
                        onTap: () {
                          settingsProvider.darkTheme(!settingsProvider.isDarkThemeOn);
                        },
                        trailing: Switch(
                            activeColor: settingsProvider.isDarkThemeOn
                            ? Colors.white
                            : Colors.black,

                            value: settingsProvider.isDarkThemeOn,
                            onChanged: (status) {
                              settingsProvider.darkTheme(status);
                            }),
                      ),
                      getDivider(),

                      ListTile(
                        leading: Icon(FontAwesomeIcons.language),
                        title: Text(AppLocalizations.of(context).translate('language')),
                        onTap: () {},
                        trailing: DropdownButton(
                            underline: Container(),
                            value: settingsProvider.activeLanguge,
                            items: Global.lang.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                            onChanged: (v) {
                              settingsProvider.setLang(v);

                            }),
                      ),
                      getDivider(),
                    ],
                  ),
                ),

            ),
          ],
        ),
      ),
    );
  }
}
