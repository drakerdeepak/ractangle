import 'dart:async';

import 'package:Two/controller/provider.dart';
import 'package:Two/controller/settings.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/style/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Two/constant/api_constant.dart';
import 'package:Two/constant/color_const.dart';
import 'package:Two/constant/string_const.dart';
import 'package:Two/utils/widgethelper/widget_helper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../aplication_localization.dart';


class PostArticleScreen extends StatefulWidget {
  @override
  _PostArticleScreenState createState() => _PostArticleScreenState();
}

class _PostArticleScreenState extends State<PostArticleScreen> {

  bool loading = true;

  final Completer<WebViewController> _webViewController =
  Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    //return Scaffold(body: _createUi());
   /* String url = Provider.of<FeedProvider>(context, listen: false).getNewsURL;*/
    String url = "";


   // https://www.colibri-sm.ru
    final provider = Provider.of<SettingsProvider>(context, listen: false);
      IconData _backIcon() {
      switch (Theme
          .of(context)
          .platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.arrow_back_ios;
        case TargetPlatform.iOS:
          return Icons.arrow_back;
        case TargetPlatform.linux:
        // TODO: Handle this case.
          break;
        case TargetPlatform.macOS:
        // TODO: Handle this case.
          break;
        case TargetPlatform.windows:
        // TODO: Handle this case.
          break;
      }
      assert(false);
      return null;
    }

    return new Scaffold(


        appBar: AppBar(
          elevation: 4,
          centerTitle: true,

          leading: IconButton(

          icon: new Image.asset("assets/icons/back_arrow.png",color: provider.isDarkThemeOn
          ? Colors.white
              : Colors.black),

              onPressed: () {
                Rouut.navigator.pushNamed(
                    Rouut.appBase);
              }),
          actions: <Widget>[
            FutureBuilder<WebViewController>(
                future: _webViewController.future,
                builder: (BuildContext context,
                    AsyncSnapshot<WebViewController> snapshot) {
                  final bool webViewReady =
                      snapshot.connectionState == ConnectionState.done;
                  final WebViewController controller = snapshot.data;
                  return IconButton(
                    icon: const Icon(Icons.refresh),
                    color: provider.isDarkThemeOn
                        ? Colors.white
                        : Colors.black,
                    onPressed: !webViewReady
                        ? null
                        : () {
                      setState(() {
                        loading = true;
                      });
                      controller.reload();
                    },
                  );
                }),
          ],
          title: Text(AppLocalizations.of(context).translate("post"),style: TextStyle(color: provider.isDarkThemeOn
              ? Colors.white
              : Colors.black,fontSize: 16,fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,),
          backgroundColor: provider.isDarkThemeOn
              ? Theme.of(context).bottomAppBarColor
              : AppColor.background,
        ),

        body:  Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Expanded(
              child:
              Text("POST ARTICLE SECTION",style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),


    );
  }
}



