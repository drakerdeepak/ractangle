import 'package:Two/aplication_localization.dart';
import 'package:Two/bloc/feed/news_feed_event.dart';
import 'package:Two/constant/api_constant.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/saadh_dev/core/service/independent_services/constant.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/app_bar.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/slide_right_roure.dart';
import 'package:Two/view/app_base/app_base.dart';
import 'package:Two/view/discover_screen/widgets/category_card.dart';
import 'package:Two/view/discover_screen/widgets/headline.dart';
import 'package:Two/view/discover_screen/widgets/topics_card_dashboard_tile.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class DiscoverScreen extends StatefulWidget {
  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();
}
enum LoadingState { DONE, LOADING, WAITING, ERROR }
class _DiscoverScreenState extends State<DiscoverScreen> {
  var bloc;

  LoadingState _loadingState = LoadingState.LOADING;
  bool _isLoading = false;

  void initState() {
    super.initState();
 //   bloc = BlocProvider.of<NewsFeedBloc>(context);

    check().then((intenet) {
      if (intenet != null && intenet) {

        // Internet Present Case
        setState(() {
          listenForCategories();
        });


      } else{
        Fluttertoast.showToast(
          msg: 'No Internet Conenction',
          backgroundColor: Colors.red,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          toastLength: Toast.LENGTH_LONG,
        );
         /*fl.show(SnackBarText.NO_INTERNET_CONNECTION, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);*/
      }
      // No-Internet Case
    });
  }


  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<FeedProvider>(context, listen: false);
    provider.setAppBarVisible(false);
    return Scaffold(
      appBar: buildAppBarWithAll(context),
      //appBar: appSearchBar(context),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //search widget

           /* GestureDetector(
              onTap: () {
                Rouut.navigator.pushNamed(Rouut.searchScreen);
              },
              child: Container(
                margin: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: AppColor.surface,
                  borderRadius: BorderRadius.circular(40),

                  border: Border.all(
                    color: Colors.black,

                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[


                    SizedBox(width: 5),

                    Text(
                      AppLocalizations.of(context).translate("search_message"),
                      style: TextStyle(
                        color: AppColor.iconGrey,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                    ),


                    Expanded(
                        child: Align(
                            alignment: Alignment.centerRight,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[

                                  Icon(
                                      Icons.search,
                                      color: AppColor.grey2
                                  ),
                                ]
                            )
                        )
                    ),
                    SizedBox(width: 5),



                  ],
                ),
              ),
            ),*/
            SizedBox(
              height: 8,
            ),
            headLine(AppLocalizations.of(context).translate("sugested_topics")),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Consumer<FeedProvider>(
                  builder: (context, value, child) => Row(
                    children: <Widget>[
                      CategoryCard(
                        title:
                        AppLocalizations.of(context).translate("my_feed"),
                        icon: "all",
                        active: provider.getActiveCategory == 1,
                        onTap: () {
                          Constant.selectedcategory="0";
                          provider.setActiveCategory(1);

                          provider.setAppBarTitle(AppLocalizations.of(context)
                              .translate("my_feed"));
                          Navigator.pushReplacement(
                            context,
                            SlideRightRoute(page: AppBase()),
                          );
                          /*bloc.add(
                            FetchNewsByCategoryEvent(category: "general"),
                          );*/
                        },
                      ),
                      CategoryCard(
                        title:
                        AppLocalizations.of(context).translate("trending"),
                        icon: "trending",
                        active: provider.getActiveCategory == 2,
                        onTap: () {
                          provider.setActiveCategory(2);
                          provider.setAppBarTitle(AppLocalizations.of(context)
                              .translate("trending"));
                          Navigator.pushReplacement(
                            context,
                            SlideRightRoute(page: AppBase()),
                          );

                          /*bloc.add(
                            FetchNewsByTopicEvent(topic: "trending"),
                          );*/
                        },
                      ),
                      CategoryCard(
                        title:
                        AppLocalizations.of(context).translate("bookmark"),
                        icon: "bookmark",
                        active: provider.getActiveCategory == 3,
                        onTap: () {
                          provider.setActiveCategory(3);
                          provider.setAppBarTitle(AppLocalizations.of(context)
                              .translate("bookmark"));
                          Navigator.pushReplacement(
                            context,
                            SlideRightRoute(page: AppBase()),
                          );
                         /* bloc.add(
                            FetchNewsFromLocalStorageEvent(box: 'bookmarks'),
                          );*/
                        },
                      ),
                      CategoryCard(
                        title:
                        AppLocalizations.of(context).translate("unreads"),
                        icon: "unread",
                        active: provider.getActiveCategory == 4,
                        onTap: () {
                          provider.setActiveCategory(4);
                          provider.setAppBarTitle(AppLocalizations.of(context)
                              .translate("unreads"));
                          Navigator.pushReplacement(
                            context,
                            SlideRightRoute(page: AppBase()),
                          );
                        /*  bloc.add(
                            FetchNewsFromLocalStorageEvent(box: 'unreads'),
                          );*/
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            headLine(AppLocalizations.of(context).translate("categories")),

            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0,0, 0),

              child: _getContentSection(context),
            ),
          ],
        ),
      ),
    );
  }


  Widget _getContentSection(BuildContext context) {
    final provider = Provider.of<FeedProvider>(context, listen: true);
    switch (_loadingState) {
      case LoadingState.DONE:
        print('done');
        return new GridView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.only(left: 8,right:8,top: 0,bottom: 8),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: 1,
            //crossAxisSpacing: 3.0
          ),

          itemCount: provider.allCategoriesList.length,

          itemBuilder: (context, index) => TopicCardTile(provider.allCategoriesList[index]),

          //_buildItemGRowGroup(_beers[index],index)//BeerTile(_beers[index],index),=

        );

      case LoadingState.ERROR:
        return Text('Sorry, there was an error loading the data!');
      case LoadingState.LOADING:
        return CircularProgressIndicator();
      default:
        return Container();
    }
  }

  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }


  void listenForCategories() async {

    _isLoading = true;
    try {
      String languagecode = AppLocalizations.of(context).locale.languageCode;
      print(" languagecode "+languagecode);

      final provider = Provider.of<FeedProvider>(context, listen: false);
      await provider.getAllCategoriesList(languagecode);


      String url = ApiConstant.BASE_URL+ApiConstant.CATEGORY+languagecode;
      //
      // String url= "http://13.232.88.56/api/categories";
      // String url = 'https://api.punkapi.com/v2/beers';
      // final List<Categories> categories = await getDashboardCategories(url);


    //  final Stream<Dashboard> stream = await getDashboardCategories(url);
      //  print('categories11'+categories.length.toString());
      _isLoading =false;
      setState(() =>
      _loadingState = LoadingState.DONE );

      //print('good'+categories.length.toString());
     /* setState(() =>
          _dashboard = categories);*/

   /*   stream.listen((Dashboard dashboard) =>
          setState(() =>
              _dashboard.add(dashboard))
      );*/

    } catch (e) {
      _isLoading = false;
      if (_loadingState == LoadingState.LOADING) {
        setState(() => _loadingState = LoadingState.ERROR);
      }
    }

  }



}


