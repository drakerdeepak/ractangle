import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:Two/aplication_localization.dart';
import 'package:Two/routes/rouut.dart';
import 'package:Two/style/colors.dart';

appSearchBar(context) {
  return PreferredSize(
    child: Material(
      elevation: 1,
      // color: Colors.white,
      child: GestureDetector(
        onTap: () {
          Rouut.navigator.pushNamed(Rouut.searchScreen);
        },
        child: Container(
          margin: const EdgeInsets.fromLTRB(16, 98, 16, 16),
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: AppColor.surface,
            borderRadius: BorderRadius.circular(40),

            border: Border.all(
              color: Colors.black,

            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[


              SizedBox(width: 5),

              Text(
                AppLocalizations.of(context).translate("search_message"),
                style: TextStyle(
                  color: AppColor.iconGrey,
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                ),
              ),


          Expanded(
          child: Align(
          alignment: Alignment.centerRight,
          child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[

            Icon(
                Icons.search,
                color: AppColor.grey2
            ),
            ]
          )
          )
          ),
              SizedBox(width: 5),



            ],
          ),
        ),
      ),
    ),
    preferredSize: Size.fromHeight(135),
  );
}
