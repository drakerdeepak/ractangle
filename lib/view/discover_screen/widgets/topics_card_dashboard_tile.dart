import 'package:Two/aplication_localization.dart';
import 'package:Two/bloc/feed/news_feed_bloc.dart';
import 'package:Two/bloc/feed/news_feed_event.dart';
import 'package:Two/constant/dashboard.dart';
import 'package:Two/constant/dashboard.dart';
import 'package:Two/constant/dashboard1.dart';
import 'package:Two/controller/provider.dart';
import 'package:Two/model/response_all_catogries.dart';
import 'package:Two/saadh_dev/core/service/independent_services/app_url.dart';
import 'package:Two/saadh_dev/core/service/independent_services/constant.dart';
import 'package:Two/saadh_dev/saadh_util/helper_widget/slide_right_roure.dart';
import 'package:Two/view/app_base/app_base.dart';
import 'package:Two/view/app_base/app_base_category.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:Two/controller/feed_controller.dart';
import 'package:Two/global/global.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/style/text_style.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class TopicCardTile extends StatelessWidget {

  final CategoryInfo _allCategoriesList;

    TopicCardTile(this._allCategoriesList);
  var bloc;



  @override
  Widget build(BuildContext context) {
  //  print( 'category_name'+_allCategoriesList..category_name);
    final provider = Provider.of<FeedProvider>(context, listen: false);
    provider.setAppBarVisible(false);
    bloc = BlocProvider.of<NewsFeedBloc>(context);
    return GestureDetector(
      onTap: () {
        Navigator.pushReplacement(
          context,
          SlideRightRoute(page: AppBaseCategory()),

        );
       // FeedController.addCurrentPage(1);

        provider.setAppBarTitle(
            _allCategoriesList.categoryName);

        String languagecode = AppLocalizations.of(context).locale.languageCode;
        print(" languagecode "+languagecode);
        Constant.selectedcategory = _allCategoriesList.categoryId;
        print(" selectedcategory "+Constant.selectedcategory );
        provider.getFilterNewsList(languagecode,_allCategoriesList.categoryId);
        FeedController.addCurrentPage(1);
        /*bloc.add(
          FetchNewsByCategoryEvent(category: _allCategoriesList.categoryId),
        );*/
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(19, 0, 19,0),

        padding: EdgeInsets.only(
          top: 0 /*+ Consts.padding*/,
          bottom: 0,
          left: 2,
          right: 2,
        ),
       // margin: EdgeInsets.only(top: Consts.avatarRadius,left: 10,right: 10,bottom: 8),
       /* decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          border: Border.all(
            color: Colors.white,
          ),
          borderRadius: BorderRadius.circular(Consts.avatarRadius),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        )*/

        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.white,
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 1.0),
            ),
          ],
        ),

       /* decoration: BoxDecoration(
          border: Border.all(
            color: AppColor.onPrimary,
          ),

          // color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),*/
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child : Padding(
                padding: const EdgeInsets.all(15.0),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                  imageUrl: _allCategoriesList.image,
                ),
              ),
             /* child: Image.asset(
                "assets/icons/$_dashboard.,.png",
                fit: BoxFit.contain,
              ),*/
            ),

            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 100,bottom: 0 /*+ Consts.padding*/,
                ),/* const EdgeInsets.all(0.0),*/
                child: Text(
                  _allCategoriesList.categoryName,
                  style: TextStyle(
                    fontSize: 13.0, // insert your font size here
                  ),
                  //style: AppTextStyle.topiccardTitle,
                  //overflow: TextOverflow.ellipsis,
                ),
              ),
            ),

          ],
        ),

      ),

    );
  }
}

class Consts {
  Consts._();

  static const double padding = 10.0;
  static const double avatarRadius = 60;
}
