import 'package:Two/controller/settings.dart';
import 'package:flutter/material.dart';
import 'package:Two/controller/feed_controller.dart';
import 'package:Two/global/global.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/style/text_style.dart';
import 'package:provider/provider.dart';

class CategoryCard extends StatelessWidget {
  final String icon;
  final String title;
  final Function onTap;
  final bool active;

  const CategoryCard(
      {Key key, this.icon, this.title, this.onTap, this.active = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider2 = Provider.of<SettingsProvider>(context, listen: false);
    return GestureDetector(
      onTap: () {
        onTap();
        FeedController.addCurrentPage(1);
      },
      child: Container(
        decoration: BoxDecoration(

          color:AppColor.surface,
          //borderRadius: BorderRadius.circular(2),
        ),
       // color: AppColor.surface,
        margin: const EdgeInsets.fromLTRB(0,15,0,8),
       // height: Global.height(context) * 0.14,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[

          /*  Padding(
              padding: EdgeInsets.fromLTRB(10,6,10,6),*/
              /*child:*/ Container(

             // minHeight: Global.height(context),
             // minWidth: double.maxFinite,
              height: 50,
                width: Global.width(context)*.24,
                decoration: BoxDecoration(
                  color: active ? Colors.black : Colors.transparent,
                  borderRadius: BorderRadius.circular(0),
                ),
                child: Center(
                  child: Text(
                    title,
                    style: active
                        ? AppTextStyle.categoryTitle.copyWith(
                        color: active ? Colors.white : Colors.black,//AppColor.onBackground,
                        fontSize: 12,
                        fontWeight: FontWeight.w600)
                        : AppTextStyle.categoryTitle,
                  ),
                ),
              ),
           // )


          ],
        ),
      ),
    );
  }
}
