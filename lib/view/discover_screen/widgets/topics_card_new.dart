import 'package:flutter/material.dart';
import 'package:Two/controller/feed_controller.dart';
import 'package:Two/global/global.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/style/text_style.dart';

class TopicCardNew extends StatelessWidget {
  final String icon;
  final String title;
  final Function onTap;

  const TopicCardNew({Key key, this.icon, this.title, this.onTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FeedController.addCurrentPage(1);
        onTap();
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(19, 0, 19,3),

        padding: EdgeInsets.only(
          top: 0 /*+ Consts.padding*/,
          bottom: 0,
          left: 2,
          right: 2,
        ),
       // margin: EdgeInsets.only(top: Consts.avatarRadius,left: 10,right: 10,bottom: 8),
       /* decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          border: Border.all(
            color: Colors.white,
          ),
          borderRadius: BorderRadius.circular(Consts.avatarRadius),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        )*/

        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.white,
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 1.0),
            ),
          ],
        ),

       /* decoration: BoxDecoration(
          border: Border.all(
            color: AppColor.onPrimary,
          ),

          // color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),*/
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                "assets/icons/$icon.png",
                fit: BoxFit.contain,
              ),
            ),

            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Text(
                  title,
                  style: AppTextStyle.topiccardTitle,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),

          ],
        ),

      ),

    );
  }
}

class Consts {
  Consts._();

  static const double padding = 10.0;
  static const double avatarRadius = 60;
}
