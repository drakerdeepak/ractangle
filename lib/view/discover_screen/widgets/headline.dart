import 'package:Two/controller/settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/style/text_style.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Widget headLine(String title) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(16, 7, 16, 0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: AppTextStyle.headline1,
        ),
        SizedBox(
          height: 8,
        ),
        Consumer<   SettingsProvider>(
          builder: (context,
              theme, child) =>
              Container(
                width: 36,
                height: 2.5,
                color: theme
                    .isDarkThemeOn
                    ? Colors.white
                    : Colors.black,
                //color: AppColor.onBackground,
              ),
        ),
        Container(
          width: 36,
          height: 2.5,
          color: AppColor.onBackground,
        ),
      ],
    ),
  );
}
