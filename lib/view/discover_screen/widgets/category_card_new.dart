import 'package:flutter/material.dart';
import 'package:Two/controller/feed_controller.dart';
import 'package:Two/global/global.dart';
import 'package:Two/style/colors.dart';
import 'package:Two/style/text_style.dart';

class CategoryCard extends StatelessWidget {
  final String icon;
  final String title;
  final Function onTap;
  final bool active;

  const CategoryCard(
      {Key key, this.icon, this.title, this.onTap, this.active = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
        FeedController.addCurrentPage(1);
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(8,0,8,0),
        height: Global.height(context) * 0.14,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[

            Padding(
              padding: EdgeInsets.only(top: 5.0),
              child: Container(
                width: 75,
                height: 30,
                decoration: BoxDecoration(
                  color: active ? Colors.black : Colors.transparent,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Center(
                  child: Text(
                    title,
                    style: active
                        ? AppTextStyle.categoryTitle.copyWith(
                        color: active ? Colors.white : Colors.black,//AppColor.onBackground,
                        fontSize: 13.5,
                        fontWeight: FontWeight.w500)
                        : AppTextStyle.categoryTitle,
                  ), /*Text(
                    title,
                    style: TextStyle(
                      //fontSize: 14,
                      color: isSelected ? Colors.white : Colors.black,
                      // fontWeight: FontWeight.w500,
                    ),
                  ),*/
                ),
              ),
            )

     /* Text(
      title,
      style: active
          ? AppTextStyle.categoryTitle.copyWith(
          color: AppColor.onBackground,
          fontSize: 13.5,
          fontWeight: FontWeight.w500)
          : AppTextStyle.categoryTitle,
    ),*/

           /* Opacity(
              opacity: active ? 1 : 0.5,
              child: Image.asset(
                "assets/icons/$icon.png",
                height: 70,
                width: 70,
                fit: BoxFit.contain,
              ),
            ),
            Text(
              title,
              style: active
                  ? AppTextStyle.categoryTitle.copyWith(
                      color: AppColor.onBackground,
                      fontSize: 13.5,
                      fontWeight: FontWeight.w500)
                  : AppTextStyle.categoryTitle,
            ),*/
          ],
        ),
      ),
    );
  }
}
